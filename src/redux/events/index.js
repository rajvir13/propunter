// ----------------------------------------
// ----------------------------------------
// Login
// ----------------------------------------
export const LOGIN_SUCCESS = 'LOGIN_SUCCESS';
export const LOGIN_FAIL = 'LOGIN_FAIL';

// ----------------------------------------
// ----------------------------------------
// Forgot
// ----------------------------------------
export const FORGOT_SUCCESS = 'FORGOT_SUCCESS';
export const FORGOT_FAIL = 'FORGOT_FAIL';

// ----------------------------------------
// ----------------------------------------
// Reset password
// ----------------------------------------
export const RESET_SUCCESS = 'RESET_SUCCESS';
export const RESET_FAIL = 'RESET_FAIL';


// ----------------------------------------
// ----------------------------------------
// New Post password
// ----------------------------------------
export const NEW_POST_SUCCESS = 'NEW_POST_SUCCESS';
export const NEW_POST_FAIL = 'NEW_POST_FAIL';

// ----------------------------------------
// ----------------------------------------
// New Post password
// ----------------------------------------
export const TIPSER_POST_SUCCESS = 'NEW_POST_SUCCESS';
export const TIPSER_POST_FAIL = 'NEW_POST_FAIL';

// ----------------------------------------
// ----------------------------------------
// New Post password
// ----------------------------------------
export const CHAT_MESSAGE_SUCCESS = 'CHAT_MESSAGE_SUCCESS';
export const CHAT_MESSAGE_FAIL = 'CHAT_MESSAGE_FAIL';


// ----------------------------------------
// ----------------------------------------
// Tipsters 
// ----------------------------------------
export const TIPSTER_SUCCESS = 'TIPSTER_SUCCESS';
export const TIPSTER_FAIL = 'TIPSTER_FAIL';


// ----------------------------------------
// ----------------------------------------
// Tipsters 
// ----------------------------------------
export const NOTIFICATION_SUCCESS = 'NOTIFICATION_SUCCESS';
export const NOTIFICATION_FAIL = 'NOTIFICATION_FAIL';
// Update profile
// ----------------------------------------
export const UPDATE_PROFILE_SUCCESS = 'UPDATE_PROFILE_SUCCESS';
export const UPDATE_PROFILE_FAIL = 'UPDATE_PROFILE_FAIL';

// ----------------------------------------
// ----------------------------------------
// Tipser Status
// ----------------------------------------
export const TIPSER_STATUS_SUCCESS = 'TIPSER_STATUS_SUCCESS';
export const TIPSER_STATUS_FAIL = 'TIPSER_STATUS_FAIL';


// ----------------------------------------
// ----------------------------------------
// Tipser Status
// ----------------------------------------
export const GET_ICON_SUCCESS = 'GET_ICON_SUCCESS';
export const TGET_ICON_FAIL = 'TGET_ICON_FAIL';

// ----------------------------------------
// ----------------------------------------
// Tipser Status
// ----------------------------------------
export const UPDATE_ICON_SUCCESS = 'UPDATE_ICON_SUCCESS';
export const UPDATE_ICON_FAIL = 'UPDATE_ICON_FAIL';

export const STARTED = 'STARTED';
