import * as types from '../events';

const initialState = {
    ResetResponse: undefined,

};
const ResetPasswordReducer = (state = initialState, action) => {

    switch (action.type) {

        case types.RESET_SUCCESS:

            return {...state, ResetResponse: action.response};
        case types.RESET_FAIL:
            return {...state, ResetResponse: action.error};
        default:
            return state;
    }
};

export default ResetPasswordReducer;
