import * as types from '../events';

const initialState = {
    updateProfileResponse: undefined,

};
const UpdateProfileReducer = (state = initialState, action) => {

    switch (action.type) {

        case types.UPDATE_PROFILE_SUCCESS:

            return {...state, updateProfileResponse: action.response};
        case types.UPDATE_PROFILE_FAIL:
            return {...state, updateProfileResponse: action.error};
        default:
            return state;
    }
};

export default UpdateProfileReducer;
