import * as types from '../events';

const initialState = {
    tipsterResponse: undefined,

};
const AllTipsterReducer = (state = initialState, action) => {

    switch (action.type) {

        case types.TIPSTER_SUCCESS:

            return {...state, tipsterResponse: action.response};
        case types.TIPSTER_FAIL:
            return {...state, tipsterResponse: action.error};
        default:
            return state;
    }
};

export default AllTipsterReducer;
