import * as types from '../events';

const initialState = {
    ForgotResponse: undefined,

};
const ForgotReducer = (state = initialState, action) => {

    switch (action.type) {

        case types.FORGOT_SUCCESS:

            return {...state, ForgotResponse: action.response};
        case types.FORGOT_FAIL:
            return {...state, ForgotResponse: action.error};
        default:
            return state;
    }
};

export default ForgotReducer;
