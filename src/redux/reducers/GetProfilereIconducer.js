import * as types from '../events';

const initialState = {
    updateProfileResponse: undefined,

};
const GetProfileIconReducer = (state = initialState, action) => {

    switch (action.type) {

        case types.GET_ICON_SUCCESS:

            return {...state, updateProfileResponse: action.response};
        case types.TGET_ICON_FAIL:
            return {...state, updateProfileResponse: action.error};
        default:
            return state;
    }
};

export default GetProfileIconReducer;
