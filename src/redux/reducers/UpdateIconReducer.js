import * as types from '../events';

const initialState = {
    updateIconResponse: undefined,

};
const UpdateIconReducer = (state = initialState, action) => {

    switch (action.type) {

        case types.UPDATE_PROFILE_SUCCESS:

            return {...state, updateIconResponse: action.response};
        case types.UPDATE_PROFILE_FAIL:
            return {...state, updateIconResponse: action.error};
        default:
            return state;
    }
};

export default UpdateIconReducer;
