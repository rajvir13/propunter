import * as types from '../events';

const initialState = {
    chatMessageResponse: undefined,

};
const ChatMessageReducer = (state = initialState, action) => {

    switch (action.type) {

        case types.CHAT_MESSAGE_SUCCESS:

            return {...state, chatMessageResponse: action.response};
        case types.CHAT_MESSAGE_FAIL:
            return {...state, chatMessageResponse: action.error};
        default:
            return state;
    }
};

export default ChatMessageReducer;
