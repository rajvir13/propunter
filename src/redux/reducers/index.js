import {combineReducers} from 'redux';
import LoginReducer from './LoginReducer';
import ForgotReducer from './ForgotReducer';
import ResetPasswordReducer from './ResetPasswordReducer';
import NewPostReducer from './NewPostReducer';
import ChatMessageReducer from './ChatMessageReducer';
import AllTipsterReducer from './AllTipsterReducer';
import NotificationReducer from './NotificationReducer';
import UpdateProfileReducer from './UpdateProfileReducer';
import TipserStatusReducer from './TipserStatusReducer';
import UpdateIconReducer from './UpdateIconReducer';
import GetProfileIconReducer from './GetProfilereIconducer';

const rootReducer = (state, action) => {
    if (action.type === 'LOGOUT_SUCCESS') {
        state = undefined;
    }
    return appReducer(state, action);
};

const appReducer = combineReducers({
    LoginReducer,
    NotificationReducer,
    AllTipsterReducer,
    ChatMessageReducer,
    NewPostReducer,
    ForgotReducer,
    ResetPasswordReducer,
    UpdateProfileReducer,
    GetProfileIconReducer,
    TipserStatusReducer,
    UpdateIconReducer,
});

export default rootReducer;
