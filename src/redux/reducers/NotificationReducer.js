import * as types from '../events';

const initialState = {
    notificationResponse: undefined,

};
const NotificationReducer = (state = initialState, action) => {

    switch (action.type) {

        case types.NOTIFICATION_SUCCESS:

            return {...state, notificationResponse: action.response};
        case types.NOTIFICATION_FAIL:
            return {...state, notificationResponse: action.error};
        default:
            return state;
    }
};

export default NotificationReducer;
