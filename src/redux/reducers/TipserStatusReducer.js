import * as types from '../events';

const initialState = {
    tipserStatusResponse: undefined,

};
const TipserStatusReducer = (state = initialState, action) => {

    switch (action.type) {

        case types.TIPSER_STATUS_SUCCESS:

            return {...state, tipserStatusResponse: action.response};
        case types.TIPSER_STATUS_FAIL:
            return {...state, tipserStatusResponse: action.error};
        default:
            return state;
    }
};

export default TipserStatusReducer;
