import * as types from '../events';

const initialState = {
    newPostResponse: undefined,

};
const NewPostReducer = (state = initialState, action) => {

    switch (action.type) {

        case types.NEW_POST_SUCCESS:

            return {...state, newPostResponse: action.response};
        case types.NEW_POST_FAIL:
            return {...state, newPostResponse: action.error};
        default:
            return state;
    }
};

export default NewPostReducer;
