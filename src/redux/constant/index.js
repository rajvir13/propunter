// ----------------------------------------
// ----------------------------------------
// Base Url
// ----------------------------------------
import {Platform} from 'react-native';

module.exports.BASE_URL = 'https://stgphys.appsndevs.com/propunter/wp-json/jwt-auth/v1/';
// ----------------------------------------
// ----------------------------------------
// Device Type
// ----------------------------------------
export const DeviceType = Platform.OS === 'ios' ? 'iphone' : 'Android';


export const API = {
    Login: 'token',
    Reset_Password: 'users/restpassword',
    Verify_Otp: 'users/verifyotp',
    Lost_Password: 'users/lostpassword',
    Get_News: 'users/getnewsdata',
    Tipser_Post: 'users/getpostsdata',
    Update_Profile: 'user/updateprofile',
    Get_Profile: 'user/getprofile',
    Notification_Status: 'updatetipster',
    Get_Chat: 'getuserfeeds',
    All_Tipsters: 'getalltipster',
    Chat_Term: 'acceptterms',
    Get_Notification_List: 'getUserNotificationstimezone',
    Read_Notification: 'readNotification',
    Last_Seen:'lastseen',
    Log_Out:'user/logout',
    Get_Post:'getpostbyid',
    Get_Icon:'getEmojis',
    Update_Icon:'update-profile-pic-of-customer-by-url'




};
