/**
 * Created by Rajnish-React on 8/23/18.
 */
import {
    API,
    BASE_URL, DeviceType,
} from '../constant';
import * as types from '../events';
import * as RNLocalize from 'react-native-localize';

export const NoificationListAction = (payload) => {

    let data = new FormData();
    data.append('timezone', RNLocalize.getTimeZone());
    return function (dispatch) {
        fetch(BASE_URL + API.Get_Notification_List, {
            method: 'POST',
            headers: {
                'Content-Type': 'multipart/form-data',
                'Authorization': 'Bearer' + payload.token,
            },
            body: data,
        })
            .then((response) => response.json())
            .then((responseData) => {
                dispatch(notificationSuccess(responseData));
            })
            .catch((error) => {
                dispatch(notificationFail(error));
            }).done();
    };
};
export const notificationSuccess = (responseData) => {
    return {
        type: types.NOTIFICATION_SUCCESS,
        response: responseData,
    };
};
export const notificationFail = (error) => {
    return {
        type: types.NOTIFICATION_FAIL,
        error: error.message,
    };
};


module.exports.NotificationCount = (payload, NotificationCountResponse) => {
    const data = new FormData();
    data.append('timezone', RNLocalize.getTimeZone());
    fetch(BASE_URL + API.Get_Notification_List, {
        method: 'POST',
        headers: {
            'Content-Type': 'multipart/form-data',
            'Authorization': 'Bearer' + payload.token,
        },
        body: data,

    })
        .then((response) => response.json())
        .then((responseData) => {
            NotificationCountResponse(responseData);
        })
        .catch((error) => {
            NotificationCountResponse(error);
        }).done();
};


module.exports.ReadNotification = (payload, ReadNotificationResponse) => {
      console.warn('helooooo', payload);

    let data = new FormData();
    data.append('post_id', payload.post_id);
    fetch(BASE_URL + API.Read_Notification, {
        method: 'POST',
        headers: {
            'Content-Type': 'multipart/form-data',
            'Authorization': 'Bearer' + payload.token,
        },
        body: data,

    })
        .then((response) => response.json())
        .then((responseData) => {
            console.warn('dataaaaaSuccess', JSON.stringify(responseData));
            ReadNotificationResponse(responseData);
        })
        .catch((error) => {
            console.warn('error', error);
            ReadNotificationResponse(error);
        }).done();
};


module.exports.GetPost = (payload, GetPostResponse) => {


    let data = new FormData();
    data.append('post_id', payload.post_id);
    fetch(BASE_URL + API.Get_Post, {
        method: 'POST',
        headers: {
            'Content-Type': 'multipart/form-data',
            'Authorization': 'Bearer' + payload.token,

        },
        body: data,

    })
        .then((response) => response.json())
        .then((responseData) => {
            //   console.warn('dataaaaa', JSON.stringify(responseData));
            console.warn('helooooo', responseData);
            GetPostResponse(responseData);
        })
        .catch((error) => {
            console.warn('helooooo', error);
            GetPostResponse(error);
        }).done();
};
