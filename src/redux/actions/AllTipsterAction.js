/**
 * Created by Rajnish-React on 8/23/18.
 */
import {
    API,
    BASE_URL, DeviceType,
} from '../constant';
import * as types from '../events';
import {Switch} from '../../components/toggleSwitch';

export const AllTipsterAction = (payload) => {
    const data = new FormData()
    return function (dispatch) {
        fetch(BASE_URL + API.All_Tipsters, {
            method: 'POST',
            headers: {
                'Content-Type': 'multipart/form-data',
                'Authorization': 'Bearer' + payload.token,
            },


        })
            .then((response) => response.json())
            .then((responseData) => {
                console.warn("respone data",responseData)
                dispatch(tipsterSuccess(responseData));
            })
            .catch((error) => {
                dispatch(tipsterFail(error));
            }).done();
    };
};
export const tipsterSuccess = (responseData) => {
    return {
        type: types.TIPSTER_SUCCESS,
        response: responseData,
    };
};
export const tipsterFail = (error) => {
    return {
        type: types.TIPSTER_FAIL,
        error: error.message,
    };
};

/*
value={data.status == 0 ? false : true}*/
