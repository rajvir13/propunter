/**
 * Created by Rajnish-React on 8/23/18.
 */
import {
    API,
    BASE_URL, DeviceType,
} from '../constant';
import * as types from '../events';

export const GetIconAction = (payload) => {
    return function (dispatch) {
        fetch(BASE_URL + API.Get_Icon, {
            method: 'GET',
            headers: {
                'Content-Type': 'multipart/form-data',
                'Authorization': 'Bearer' + payload.token,
            },
        })
            .then((response) => response.json())
            .then((responseData) => {
               // console.warn("respone data",responseData)
                dispatch(GetIconSuccess(responseData));
            })
            .catch((error) => {
                dispatch(GetIconFail(error));
            }).done();
    };
};


export const GetIconSuccess = (responseData) => {
    return {
        type: types.GET_ICON_SUCCESS,
        response: responseData,
    };
};
export const GetIconFail = (error) => {
    return {
        type: types.GET_ICON_FAIL,
        error: error.message,
    };
};


