/**
 * Created by Rajnish-React on 8/23/18.
 */
import {
    API,
    BASE_URL,
} from '../constant';
import * as types from '../events';

export const ForgotAction = (payload) => {
    return function (dispatch) {

        let data = new FormData();
        console.warn('Data', data);
        data.append('useremail', payload.useremail);
        fetch(BASE_URL + API.Lost_Password, {
            method: 'POST',
            headers: {
                'Content-Type': 'multipart/form-data',
                //'Authorization': 'Bearer' + payload.token

            },
            body: data,
        })
            .then((response) => response.json())
            .then((responseData) => {
                dispatch(ForgotSuccess(responseData));
            })
            .catch((error) => {
                dispatch(ForgotFail(error));
            }).done();
    };
};
export const ForgotSuccess = (responseData) => {
    return {
        type: types.FORGOT_SUCCESS,
        response: responseData,
    };
};
export const ForgotFail = (error) => {
    return {
        type: types.FORGOT_FAIL,
        error: error.message,
    };
};


module.exports.VerifyOTPAction = (payload, reverifyOTPResponse) => {
    let data = new FormData();
    data.append('useremail', payload.useremail);
    data.append('userotp', payload.userotp);
    fetch(BASE_URL + API.Verify_Otp, {
        method: 'POST',
        headers: {
            'Content-Type': 'multipart/form-data',
            //'Authorization': 'Bearer' + payload.token

        },
        body: data,
    })
        .then((response) => response.json())
        .then((responseData) => {
            reverifyOTPResponse(responseData);
        })
        .catch((error) => {
            reverifyOTPResponse(error);
        }).done();
};
