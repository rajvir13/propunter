/**
 * Created by Rajnish-React on 8/23/18.
 */
import {
    API,
    BASE_URL, DeviceType,
} from '../constant';
import * as types from '../events';


export const TipserNotificationStatusAction = (payload) => {
    console.warn('Check data', payload);
    return function (dispatch) {

        let data = new FormData();
        data.append('tipster_id', Number(payload.tipster_id));
        data.append('status', Number(payload.status));
        fetch(BASE_URL + API.Notification_Status, {
            method: 'POST',
            headers: {
                'Authorization': 'Bearer' + payload.token,
            },
            body: data,
        })
            .then((response) => response.json())
            .then((responseData) => {
                dispatch(TipserStatusSuccess(responseData));
            })
            .catch((error) => {
                console.warn("Failll-->",error)
                dispatch(TipserStatusFail(error));
            }).done();
    };
};

export const TipserStatusSuccess = (responseData) => {
    return {
        type: types.TIPSER_STATUS_SUCCESS,
        response: responseData,
    };
};
export const TipserStatusFail = (error) => {
    return {
        type: types.TIPSER_STATUS_FAIL,
        error: error.message,
    };
};



