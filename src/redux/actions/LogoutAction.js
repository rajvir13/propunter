/**
 * Created by Rajnish-React on 8/23/18.
 */
import {
    API,
    BASE_URL, DeviceType,
} from '../constant';
import * as types from '../events';


module.exports.logout = (payload, logoutResponse) => {
/*
    console.warn("chkkk-->",payload)
*/
    fetch(BASE_URL + API.Log_Out, {
        method: 'POST',
        headers: {
            //'Content-Type': 'multipart/form-data',
            'Authorization': 'Bearer' + payload.token,

        },
        //  body: data,
    })
        .then((response) => response.json())
        .then((responseData) => {
            logoutResponse(responseData);
        })
        .catch((error) => {
            logoutResponse(error);
        }).done();
};



