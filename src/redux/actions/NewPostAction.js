/**
 * Created by Rajnish-React on 8/23/18.
 */
import {
    API,
    BASE_URL, DeviceType,
} from '../constant';
import * as types from '../events';

export const NewsPostAction = (payload) => {
    return function (dispatch) {

        let data = new FormData();
        data.append('per_pages', payload.check === 1 ? 3 : payload.perPage);
        fetch(BASE_URL + API.Get_News, {
            method: 'POST',
            headers: {
                'Content-Type': 'multipart/form-data',
                'Authorization': 'Bearer ' + payload.token,
            },
            body: data,
        })
            .then((response) => response.json())
            .then((responseData) => {
                dispatch(NewPostSuccess(responseData));
            })
            .catch((error) => {
                dispatch(NewPostFail(error));
            }).done();
    };
};
export const NewPostSuccess = (responseData) => {
    return {
        type: types.NEW_POST_SUCCESS,
        response: responseData,
    };
};
export const NewPostFail = (error) => {
    return {
        type: types.NEW_POST_FAIL,
        error: error.message,
    };
};


module.exports.TipserPostAction = (payload, tipserPostResponse) => {
    let data = new FormData();
    console.warn('tooo-->',payload)
    data.append('getby', payload.getby);
    fetch(BASE_URL + API.Tipser_Post, {
        method: 'POST',
        headers: {
            'Content-Type': 'multipart/form-data',
            'Authorization': 'Bearer' + payload.token,
        },
        body: data,
    })
        .then((response) => response.json())
        .then((responseData) => {
            tipserPostResponse(responseData);
        })
        .catch((error) => {
            tipserPostResponse(error);
        }).done();
};
