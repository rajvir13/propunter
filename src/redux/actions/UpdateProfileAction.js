/**
 * Created by Rajnish-React on 8/23/18.
 */
import {
    API,
    BASE_URL, DeviceType,
} from '../constant';
import * as types from '../events';

export const UpdateProfileAction = (payload) => {
    console.warn('pauloaddd', payload);
    return function (dispatch) {
        let data = new FormData();
        data.append('first_name', payload.first_name);
        data.append('last_name', payload.last_name);
        data.append('password', payload.password);
        data.append('display_name', payload.display_name);
        data.append('confirm_password', payload.confirm_password);
        data.append('email', payload.email);
        // data.append('profile_image', payload.profile_image);
        fetch(BASE_URL + API.Update_Profile, {
            method: 'POST',
            headers: {
                'Content-Type': 'multipart/form-data',
                'Authorization': 'Bearer' + payload.token,
            },
            body: data,
        })
            .then((response) => response.json())
            .then((responseData) => {
               // console.warn('data', responseData);
                dispatch(UpdateProfileSuccess(responseData));
            })
            .catch((error) => {
                console.warn('error', error);
                dispatch(UpdateProfileFail(error));
            }).done();
    };
};
export const UpdateProfileSuccess = (responseData) => {
    return {
        type: types.UPDATE_PROFILE_SUCCESS,
        response: responseData,
    };
};
export const UpdateProfileFail = (error) => {
    return {
        type: types.UPDATE_PROFILE_FAIL,
        error: error.message,
    };
};


export const GetProfileAction = (payload) => {
    // console.warn("pauloaddd",payload)
    return function (dispatch) {


        fetch(BASE_URL + API.Get_Profile, {
            method: 'POST',
            headers: {
                'Content-Type': 'multipart/form-data',
                'Authorization': 'Bearer' + payload.token,
            },
        })
            .then((response) => response.json())
            .then((responseData) => {
                // console.warn("resposne data",JSON.stringify((responseData)))
                dispatch(UpdateProfileSuccess(responseData));
            })
            .catch((error) => {
                dispatch(UpdateProfileFail(error));
            }).done();
    };
};





