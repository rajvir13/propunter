/**
 * Created by Rajnish-React on 8/23/18.
 */
import {
    API,
    BASE_URL, DeviceType,
} from '../constant';
import * as types from '../events';

export const ChatMessageAction = (payload) => {
    return function (dispatch) {
        let data = new FormData();
        data.append('per_page', payload.per_page);
        data.append('page', payload.page);
        fetch(BASE_URL + API.Get_Chat, {
            method: 'POST',
            headers: {
                'Content-Type': 'multipart/form-data',
                'Authorization': 'Bearer' + payload.token,
            },
            body: data,
        })
            .then((response) => response.json())
            .then((responseData) => {
                dispatch(chatSuccess(responseData));
            })
            .catch((error) => {
                dispatch(chatFail(error));
            }).done();
    };
};
export const chatSuccess = (responseData) => {
    return {
        type: types.CHAT_MESSAGE_SUCCESS,
        response: responseData,
    };
};
export const chatFail = (error) => {
    return {
        type: types.CHAT_MESSAGE_FAIL,
        error: error.message,
    };
};


module.exports.acceptTerm = (payload, acceptTermResponse) => {
    let data = new FormData();
    data.append('status', payload.status);
    fetch(BASE_URL + API.Chat_Term, {
        method: 'POST',
        headers: {
            'Content-Type': 'multipart/form-data',
            'Authorization': 'Bearer' + payload.token,

        },
        body: data,
    })
        .then((response) => response.json())
        .then((responseData) => {
            acceptTermResponse(responseData);
        })
        .catch((error) => {
            acceptTermResponse(error);
        }).done();
};

module.exports.lastSeen = (payload, lastSeenResponse) => {

    //console.warn("chkkk-->",payload)
    fetch(BASE_URL + API.Last_Seen, {
        method: 'POST',
        headers: {
            'Content-Type': 'multipart/form-data',
            'Authorization': 'Bearer' + payload.token,

        },
      //  body: data,
    })
        .then((response) => response.json())
        .then((responseData) => {
            lastSeenResponse(responseData);
        })
        .catch((error) => {
            lastSeenResponse(error);
        }).done();
};