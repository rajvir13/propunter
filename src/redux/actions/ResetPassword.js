/**
 * Created by Rajnish-React on 8/23/18.
 */
import {
    API,
    BASE_URL, DeviceType,
} from '../constant';
import * as types from '../events';

export const ResetPasswordAction = (payload) => {
    return function (dispatch) {
        let data = new FormData();
        data.append('useremail', payload.useremail);
        data.append('resetpassword', payload.resetpassword);
        fetch(BASE_URL + API.Reset_Password, {
            method: 'POST',
            headers: {
                'Content-Type': 'multipart/form-data',
                // 'Authorization': 'Bearer' + payload.token
            },
            body: data,
        })
            .then((response) => response.json())
            .then((responseData) => {
                dispatch(ResetSuccess(responseData));
            })
            .catch((error) => {
                dispatch(ResetFail(error));
            }).done();
    };
};
export const ResetSuccess = (responseData) => {
    return {
        type: types.RESET_SUCCESS,
        response: responseData,
    };
};
export const ResetFail = (error) => {
    return {
        type: types.RESET_FAIL,
        error: error.message,
    };
};
