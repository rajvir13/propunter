/**
 * Created by Rajnish-React on 8/23/18.
 */
import {
    API,
    BASE_URL, DeviceType,
} from '../constant';
import * as types from '../events';

export const LoginAction = (payload) => {
    //console.warn('token succefully ', payload.device_token);
    return function (dispatch) {
        let data = new FormData();
        data.append('useremail', payload.useremail);
        data.append('device_token', payload.device_token);
        data.append('device_type', DeviceType);
        data.append('password', payload.password);
        fetch(BASE_URL + API.Login, {
            method: 'POST',
            headers: {
                'Content-Type': 'multipart/form-data',
            },
            body: data,
        })
            .then((response) => response.json())
            .then((responseData) => {
                console.warn("test", responseData);
                dispatch(LoginSuccess(responseData));
            })
            .catch((error) => {
                dispatch(LoginFail(error));
            }).done();
    };
};
export const LoginSuccess = (responseData) => {
    return {
        type: types.LOGIN_SUCCESS,
        response: responseData,
    };
};
export const LoginFail = (error) => {
    return {
        type: types.LOGIN_FAIL,
        error: error.message,
    };
};












