/**
 * Created by Rajnish-React on 8/23/18.
 */
import {
    API,
    BASE_URL, DeviceType,
} from '../constant';
import * as types from '../events';

export const UpdateIconAction = (payload, UpdateIconResponse) => {
    let data = new FormData();
    data.append('emojiId', payload.id);

    fetch(BASE_URL + API.Update_Icon, {
        method: 'POST',
        headers: {
            'Authorization': 'Bearer' + payload.token,
        },
        body: data,
    })
        .then((response) => response.json())
        .then((responseData) => {
            console.warn('data', responseData);
            UpdateIconResponse(responseData);
        })
        .catch((error) => {
            console.warn('error', error);
            UpdateIconResponse(error);
        }).done();
};








