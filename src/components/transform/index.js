exports.timeDifference = function (previous, hours) {
    let current = new Date();
    let convertedTimeStamp = new Date(previous * 1000);
    let msPerMinute = 60 * 1000;
    let msPerHour = msPerMinute * 60;
    let msPerDay = msPerHour * 24;
    let msPerMonth = msPerDay * 30;
    let msPerYear = msPerDay * 365;
    let properDateFormat = convertedTimeStamp.toDateString().slice(4);

    let elapsed = current - convertedTimeStamp;
    if (elapsed < 0) {
        return 'second ';
    } else if (elapsed < msPerMinute) {
        return Math.round(elapsed / 1000) + ' seconds ago';
    } else if (elapsed < msPerHour) {
        let time = Math.round(elapsed / msPerMinute);
        return (time === 1) ? 'about a minute ago' : time + ' minutes ago';
    } else if (elapsed < msPerDay) {
        let time = Math.round(elapsed / msPerHour);
        return (time === 1) ? 'about an hour ago' : hours + ' TODAY';
    } else if (elapsed < msPerMonth) {
        let time = Math.round(elapsed / msPerDay);
        return (time === 1) ? 'Yesterday' : time + ' days ago';
    } else if (elapsed < msPerYear) {
        return properDateFormat;
    } else {
        return properDateFormat;
    }
};