// ----------------------------------------
// PACKAGE IMPORTS
// ----------------------------------------
import React, { Component } from 'react';
import { View, Image, Text, ScrollView, SafeAreaView, FlatList, TouchableOpacity, StatusBar } from "react-native";
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from "react-native-responsive-screen";
import { CommonActions } from '@react-navigation/native';
import { DRAWER_ICONS } from "../../utils/ImagePaths";
import { Spacer } from '../../components/spacer';
import { COLORS } from '../../themes/Colors'
import styles from '../drawerItem/Styles';



const BottomTab = (props) => {
    const { openSelection, openHome, openFeed, openNews } = props
    return (
        <View style={{
            flexDirection: 'row',
            height: 65,
            paddingTop: hp('1%'),
            fontSize: wp(2.5),
            justifyContent: 'space-around',
            backgroundColor:COLORS.THEME_COLOR,
            alignItems: 'center',
            paddingVertical: hp('2%'),

        }}>
            <TouchableOpacity
                onPress={openHome}
                style={{ alignItems: 'center' }}>
                <Image
                    style={{
                        width: wp("6%"),
                        height: hp("3%"),
                        marginTop: wp(1),
                    }}
                    source={DRAWER_ICONS.HOME_ICON}
                    resizeMode={'contain'} />
                <Spacer space={.6} />
                <Text style={{ color: COLORS.WHITE_COLOR, fontSize: wp(2.7) }}>HOME</Text>
            </TouchableOpacity>
            <TouchableOpacity
                onPress={openSelection}
                style={{ alignItems: 'center' }}>
                <Image
                    style={{
                        width: wp("6%"),
                        height: hp("3%"),
                        marginTop: wp(1),
                    }}
                    source={DRAWER_ICONS.PREVIEW_ICON}
                    resizeMode={'contain'} />
                <Spacer space={.6} />

                <Text style={{ color: COLORS.WHITE_COLOR, fontSize: wp(2.7) }}>SELECTIONS</Text>
            </TouchableOpacity>
            <TouchableOpacity
                onPress={openFeed}
                style={{ alignItems: 'center' }}>

                <Image
                    style={{
                        width: wp("6%"),
                        height: hp("3%"),
                        marginTop: wp(1),
                    }}
                    source={DRAWER_ICONS.FEED_ICON}
                    resizeMode={'contain'} />
                <View style={{
                    position:'absolute',
                    right:-4,
                    height: wp(4.5),
                    alignItems: 'center',
                    justifyContent: 'center',
                    width: wp(5),
                    borderRadius: wp(2),
                    backgroundColor: COLORS.THEME_COLOR,
                    // paddingLeft:wp(2),
                }}>
                    <Text style={{
                        fontSize: wp(2.7),
                        color: COLORS.WHITE_COLOR,
                    }}>{props.chatCount}</Text>
                </View>
                <Spacer space={.6} />

                <Text style={{ color: COLORS.WHITE_COLOR, fontSize: wp(2.7) }}>PPA CHAT</Text>
            </TouchableOpacity>
            <TouchableOpacity
                onPress={openNews}
                style={{ alignItems: 'center' }}>
                <Image
                    style={{
                        width: wp("6%"),
                        height: hp("3%"),
                        marginTop: wp(1),
                    }}
                    source={DRAWER_ICONS.NEWS_ICON}
                    resizeMode={'contain'} />
                <Spacer space={.6} />

                <Text style={{ color: COLORS.WHITE_COLOR, fontSize: wp(2.7) }}>  NEWS</Text>
            </TouchableOpacity>

        </View>
    )
}

export default BottomTab