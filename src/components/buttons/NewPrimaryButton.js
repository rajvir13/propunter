// ----------------------------------------
// PACKAGE IMPORTS
// ----------------------------------------
import React, {Component} from 'react';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import {View, Text, TouchableOpacity} from 'react-native';
// ----------------------------------------
// LOCAL IMPORTS
// ----------------------------------------
import {COLORS} from '../../themes/Colors';
import {FONTNAME} from '../../utils/FontName';


const NewPrimaryButton = (props) => {
    const {label, onButtonPress} = props;
    return (
        <View style={{alignItems: 'center', justifyContent: 'center'}}>
            <TouchableOpacity
                onPress={onButtonPress}
                style={{
                    height: wp(10), alignItems: 'center', justifyContent: 'center',
                    borderRadius: wp(2), width: wp(85),

                    backgroundColor: COLORS.THEME_COLOR,
                }}>
                <Text style={{
                    color: COLORS.WHITE_COLOR,
                    fontSize: wp(4.5), fontWeight: '500', fontFamily: FONTNAME.ProximaNova,
                }}>{label}</Text>
            </TouchableOpacity>
        </View>
    );
};


export default NewPrimaryButton;
