// ----------------------------------------
// PACKAGE IMPORTS
// ----------------------------------------
import React, {Component} from 'react';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import {View, Text, TouchableOpacity} from 'react-native';
// ----------------------------------------
// LOCAL IMPORTS
// ----------------------------------------
import {COLORS} from '../../themes/Colors';
import {FONTNAME} from '../../utils/FontName';


const PrimaryButton = (props) => {
    const {label, onButtonPress} = props;
    return (
        <TouchableOpacity
            onPress={onButtonPress}
            style={{
                height: wp(11), alignItems: 'center', justifyContent: 'center',
                borderRadius: wp(4), borderWidth: 1, borderColor: 'white',

                backgroundColor: COLORS.THEME_COLOR,
            }}>
            <Text style={{
                color: COLORS.WHITE_COLOR,
                fontSize: wp(4.5),
                fontFamily: FONTNAME.ProximaNova,
            }}>{label}</Text>
        </TouchableOpacity>
    );
};

export default PrimaryButton;
