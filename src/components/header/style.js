import React from 'react';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import {StyleSheet, Platform, StatusBar} from 'react-native';
import {COLORS} from '../../themes/Colors';

const styles = StyleSheet.create({
    mainContainer: {
        paddingTop: Platform.OS == 'ios' ? wp(0) : StatusBar.currentHeight,
        backgroundColor: COLORS.THEME_COLOR,
        paddingVertical:  wp(5),
    },
    childContainer: {
       // justifyContent: 'center',
        alignItems: 'center',
        flexDirection: 'row',
        justifyContent: 'space-between',
        paddingHorizontal: wp(3),
    },

});

export default styles;
