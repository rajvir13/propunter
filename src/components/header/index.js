// ----------------------------------------
// PACKAGE IMPORTS
// ----------------------------------------
import React, {Component} from 'react';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import {View, Text, TouchableOpacity} from 'react-native';
import {COLORS} from '../../themes/Colors';
import Icon from 'react-native-vector-icons/MaterialIcons';

// ----------------------------------------
// LOCAL IMPORTS
// ----------------------------------------
import * as RootNavigation from '../../../RootNavigation';
import style from './style';
import Image from 'react-native-remote-svg';
import {SvgUri} from 'react-native-svg';
import {Spacer} from '../spacer';
import SvgComponent from './SvgImage';


Icon.loadFont();
const Header = (props) => {
    const {openDrawer, onPressIcon} = props;
    const openNotification = () => {
        RootNavigation.navigate('notification');
    };

    return (
        <View style={style.mainContainer}>
            <View style={style.childContainer}>
                <Icon
                    style={{paddingTop: wp(4)}}
                    onPress={openNotification}
                    name={'notifications-active'}
                    size={wp('7%')} color={COLORS.WHITE_COLOR}/>
                {/*   <Image
                    resizeMode='contain'
                    source={require('../../assets/images/Header/logo.svg')}
                    style={{width: wp(65), height: wp(17)}}
                />*/}

                <SvgComponent/>
                {/*<SvgUri*/}
                {/*    width={wp(65)}*/}
                {/*    height={wp(17)}*/}
                {/*    uri="https://np.seasiafinishingschool.com/img/logo.svg"*/}
                {/*/>*/}

                {/*<Image source={require('../../assets/logo/logo.png')}/>*/}


                <TouchableOpacity
                    style={{paddingTop: wp(4)}}
                    onPress={openDrawer}>
                    <Image
                        source={require('../../assets/images/Header/menu.png')}/>
                </TouchableOpacity>
            </View>
        </View>
    );
};

export default Header;
