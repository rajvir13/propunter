import React from 'react';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import {StyleSheet, Platform} from 'react-native';
import {COLORS} from '../../themes/Colors';
import {FONTNAME} from '../../utils/FontName';

const styles = StyleSheet.create({
    mainContainer: {
        flex: 1,
    },
    childContainer: {
        flex: 1,
        marginTop: wp(12),
        alignSelf: 'center',
        opacity: 0.1,
        justifyContent: 'center',
        width: wp(60),
        height: wp(80),
        resizeMode: 'contain',
    },
    socialContainer: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'flex-end',
        paddingHorizontal:wp(4),
    },
    drawerItem: {
        flex: 1,
        position: 'absolute',
        left: wp(27),
        top: wp(18),
    },
    drawerItemView: {
        flexDirection: 'row',
        paddingVertical: Platform.OS == 'ios' ? wp(3) : wp(2),
        justifyContent: 'center',
        alignSelf: 'flex-end',
    },
    notificationCount: {
        height: wp(4),
        alignItems: 'center',
        justifyContent: 'center',
        width: wp(6),
        borderRadius: wp(2),
        backgroundColor: COLORS.THEME_COLOR,
    },
    countText: {
        fontSize: wp(2.7),
        color: COLORS.WHITE_COLOR,
    },
    itemText: {
        color: COLORS.WHITE_COLOR,
        fontFamily: FONTNAME.ProximaNova,
        fontSize: wp(4.8),
    },

});

export default styles;
