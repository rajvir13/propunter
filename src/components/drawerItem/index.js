// ----------------------------------------
// PACKAGE IMPORTS
// ----------------------------------------
import React, {useState, useEffect} from 'react';
import {View, Linking, WebView, ImageBackground, Alert, Text, FlatList, TouchableOpacity, Platform} from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import Icon from 'react-native-vector-icons/AntDesign';
import IconSocial from 'react-native-vector-icons/FontAwesome';
import Insta from 'react-native-vector-icons/Entypo';


// ----------------------------------------
// LOCAL IMPORTS
// ----------------------------------------
import {NotificationCount} from '../../redux/actions/NotificationAction';
import {Spacer} from '../../components/spacer';
import {COLORS} from '../../themes/Colors';
import {Strings} from '../../utils/Strings';
import styles from './Styles';

const image = {uri: 'https://reactjs.org/logo-og.png'};
import {CommonActions} from '@react-navigation/native';
import {FONTNAME} from '../../utils/FontName';
import {FONT} from '../../utils/Sizes';
import {logout} from '../../redux/actions/LogoutAction';
import {lastSeen} from '../../redux/actions/ChatMessageAction';

Icon.loadFont();
IconSocial.loadFont();
Insta.loadFont();
const DrawerItemComponent = (props) => {
  const {navigation} = props;
  const [count, setCount] = useState(0);
  const [token, setToken] = useState('');
  /*    const [postId, setPostId] = useState(null);*/

  useEffect(() => {
    AsyncStorage.getItem(Strings.TOKEN, (err, token) => {
      if (token !== undefined && token !== null) {
        setToken(JSON.parse(token));
        NotificationCount({
              token: JSON.parse(token),
            }, res => NotificationCountResponse(res),
        );
      }
    });
  });

  const NotificationCountResponse = (res) => {
    const {navigation} = props;
    //  console.warn("CheckData",res)
    if (res.code == 200 && res.data !== undefined) {
      AsyncStorage.setItem('chatCount', res.data.chat_count);
      setCount(res.data.count);
    }else if (res.code === 'rest_forbidden') {
      navigation.dispatch(
          CommonActions.reset({
            index: 0,
            routes: [
              {
                name: 'login',
              },
            ],
          }),
      );
    }


  };


  const logoutUser = (navigation) => {
    Alert.alert(
        'ProPunter alert',
        Strings.EXIT_ALERT,
        [
          {
            text: 'Cancel', style: 'cancel',
          },
          {
            text: 'OK',
            onPress: () => {
              logout({
                    token: token,
                  }, res => logoutResponse(res),
              );

            },
          },
        ],
        {cancelable: false});
    return true;

  };

  function logoutResponse(res) {
    const {navigation} = props;
    if (res.code === 200) {
      AsyncStorage.clear();
      navigation.dispatch(
          CommonActions.reset({
            routes: [
              {name: 'login'},

            ],
          }),
      );

    } else if (res.code === 'rest_forbidden') {
      navigation.dispatch(
          CommonActions.reset({
            index: 0,
            routes: [
              {
                name: 'login',
              },
            ],
          }),
      );
    } else if (res.code === 403) {


    }
  }


  const _itemClick = (label) => {
    switch (label) {
      case 'HOME':
        navigation.navigate('home');
        break;
      case 'SELECTIONS':
        navigation.navigate('selection');
        break;
      case 'LATEST NEWS':
        navigation.navigate('news');
        break;
      case 'PPA CHAT':
        navigation.navigate('feed',{dummy: "Dummy"});
        break;
      case 'NOTIFICATIONS':
        navigation.navigate('notification');
        break;
      case 'TIPSTERS':
        navigation.navigate('tipstrsprofile');

        break;
      case 'SETTINGS':
        navigation.navigate('settings');
        //commingSoon()

        break;
      case 'LOGOUT':
        logoutUser();
        _closeDrawer();
        break;
      default:
        // code block
    }
  };

  const _closeDrawer = () => {
    const {navigation} = props;
    navigation.closeDrawer();

  };

  function _openFB() {
    Linking.openURL('https://www.facebook.com/propunteralert/');
  }

  function _openTwitter() {
    Linking.openURL('https://twitter.com/ProPunterAlert');
  }

  function _openInsta() {
    Linking.openURL('https://www.instagram.com/pro_punter_alert/');
  }

  return (
      <View style={styles.mainContainer}>
        <ImageBackground source={require('../../assets/images/SplashImages/Group.png')}
                         style={styles.childContainer}>
        </ImageBackground>

        <View style={styles.drawerItem}>
          <View style={{alignSelf: 'flex-end'}}>
            <Icon
                onPress={() => _closeDrawer()}
                name={'close'}
                size={wp('7%')} color={COLORS.WHITE_COLOR}/>
          </View>
          <Spacer space={17}/>
          <FlatList
              scrollEnabled={false}
              keyExtractor={item => item}
              data={[
                {'label': Strings.HOME},
                {'label': Strings.LATEST_PREVIEW},
                {'label': Strings.LATEST_NEWS},
                {'label': Strings.PPA_CHAT},
                {'label': Strings.NOTIFICATIONS},
                {'label': Strings.TIPSTERS},
                {'label': Strings.SETTINGS},
                {'label': Strings.LOGOUT},
              ]}
              renderItem={({item, index}) =>
                  <TouchableOpacity
                      onPress={() => _itemClick(item.label)}
                      style={styles.drawerItemView}>

                    {index === 4 &&

                    <View style={styles.notificationCount}>
                      <Text style={styles.countText}>{count}</Text>
                    </View>
                    }
                    <Spacer row={1}/>

                    <Text style={styles.itemText}>
                      {item.label}
                    </Text>
                  </TouchableOpacity>
              }
          />
          <Spacer space={7}/>

        </View>
        <View style={styles.socialContainer}>
          <IconSocial
              onPress={() => _openFB()}
              name={'facebook'}
              size={wp('6%')} color={COLORS.WHITE_COLOR}/>
          <Spacer row={3}/>
          <IconSocial
              onPress={() => _openTwitter()}
              name={'twitter'}
              size={wp('7%')} color={COLORS.WHITE_COLOR}/>
          <Spacer row={3}/>
          <Insta
              onPress={() => _openInsta()}
              name={'instagram'}
              size={wp('6%')} color={COLORS.WHITE_COLOR}/>
        </View>
        <Spacer space={4}/>
      </View>
  );
};
export default DrawerItemComponent;