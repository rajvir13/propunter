

// ----------------------------------------
// PACKAGE IMPORTS
// ----------------------------------------
import {View, Text, Modal, TouchableOpacity} from 'react-native';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import React, {useState} from 'react';
// ----------------------------------------
// LOCAL IMPORTS
// ----------------------------------------
import {Spacer} from '../../spacer';
import {Strings} from '../../../utils/Strings';
import {COLORS} from '../../../themes/Colors';
import {FONTNAME} from '../../../utils/FontName';

const InnerModal = (props) => {
    const [visible, setVisible] = useState(props.isInnerVisible);
    const userlogout = () => {
        setVisible(true);
        props.userlogout()
    };

    const acceptTerms = () => {
        setVisible(false);
        props.acceptTerms();
    };
    return (
        <Modal
            transparent={true}
            visible={props.isInnerVisible}
            onRequestClose={() => {
                console.log('hhh');
            }}>
            <View
                style={{paddingVertical: wp(65), width: wp(95), paddingLeft: wp(4)}}>
                <View style={{backgroundColor: COLORS.LIGHT_BLACK, borderRadius: wp(3)}}>
                    <View style={{padding: wp(5)}}>
                        <Text style={{
                            textAlign:'center',
                            alignSelf: 'center',
                            color: COLORS.WHITE_SEMI, fontFamily: FONTNAME.ProximaNova, fontSize: wp(4.8),
                        }}>ARE YOU SURE?</Text>
                        <Spacer space={1.7}/>
                        <Text style={{
                            color: COLORS.WHITE_SEMI, fontSize: wp(3.2),
                            fontFamily: FONTNAME.ProximaNovaRegular, alignSelf: 'center',
                            textAlign:'center'
                        }}>
                            Declining our terms and conditions will log you out of the App and notify the admin. This may result in restricted access to our content or account suspension.
                            Are you sure you want to decline our terms and conditions? </Text>
                    </View>
                    <View style={{width: wp(90), height: 1, backgroundColor: COLORS.PLACEHOLDER_TEXT_COLOR}}/>
                    <View style={{flexDirection: 'row', justifyContent: 'space-evenly'}}>
                        <TouchableOpacity
                            onPress={() => userlogout()}
                            style={{alignItems: 'center', justifyContent: 'center'}}>
                            <Text style={{
                                color: COLORS.WHITE_COLOR,
                                fontFamily: FONTNAME.ProximaNovaRegular,
                                fontSize: wp(3.5),
                                textAlign:'center'
                            }}>
                                YES, LOG ME OUT</Text>
                        </TouchableOpacity>
                        <View style={{
                            width: wp(.2), height: wp(13),
                            backgroundColor: COLORS.PLACEHOLDER_TEXT_COLOR,
                        }}/>
                        <TouchableOpacity
                            onPress={() => acceptTerms()}
                            style={{alignItems: 'center', justifyContent: 'center'}}>
                            <Text style={{
                                alignSelf: 'center', fontFamily: FONTNAME.ProximaNovaRegular,
                                color: COLORS.WHITE_COLOR, fontSize: wp(3.5),
                                textAlign:'center'
                            }}>
                                NO, ACCEPT TERMS</Text>
                        </TouchableOpacity>
                    </View>


                </View>


            </View>
        </Modal>
    );
};


export default InnerModal;