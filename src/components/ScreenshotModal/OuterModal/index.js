


// ----------------------------------------
// PACKAGE IMPORTS
// ----------------------------------------
import {View, Text, Modal, TouchableOpacity} from 'react-native';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import React, {useState} from 'react';
// ----------------------------------------
// LOCAL IMPORTS
// ----------------------------------------
import {Spacer} from '../../spacer';
import {Strings} from '../../../utils/Strings';
import {COLORS} from '../../../themes/Colors';
import {FONTNAME} from '../../../utils/FontName';

const OuterModal = (props) => {
    const [visible, setVisible] = useState(props.isVisible);
    const decline = () => {
        setVisible(false);
        props.decline();
    };

    const accept = () => {
        setVisible(false);
        props.accept();
    };
    return (
        <Modal
            transparent={true}
            visible={props.isVisible}
            onRequestClose={() => {
                console.log('hhh');
            }}>
            <View
                style={{paddingVertical: wp(65), width: wp(95), paddingLeft: wp(4)}}>
                <View style={{backgroundColor: COLORS.LIGHT_BLACK, borderRadius: wp(3)}}>
                    <View style={{padding: wp(5)}}>
                        <Text style={{
                            textAlign:'center',
                            alignSelf: 'center',
                            color: COLORS.WHITE_SEMI, fontFamily: FONTNAME.ProximaNova, fontSize: wp(4.8),
                        }}>HOLD UP!</Text>
                        <Spacer space={1.7}/>
                        <Text style={{
                            color: COLORS.WHITE_SEMI, fontSize: wp(3.2),
                            fontFamily: FONTNAME.ProximaNovaRegular, alignSelf: 'center',
                            textAlign:'center'
                        }}>
                            We noticed you just took a screenshot! It’s currently against our terms and conditions to screenshot
                            and share the content that is contained within this App. We’re working hard to give the very best information.
                            Please support us by not sharing our content. </Text>
                    </View>
                    <View style={{width: wp(90), height: 1, backgroundColor: COLORS.PLACEHOLDER_TEXT_COLOR}}/>
                    <View style={{flexDirection: 'row', justifyContent: 'space-evenly'}}>
                        <TouchableOpacity
                            onPress={() => decline()}
                            style={{alignItems: 'center', justifyContent: 'center'}}>
                            <Text style={{
                                color: COLORS.WHITE_COLOR,
                                fontFamily: FONTNAME.ProximaNovaRegular,
                                fontSize: wp(3.5),
                                textAlign:'center'
                            }}>
                                DECLINE</Text>
                        </TouchableOpacity>
                        <View style={{
                            width: wp(.2), height: wp(13),
                            backgroundColor: COLORS.PLACEHOLDER_TEXT_COLOR,
                        }}/>
                        <TouchableOpacity
                            onPress={() => accept()}
                            style={{alignItems: 'center', justifyContent: 'center'}}>
                            <Text style={{
                                textAlign:'center',
                                alignSelf: 'center', fontFamily: FONTNAME.ProximaNovaRegular,
                                color: COLORS.WHITE_COLOR, fontSize: wp(3.5),
                            }}>
                                ACCEPT</Text>
                        </TouchableOpacity>
                    </View>


                </View>


            </View>
        </Modal>
    );
};


export default OuterModal;