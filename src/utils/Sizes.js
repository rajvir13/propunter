import {widthPercentageToDP as wp} from 'react-native-responsive-screen';

export const FONT = {
    TextHero: wp(10),
    TextLarge: wp(8),
    TextNormal: wp(6),
    TextMedium: wp(4.5),
    TextMediumNormal: wp(4.6),
    TextMediumXX: wp(4.8),
    TextMediumX: wp(5),
    TextSmall: wp(4),
    TextSmall_2: wp(3.5),
    TextExtraSmall: wp(3),
    TextXExtraSmall: wp(2.5),
    TextXSmall: wp(1),

};
