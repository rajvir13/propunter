const DRAWER_ICONS = {
    HOME_ICON: require('../assets/images/DrawerIcons/Home.png'),
    FEED_ICON: require('../assets/images/DrawerIcons/Feed.png'),
    NEWS_ICON: require('../assets/images/DrawerIcons/News.png'),
    PREVIEW_ICON: require('../assets/images/DrawerIcons/Preview.png'),
};
const SPLASH_IMAGES = {
    HOME_ICON: require('../assets/images/SplashImages/Group.png'),
    SPLASH_LOADING_GIF: require('../assets/images/SplashImages/loading.gif'),
    LOADING_SVG: require('../assets/images/SplashImages/Loading.svg'),


};

const TUTORIAL_IMAGES = {
    TUTORIAL: require('../assets/images/Tutorial/S1.png'),
    TUTORIAL1: require('../assets/images/Tutorial/S2.png'),
    TUTORIAL2: require('../assets/images/Tutorial/S3.png'),
    TUTORIAL3: require('../assets/images/Tutorial/S4.png'),
    TUTORIAL4: require('../assets/images/Tutorial/S5.png'),

};

export {
    TUTORIAL_IMAGES,
    DRAWER_ICONS,
    SPLASH_IMAGES,
};
