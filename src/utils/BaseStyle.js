import styled from 'styled-components';
import {widthPercentageToDP as wp} from 'react-native-responsive-screen';

import {COLORS} from '../themes/Colors';

import {Platform} from 'react-native';

const isTablet = DeviceInfo.isTablet();

export const MainContainer = styled.View`
flex: 1;
backgroundColor: ${COLORS.WHITE_COLOR};
alignItems: center;
`;

export const ScrollContainer = styled.ScrollView`
flex: 1;
backgroundColor:${COLORS.WHITE_COLOR};
`;

export const ShadowViewContainer = styled(ShadowView)`
 shadowColor: ${COLORS.SHADOW_COLOR};
 shadowOpacity: 1;
 shadowRadius: 3;
 borderRadius: 5;
 shadowOffset: 0px 2px;
 backgroundColor: ${COLORS.WHITE_COLOR};
 marginBottom: ${wp('4%')};
`;

export const BorderViewContainer = styled.View`
 borderColor:${COLORS.GREY_1};
 borderWidth: 1 ;
 borderRadius:10;
 paddingVertical: ${Platform.isPad === true || isTablet === true ? 8 : 4}; 
`;

export const SafeAreaViewContainer = styled(SafeAreaView).attrs(() => ({
    forceInset: {top: 'never'},
}))`
flex:1;
`;
//
// export {
//     MainContainer,
//     ScrollContainer,
//     ShadowViewContainer,
//     SafeAreaViewContainer,
//     BorderViewContainer
// }
