import React from 'react';

export const Strings = {
    // ----------------------------------------
    // LOGIN SCREEN
    // ----------------------------------------
    EMAIL_PLACEHOLDER: 'Email',
    PASSWORD_PLACEHOLDER: 'Password',
    FORGOT_LINK: 'forgot your password?',
    SIGN_IN: 'SIGN IN',
    TEXT_LOADING: 'Loading...',
    EMPTY_EMAIL: 'Email field can not be empty.',
    EMPTY_PASSWORD: 'Password field can not be empty.',
    CONFIRM_PASSWORD: 'Password doest not matched',
    VALID_EMAIL: 'Please enter a valid email. ',
    VALID_PASSWORD: 'Please enter a valid password. ',
    NO_INTERNET: 'Please check your internet connection.',
    TOKEN: 'Auth_token',
    SOMETHING_WENT_WRONG: 'Something went wrong!!',
    SUCSESS: 'User login successfully',


    // ----------------------------------------
    // FORGOT SCREEN
    // ----------------------------------------

    BACK_TO_LOGIN: 'back to login page',
    USER_GETS_DELETED: "All the access has been revoked from the user, Please contact owner.",
    CONTINUE: 'CONTINUE',
    // SUCCESS_FORGOT1: " ",
    // ----------------------------------------
    // CREATE PASSWORD SCREEN
    // ----------------------------------------
    CONFIRM_NEW_PASSWORD_PLACEHOLDER: 'Confirm New Password',
    NEW_PASSWORD_PLACEHOLDER: 'New Password',
    CONFIRM: 'CONFIRM',
    CREATETEXT: 'Create your new password below',
    SUCCESS_PASSWORD: 'Success!',
    SUCCESS_PASSWORD1: 'Your password has now been reset.',

    // ----------------------------------------
    // DRAWER ITEM SCREEN
    // ----------------------------------------
    LATEST_PREVIEW: 'SELECTIONS',
    LATEST_NEWS: 'LATEST NEWS',
    THE_FEED: 'THE FEED',
    PPA_CHAT:'PPA CHAT',
    NOTIFICATIONS: 'NOTIFICATIONS',
    TIPSTERS: 'TIPSTERS',
    SETTINGS: 'SETTINGS',
    LOGOUT: 'LOGOUT',
    HOME: 'HOME',


    // ----------------------------------------
    // TUTORIAL SCREEN
    // ----------------------------------------
    NEXT_BUTTON_TUTORIAL: 'NEXT',
    FINISH: 'FINISH',
    SKIP: 'skip tutorial',
    NEVER_MISS_TIP: 'NEVER MISS A TIP',
    SWITCH_NOTIFICATION: 'Switch on Push Notification and',
    TIP: 'never miss a tip again',
    JOIN_CHAT: 'JOIN THE CHAT',
    LIVE_CHAT: 'Live Chat with other PPA members',
    TIPSER: 'and Tipsters',
    LATEST_NEWS_REVIEWS: 'LATEST NEWS & REVIEWS',
    READ_LATEST: 'Read the latest news and reviews of',
    PPA_PERFORMANCE: 'the PPA performance',
    REFER_MATE: 'REFER A MATE',
    MATE_TO_PPA: 'Refer a mate to PPA and get',
    MONTH_FREE_PLANTINUMM: '$60 worth of merchandise FREE',
    GET_STARTED: 'GET STARTED',
    TIME_START: 'Time to get started! ',
    TERM_CONDITION: 'Hit Finish below',


    // ----------------------------------------
    // HOME SCREEN
    // ----------------------------------------

    LATEST: 'LATEST NEWS',
    ALL: 'ALL',
    EXIT_ALERT: 'Are you sure, you want to logout from the application.',
    TIPSTERS_TITLE: 'PPA TIPSTER TEAM',

    // ----------------------------------------
    // SELECTION SCREEN
    // ----------------------------------------
    NOTIFICATION_ALERT: 'Don`t miss a tip . Switch on Push Notifications to get all the latest updates live.',
    NOTIFICATION_ALERT1: 'Update settings here.',
    TIPSTER: 'VIEW ALL TIPSTERS',
    SELECTION_HEADING: 'SELECTIONS',

    // ----------------------------------------
    // THE FEED
    // ----------------------------------------

    TYPE_MESSAGE: 'Type Message',
    FEED: 'PPA CHAT',
    UNREAD: 'See unread messages',
    // ----------------------------------------
    // Notification
    // ----------------------------------------

    LATEST_NOTIFICATION: 'LATEST NOTIFICATIONS',
    // ----------------------------------------
    // LOGIN SCREEN
    // ----------------------------------------
    LOADING_TEXT: 'LOADING',
    FIRSTNAME: 'Firstname can not be empty.',
    LASTNAME: 'Lastname field can not be empty.',
    DISPALYNAME:'Display Name field can not be empty.',
    CONF_PASSWORD: 'Confirm Password field can not be empty.',
    PASS_MATCH: 'New password and confirm password does not match.',
    ENTER_VALID_PASSWORD: "Password must contain uppercase/lowercase and numbers",
    // ----------------------------------------
    // FORGOT SCREEN
    // ----------------------------------------

    RESET: 'RESET',
    SUCCESS_FORGOT: 'Success! We’ve emailed you a temporary password. Enter it below.',
    TEMP_PASSWORD_PLACEHOLDER: 'Temp Password',


    // ----------------------------------------
    // TUTORIAL SCREEN
    // ----------------------------------------
    MONTH_FREE_PLANTINUM: 'month free platinum access*',
    NOTIFYTEXT: 'Receive push notifications to your device from your favourite Tipsters. You can manage the settings below,',


    // ----------------------------------------
    // SETTINGS SCREEN
    // ----------------------------------------
    APP_SETTINGS: 'APP SETTINGS',
    ACCOUNT_SETTINGS: 'ACCOUNT DETAILS',
    SELECT_ICON:'SELECT YOUR ICON',
    CONTACT_US: 'CONTACT US',
    INTER_NOTIFICATION_TEXT: 'Manage your push notification preference for each Tipster.',
    INTER_ACCOUNT_SETTINGS: 'Reset password and manage your account details.',
    INTER_CONTACT_US: 'Manage your subscription type and payment settings on our website.',
    NAME_TEXT: 'FIRST NAME',
    NAME_HERE: 'Name',
    LAST_NAME: 'LAST NAME',
    DISPLAY_NAME:'DISPLAY NAME',
    DISPLAY_ICON:'DISPLAY ICON',
    CHANGE_ICON:'Change Icon',
    LAST_NAME_HERE: 'Lastname',
    EMAIL_ADDRESS: 'EMAIL ADDRESS',
    EMAIL_ADDRESS_HERE: 'Email Address',
    PASSWORD_TEXT: 'PASSWORD',
    PASSWORD_TEXT_HERE: 'Password',
    CONFIRM_PASS_TEXT: 'CONFIRM PASSWORD',
    CONFIRM_PASS_TEXT_HERE: 'Confirm Password',
    UPDATE_DETAILS: 'UPDATE MY DETAILS',


};
