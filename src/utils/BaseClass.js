/*
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 *  IMPORTS
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 */

// ----------------------------------------
// PACKAGE IMPORTS
// ----------------------------------------
import React, {Component} from 'react';
import {Platform, Linking} from 'react-native';
import Snackbar from 'react-native-snackbar';
import Toast from 'react-native-simple-toast';
import NetInfo from '@react-native-community/netinfo';

// ----------------------------------------
// LOCAL & CONFIG IMPORTS
// ----------------------------------------
import {COLORS} from '../themes/Colors';
import AsyncStorage from '@react-native-community/async-storage';
import {Strings} from './Strings';

// variables.....
let autoRefreshToken;

/*
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 *  MAIN CLASS
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 */
export default class BaseClass extends Component {
    // ----------------------------------------
    // ----------------------------------------
    // CONSTRUCTOR AND LIFE CYCLES
    // ----------------------------------------
    constructor(props) {
        super(props);
        this.isAllow = false;
        this.currentRouteName = 'Home';
        this.token = '';
        this.isInternetConnected = true;

    }


    // ----------------------------------------

    isConnected = () => {


        NetInfo.addEventListener(state => {

            this.isInternetConnected = state.isConnected;

        });
        NetInfo.fetch().then(state => {

            this.isInternetConnected = state.isConnected;

        });
        return this.isInternetConnected;

    };


    _onRefresh = (callback) => {
        let avatarSource;
        AsyncStorage.getItem(Strings.LOGIN_DATA, ((error, data) => {
            if (data !== undefined && data !== null) {
                let userData = JSON.parse(data);
                if (userData !== undefined) {
                    // this.setState({
                    avatarSource = (userData.profilepic !== null) ? userData.profilepic : avatarSource;
                    callback(avatarSource);
                    // })
                }
            }
        }));
    };


    // ----------------------------------------
    // ----------------------------------------
    // DATA METHODS
    // ----------------------------------------
    showToast(title) {
        if (Platform.OS === 'ios') {
            Snackbar.show({
                text: title,
                textColor: COLORS.WHITE_COLOR,
                backgroundColor: COLORS.GREY,
                duration: Snackbar.LENGTH_LONG,
                action: {
                    color: 'white',
                    onPress: () => { /* Do something. */
                    },
                },
            });
        } else {
            Toast.show(title, 3000);
        }
    }

    // ----------------------------------------

    showToastSucess(title) {
        if (Platform.OS === 'ios') {
            Snackbar.show({
                text: title,
                textColor: COLORS.WHITE_COLOR,
                backgroundColor: COLORS.SUCCESS_TOAST,
                duration: Snackbar.LENGTH_LONG,
                action: {
                    color: 'white',
                    onPress: () => { /* Do something. */
                    },
                },
            });
        } else {
            Toast.show(title, 4000);
        }
    }

    // ----------------------------------------

    showToastAlert(title) {
        if (Platform.OS === 'ios') {
            Snackbar.show({
                text: title,
                textColor: COLORS.WHITE_COLOR,
                backgroundColor: COLORS.THEME_COLOR,
                duration: Snackbar.LENGTH_LONG,
                action: {
                    color: COLORS.THEME_COLOR,
                    onPress: () => { /* Do something. */
                    },
                },
            });
        } else {
            Toast.show(title, 3000);
        }
    }

    // ----------------------------------------
    /**
     * start screen loader.....
     * @param isLoading
     */
    showDialog() {
        this.setState({isLoading: true});
    }

    // ----------------------------------------
    /**
     * stop screen loader.....
     * @param isLoading
     */
    hideDialog() {
        this.setState({isLoading: false});
    }


    // ----------------------------------------
    /**
     * make link to device phone.....
     * @param isLoading
     */
    goToDialNumber(phone) {
        let phoneNumber = phone;
        if (Platform.OS !== 'android') {
            phoneNumber = `telprompt:${phone}`;
        } else {
            phoneNumber = `tel:${phone}`;
        }
        Linking.openURL(phoneNumber);
    }


    // ----------------------------------------
    /**
     * auto refreshing token () start & cancel......
     * @param value
     */
    autoRefreshToken(value) {
        if (value === 'start') {
            autoRefreshToken = setInterval(() => {
                this.requestTokenRefresh();
            }, 3000000); // 50 mins
        } else {
            clearInterval(autoRefreshToken);
        }
    }


    /**
     * token () start & cancel......
     * @param value
     */
    deviceToken() {
        return Math.floor(100000 + Math.random() * 900000);
    }

    imageTimestamp() {
        return Math.floor(100000 + Math.random() * 900000);
    }


}
