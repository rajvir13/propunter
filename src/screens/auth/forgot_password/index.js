// ----------------------------------------
// PACKAGE IMPORTS
// ----------------------------------------
import React, {Component} from 'react';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import {
    View,
    Text,
    Image,
    ScrollView,
    SafeAreaView,
    TextInput,
    TouchableWithoutFeedback,
    StatusBar,
} from 'react-native';
import {connect} from 'react-redux';
import AsyncStorage from '@react-native-community/async-storage';

// ----------------------------------------
// LOCAL IMPORTS
// ----------------------------------------
import PrimaryButton from '../../../components/buttons/PrimaryButton';
import {Spacer} from '../../../components/spacer';
import {COLORS} from '../../../themes/Colors';
import style from './ForgotStyle';
import {Strings} from '../../../utils/Strings';
import Validations from '../../../utils/Validations';
import BaseClass from '../../../utils/BaseClass';
import {ForgotAction, VerifyOTPAction} from '../../../redux/actions/ForgotAction';
import OrientationLoadingOverlay from '../../../utils/CustomLoader';

class ForgotScreen extends BaseClass {
    constructor(props) {
        super(props);
        this.state = {
            email: undefined,
            temp_password: undefined,
            success: false,
            token: undefined,
        };
    }


    // ----------------------------------------
    // Class Life cycle method
    // ----------------------------------------
    componentDidMount = () => {
        AsyncStorage.getItem(Strings.TOKEN, (err, token) => {
            if (token !== undefined && token !== null) {
                this.setState(({
                    token: JSON.parse(token),
                }));
            }
        });

    };

    UNSAFE_componentWillReceiveProps = (nextProps) => {
        this.hideDialog();
        const {ForgotResponse} = nextProps.forgotResponse;
        if (ForgotResponse !== undefined && ForgotResponse !== null) {
            if (ForgotResponse.code === 200) {
                this.setState({
                    success: true,
                });
                this.showToastSucess(ForgotResponse.message);
            } else if (ForgotResponse.code === 401) {
                this.showToastAlert(ForgotResponse.message);
            } else {
                this.showToastAlert(Strings.SOMETHING_WENT_WRONG);
            }

        }

    };

    // ----------------------------------------
    // Validation
    // ----------------------------------------

    _onReset = () => {
        const {email, token} = this.state;
        if (email === undefined || email.trim().length === 0) {
            this.showToastAlert(Strings.EMPTY_EMAIL);
        } else if (!Validations.validateEmail(email.trim())) {
            this.showToastAlert(Strings.VALID_EMAIL);
        } else if (!this.isConnected()) {
            this.showToastAlert(Strings.NO_INTERNET);
        } else {
            this.showDialog();
            this.props.forgotPasswordApi({
                useremail: email,
                token: token,
            });

        }
    };

    // ----------------------------------------
    // Verify OTP
    // ----------------------------------------
    _onContinue = () => {
        const {temp_password, email, token} = this.state;

        if (temp_password === undefined || temp_password.trim().length === 0) {
            this.showToastAlert(Strings.EMPTY_PASSWORD);
        } else if (!this.isConnected()) {
            this.showToastAlert(Strings.NO_INTERNET);
        } else {
            this.showDialog();
            VerifyOTPAction({
                    useremail: email,
                    userotp: temp_password,
                    token: token,
                }, res => this.verifyOTPResponse(res),
            );

        }
    };
    verifyOTPResponse = (response) => {
        const {email} = this.state;
        this.hideDialog();
        const {navigate} = this.props.navigation;
        if (response !== undefined && response !== null) {
            if (response.code === 200) {
                this.showToastSucess(response.message);
                navigate('createPassword', {useremail: email});
            } else if (response.code === 401) {
                this.showToastAlert(response.message);
            } else {
                this.showToastAlert(Strings.SOMETHING_WENT_WRONG);
            }

        }

    };


    /*
          // =============================================================================================
          // Render method for Custom Loader
          // =============================================================================================
          */
    _renderCustomLoader = () => {
        const {isLoading} = this.state;
        return (
            <OrientationLoadingOverlay visible={isLoading} message={Strings.LOADING_TEXT}/>
        );
    };


    // ----------------------------------------
    // Main render
    // ----------------------------------------
    render() {
        const {goBack} = this.props.navigation;
        const {success} = this.state;
        return (
            <SafeAreaView style={{flex: 1, backgroundColor: COLORS.THEME_COLOR}}>
                <StatusBar barStyle="light-content" hidden={false} backgroundColor={COLORS.THEME_COLOR}
                           translucent={true}/>

                <ScrollView style={{backgroundColor: COLORS.THEME_COLOR}}>
                    <View style={style.mainContainer}>
                        <View>
                            <Image source={require('../../../assets/logo/logo.png')}
                                   style={{}}/>
                        </View>
                        <Spacer space={10}/>
                        {!success ?
                            <View>
                                <TextInput style={style.textInputStyle}
                                           onSubmitEditing={() => {
                                               this._onReset();
                                           }}
                                           underlineColorAndroid="transparent"
                                           placeholder={Strings.EMAIL_PLACEHOLDER}
                                           placeholderTextColor={COLORS.PLACEHOLDER_COLOR}
                                           autoCapitalize="none"
                                           onChangeText={text => this.setState({
                                               email: text,
                                           })}
                                />
                                <Spacer space={3}/>
                                <PrimaryButton
                                    onButtonPress={() => this._onReset()}
                                    label={Strings.RESET}/>
                            </View>
                            :
                            <View style={style.successView}>
                                <Text style={style.successText}>{Strings.SUCCESS_FORGOT}</Text>
                                {/* <Text style={style.successText}>{Strings.SUCCESS_FORGOT1}</Text> */}
                                <Spacer space={3}/>
                                <TextInput style={style.textInputStyle}
                                           onSubmitEditing={() => {
                                               this._onContinue();
                                           }}
                                           keyboardType='numeric'
                                           underlineColorAndroid="transparent"
                                           placeholder={Strings.TEMP_PASSWORD_PLACEHOLDER}
                                           placeholderTextColor={COLORS.PLACEHOLDER_COLOR}
                                           autoCapitalize="none"
                                           onChangeText={text => this.setState({
                                               temp_password: text,
                                           })}/>
                                <Spacer space={3}/>
                                <PrimaryButton
                                    onButtonPress={() => this._onContinue()}
                                    label={Strings.CONTINUE}/>
                                <Spacer space={4}/>
                            </View>
                        }
                        <Spacer space={10}/>
                        <Text
                            onPress={() => goBack()}
                            style={style.forgotText}>{Strings.BACK_TO_LOGIN}</Text>
                    </View>
                    {this._renderCustomLoader()}
                </ScrollView>
            </SafeAreaView>

        );
    }
}


// ----------------------------------------
// ----------------------------------------
// CONNECT
// ----------------------------------------

const mapStateToProps = state => ({
    forgotResponse: state.ForgotReducer,

});

// ----------------------------------------

const mapDispatchToProps = (dispatch) => {
    return {
        forgotPasswordApi: (payload) => dispatch(ForgotAction(payload)),
    };
};

// ----------------------------------------

export default connect(mapStateToProps, mapDispatchToProps)(ForgotScreen);