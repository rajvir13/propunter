import React from 'react';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import {StyleSheet} from 'react-native';
import {COLORS} from '../../../../../themes/Colors';
import {FONTNAME} from '../../../../../utils/FontName';

const styles = StyleSheet.create({
    mainContainer: {
        borderRadius: 15,
        borderWidth: 1,
        width: wp(90),
        borderColor: COLORS.WHITE_COLOR,
        alignItems: 'center',
        backgroundColor: COLORS.LIGHT1_GREY,
    },
    nextButton: {
        fontSize: wp(4),
        color: COLORS.THEME_COLOR,
        fontFamily: FONTNAME.ProximaNova,
    },

});

export default styles;
