// ----------------------------------------
// PACKAGE IMPORTS
// ----------------------------------------
import React, {Component} from 'react';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import {View, Text, Image, TouchableOpacity, Platform} from 'react-native';
import Icon from 'react-native-vector-icons/AntDesign';

// ----------------------------------------
// LOCAL IMPORTS
// ----------------------------------------
import {Spacer} from '../../../../../components/spacer';
import {COLORS} from '../../../../../themes/Colors';
import {Strings} from '../../../../../utils/Strings';
import style from './style';
import {FONTNAME} from '../../../../../utils/FontName';
import {FONT} from '../../../../../utils/Sizes';

Icon.loadFont();

const SwiperItem = (props) => {
    const {header, subHeader, finish, dot, onPressNext, subHeader1, onPressFinish,TutorialShow} = props;
    return (
        <View style={style.mainContainer}>
            <Spacer space={2}/>
            <View style={{alignItems: 'center', width: wp(80)}}>
                <Spacer space={4}/>
                <Text style={{
                    color: COLORS.PLACEHOLDER_TEXT_COLOR, fontFamily: FONTNAME.ProximaNova,
                    fontSize: FONT.TextMediumXX,
                }}>{header}</Text>
                <Spacer space={2}/>
                <Text
                    style={{color: COLORS.GREY, fontFamily: FONTNAME.ProximaNova, fontSize: wp(3.5)}}>{subHeader}</Text>
                <Text style={{
                    color: COLORS.GREY,
                    fontFamily: FONTNAME.ProximaNova,
                    fontSize: wp(3.5),
                }}>{subHeader1}</Text>
            </View>
            {!finish &&
            <Image
                resizeMode='contain'
                style={{height: wp(70), width: wp(80)}}
                //source={require('../../../../../assets/images/Tutorial/notification.gif')}
                source={TutorialShow}
            />
            }
            {!finish ?
                <View>
                    <Image
                        resizeMode='contain'
                        style={{height: wp(4), width: wp(20)}}
                        source={dot}
                    />
                    <Spacer space={3}/>
                    <TouchableOpacity
                        onPress={onPressNext}
                        style={{flexDirection: 'row', justifyContent: 'center', alignItems: 'center'}}><Text
                        style={style.nextButton}>{Strings.NEXT_BUTTON_TUTORIAL}</Text>
                        <Spacer row={.3}/>
                        <Icon
                            // style={{marginTop:1}}
                            name={'right'}
                            size={wp('4%')} color={COLORS.THEME_COLOR}/>
                    </TouchableOpacity>
                    <Spacer space={2.5}/>
                </View>
                :
                <View>
                    <Spacer space={5}/>
                    <Image
                        resizeMode='contain'
                        style={{height: wp(4), width: wp(20)}}
                        source={dot}
                    />
                    <Spacer space={3}/>
                    <TouchableOpacity
                        onPress={onPressFinish}
                        style={{flexDirection: 'row', justifyContent: 'center', alignItems: 'center'}}><Text
                        style={style.nextButton}>{Strings.FINISH}</Text>
                        <Spacer row={.3}/>
                        <Icon
                            style={{}}
                            name={'right'}
                            size={wp('4%')} color={COLORS.THEME_COLOR}/>
                    </TouchableOpacity>
                    <Spacer space={5}/>

                </View>

            }

        </View>
    );
};

export default SwiperItem;
