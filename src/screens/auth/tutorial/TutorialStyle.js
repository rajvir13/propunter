import React from 'react';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import {StyleSheet, Platform} from 'react-native';
import {COLORS} from '../../../themes/Colors';
import {FONTNAME} from '../../../utils/FontName';
import {FONT} from '../../../utils/Sizes';

const styles = StyleSheet.create({
    mainContainer: {
        paddingHorizontal: wp(5),
        justifyContent: 'center',
        alignItems: 'center',
        flex: 1,
        borderColor: 'red',
        // paddingVertical: wp(20),
    },
    dot: {
        backgroundColor: 'rgba(0,0,0,.2)',
        width: 8, height: 8, borderRadius: 4,
        marginLeft: 3, marginRight: 3, marginTop: 3,
        marginBottom: wp(10),
    },
    wrapperButton: {
        flexDirection: 'row',
        position: 'absolute', top: Platform.OS == 'ios' ? hp(29) : wp(48),
        left: wp(-34),
    },
    nextButton: {
        fontSize: FONT.TextMediumXX,
        color: COLORS.THEME_COLOR,
        fontFamily: FONTNAME.ProximaNova,
    },
    skip: {
        color: COLORS.WHITE_COLOR,
        fontSize: wp(4),
        fontFamily: FONTNAME.ProximaNovaregular,
    },
});

export default styles;
