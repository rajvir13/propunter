// ----------------------------------------
// PACKAGE IMPORTS
// ----------------------------------------
import React, {Component} from 'react';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import {View, Text, ScrollView, SafeAreaView, TextInput, TouchableWithoutFeedback, StatusBar} from 'react-native';
import Icon from 'react-native-vector-icons/AntDesign';
import GestureRecognizer, {swipeDirections} from 'react-native-swipe-gestures';
import {CommonActions} from '@react-navigation/native';

// ----------------------------------------
// LOCAL IMPORTS
// ----------------------------------------
import {TUTORIAL_IMAGES} from '../../../utils/ImagePaths';
import {COLORS} from '../../../themes/Colors';
import {Spacer} from '../../../components/spacer';
import SwiperItem from './component/swiperItem';
import style from './TutorialStyle';
import {Strings} from '../../../utils/Strings';
import OrientationLoadingOverlay from '../../../utils/CustomLoader';
import BaseClass from '../../../utils/BaseClass';

Icon.loadFont();

class Tutorial2Screen extends BaseClass {
    constructor(props) {
        super(props);
        this.state = {
            isLoading: false,
            index: undefined,
        };
    }

    _next = (index) => {
        const {navigate} = this.props.navigation;
        if (index == 4) {

            // setTimeout(() => {
            //     navigate('home')
            // }, 800)

        }

    };


    /*
 // =============================================================================================
 // Render method for Custom Loader
 // =============================================================================================
 */
    _renderCustomLoader = () => {
        const {isLoading} = this.state;
        return (
            <OrientationLoadingOverlay visible={isLoading} message={Strings.TEXT_LOADING}/>
        );
    };


    _goToHome = () => {
        const {navigate} = this.props.navigation;
        this.props.navigation.dispatch(
            CommonActions.reset({
                index: 0,
                routes: [
                    {name: 'home'},
                ],
            }),
        );

    };

    onSwipeRight(gestureState) {

        this.setState({myText: 'You swiped right!'});
    }

    onSwipe(gestureName, gestureState) {
        const {SWIPE_UP, SWIPE_DOWN, SWIPE_LEFT, SWIPE_RIGHT} = swipeDirections;
        this.setState({gestureName: gestureName});
        switch (gestureName) {
            case SWIPE_UP:
                this.props.navigation.navigate('tutorial3');
                break;
            case SWIPE_DOWN:
                this.props.navigation.navigate('tutorial3');
                break;
            case SWIPE_LEFT:
                this.props.navigation.navigate('tutorial3');
                break;
            case SWIPE_RIGHT:
                this.props.navigation.navigate('tutorial1');
                break;
        }
    }

    // ----------------------------------------
    // Main render
    // ----------------------------------------

    render() {
        const {navigate} = this.props.navigation;
        const config = {
            velocityThreshold: 0.3,
            directionalOffsetThreshold: 80,
        };

        return (
            <SafeAreaView style={{flex: 1, backgroundColor: COLORS.THEME_COLOR}}>
                <StatusBar barStyle="light-content" hidden={false} backgroundColor={COLORS.THEME_COLOR}
                           translucent={true}/>
                <View style={[style.mainContainer, {paddingVertical: wp(10)}]}>
                    <GestureRecognizer
                        onSwipe={(direction, state) => this.onSwipe(direction, state)}
                        //onSwipeLeft={(state) => this.onSwipeLeft(state)}
                        onSwipeRight={(state) => this.onSwipeRight(state)}
                        config={config}
                        style={{
                            flex:.9,
                            alignItems: "center",
                            justifyContent: "center",
                            backgroundColor: COLORS.THEME_COLOR,

                        }}
                    >
                        <View>
                            <SwiperItem
                                dot={TUTORIAL_IMAGES.TUTORIAL2}
                                onPressNext={() => this.props.navigation.navigate('tutorial3')}
                                header={Strings.LATEST_NEWS_REVIEWS}
                                subHeader={Strings.READ_LATEST}
                                subHeader1={Strings.PPA_PERFORMANCE}
                                TutorialShow={require('../../../assets/images/Tutorial/news.gif')}

                            />
                        </View>
                    </GestureRecognizer>

                    <Spacer space={4}/>
                    <Text
                        onPress={() => this._goToHome()}
                        // () => navigate('home')}
                        style={style.skip}>{Strings.SKIP}</Text>
                </View>
            </SafeAreaView>

        );
    }
}

export default Tutorial2Screen;
