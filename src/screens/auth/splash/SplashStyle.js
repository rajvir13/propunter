import React from 'react';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import {StyleSheet, Platform} from 'react-native';
import {COLORS} from '../../../themes/Colors';

const styles = StyleSheet.create({
    mainContainer: {
        backgroundColor: COLORS.THEME_COLOR,
        justifyContent: 'center',
        alignItems: 'center',
        flex: 1,
    },

});

export default styles;
