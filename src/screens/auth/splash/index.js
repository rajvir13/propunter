/*
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 *  IMPORTS
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 */

// ----------------------------------------
// PACKAGE IMPORTS
// ----------------------------------------

import React from 'react';

// ----------------------------------------
// LOCAL & CONFIG IMPORTS
// ----------------------------------------
import BaseClass from '../../../utils/BaseClass';
import {COLORS} from '../../../themes/Colors';
import style from './SplashStyle';
import {SPLASH_IMAGES} from '../../../utils/ImagePaths';
import {View, StatusBar} from 'react-native';
import {Strings} from '../../../utils/Strings';
import OrientationLoadingOverlay from '../../../utils/CustomLoader';
import {CommonActions} from '@react-navigation/native';
import AsyncStorage from '@react-native-community/async-storage';
import Image from 'react-native-remote-svg';
import {
    widthPercentageToDP as wp,
    heightPercentageToDP as hp,
} from 'react-native-responsive-screen';

/*
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 *  MAIN CLASS
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 */
class Splash extends BaseClass {

    // ----------------------------------------
    // ----------------------------------------
    // CONSTRUCTOR AND LIFE CYCLES
    // ----------------------------------------
    constructor(props) {
        super(props);
        this.state = {

            isLoading: false,


        };
    }

    componentDidMount() {

        const {navigate} = this.props.navigation;
        // this.showDialog();

        AsyncStorage.getItem(Strings.TOKEN, (err, token) => {
            if (token !== undefined && token !== null) {
                this.props.navigation.dispatch(
                    CommonActions.reset({
                        index: 0,
                        routes: [
                            {name: 'home'},
                        ],
                    }),
                );

            } else {
                setTimeout(() => {
                    this.props.navigation.dispatch(
                        CommonActions.reset({
                            index: 0,
                            routes: [
                                {name: 'login'},
                            ],
                        }),
                    );
                    /*  navigate('login');
                      this.hideDialog();*/
                }, 3000);
            }
        });


    }


    /*
// =============================================================================================
// Render method for Custom Loader
// =============================================================================================
*/
    _renderCustomLoader = () => {
        const {isLoading} = this.state;
        return (
            <OrientationLoadingOverlay visible={isLoading} message={Strings.LOADING_TEXT}/>
        );
    };

    // ----------------------------------------
    // ----------------------------------------
    // MAIN RENDER
    // ----------------------------------------
    render() {
        return (

            <View style={style.mainContainer}>
                <StatusBar barStyle="light-content" hidden={false} backgroundColor={COLORS.THEME_COLOR}
                           translucent={true}/>
                <Image
                    resizeMode='contain'
                    source={require('../../../assets/images/SplashImages/Loading.svg')}
                   style={{ width: wp(65), height: wp(70) }}
                />
            </View>
        );
    }
}

export default Splash;
