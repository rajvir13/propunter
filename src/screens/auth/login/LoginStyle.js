import React from 'react';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import {StyleSheet} from 'react-native';
import {COLORS} from '../../../themes/Colors';
import {FONTNAME} from '../../../utils/FontName';

const styles = StyleSheet.create({
    mainContainer: {
        alignItems: 'center',
        backgroundColor: COLORS.THEME_COLOR,
        justifyContent: 'center',
        height: hp(100),
    },
    logoImageStyle: {},
    textInputStyle: {
        width: wp(85),
        height: wp(11),
        textAlign: 'center',
        borderRadius: wp(3.5),
        borderColor: COLORS.WHITE_COLOR,
        borderWidth: 1,
        fontSize: wp(4.5),
        backgroundColor: COLORS.WHITE_COLOR,
        fontFamily: FONTNAME.ProximaNova,

    },
    forgotText: {
        color: COLORS.WHITE_COLOR,
        fontSize: wp(4.5),
        fontFamily: FONTNAME.ProximaNova,
    },
    underline: {
        width: wp(100), height: wp(.3), backgroundColor: '#707070',
    },
});

export default styles;
