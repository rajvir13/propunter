// ----------------------------------------
// PACKAGE IMPORTS
// ----------------------------------------
import React, {Component} from 'react';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import {
    View,
    Text,
    ScrollView,
    SafeAreaView,
    Image,
    TextInput,
    KeyboardAvoidingView,
    Keyboard,
    StatusBar, Linking,
} from 'react-native';
import {connect} from 'react-redux';
import AsyncStorage from '@react-native-community/async-storage';
import {CommonActions} from '@react-navigation/native';
import PushNotification from 'react-native-push-notification';

// ----------------------------------------
// LOCAL IMPORTS
// ----------------------------------------
import PrimaryButton from '../../../components/buttons/PrimaryButton';
import {Spacer} from '../../../components/spacer';
import {COLORS} from '../../../themes/Colors';
import style from './LoginStyle';
import {Strings} from '../../../utils/Strings';
import Validations from '../../../utils/Validations';
import styles from './LoginStyle';
import {LoginAction} from '../../../redux/actions/LoginAction';
import BaseClass from '../../../utils/BaseClass';
import OrientationLoadingOverlay from '../../../utils/CustomLoader';
import {CheckBox} from 'react-native-elements';
import Insta from 'react-native-vector-icons/Entypo';
import {FONTNAME} from '../../../utils/FontName';
import {FONT} from '../../../utils/Sizes';


class LoginScreen extends BaseClass {
    constructor(props) {
        super(props);
        this.state = {
            email: undefined,
            password: undefined,
            isLoading: false,
            success: false,
            fcmToken: undefined,
            checked: false,
        };
    }


    // ----------------------------------------
    // Life cycle method
    // ----------------------------------------

    componentDidMount = () => {


        PushNotification.configure({
            // (optional) Called when Token is generated (iOS and Android)
            onRegister: function (token) {
                AsyncStorage.setItem('notification', token.token);
            },
        });
    };
    UNSAFE_componentWillReceiveProps = (nextProps) => {
        const {loginResponse} = nextProps;
        //  console.warn('login ::::', loginResponse);
        const {navigate} = this.props.navigation;
        if (loginResponse !== undefined && loginResponse !== null) {
            AsyncStorage.setItem(Strings.TOKEN, JSON.stringify(loginResponse.token));
            if (loginResponse.code === 200) {
                this.hideDialog();
                if (loginResponse.code === 200) {
                    this.setState({success: true});
                }
                setTimeout(() => {
                    this.props.navigation.dispatch(
                        CommonActions.reset({
                            index: 0,
                            routes: [
                                {name: 'tutorial'},
                            ],
                        }),
                    );
                }, 1000);

            } else if (loginResponse.code === 400) {
                this.hideDialog();
                this.showToastAlert(loginResponse.message);
            } else {
                this.hideDialog();
                this.showToastAlert(Strings.SOMETHING_WENT_WRONG);
            }

        } else {
            this.hideDialog();
            this.showToastAlert(Strings.SOMETHING_WENT_WRONG);

        }
    };

    // ----------------------------------------
    // Validation
    // ----------------------------------------

    _signin = () => {
        AsyncStorage.getItem('notification', (err, fcmToken) => {
            const {navigate} = this.props.navigation;
            const {email, password, checked} = this.state;
            if (email === undefined || email.trim().length === 0) {
                this.showToastAlert(Strings.EMPTY_EMAIL);
            } else if (!Validations.validateEmail(email.trim())) {
                this.showToastAlert(Strings.VALID_EMAIL);
            } else if (password === undefined || password.trim().length === 0) {
                this.showToastAlert(Strings.EMPTY_PASSWORD);
            } else if (checked === false) {
                this.showToastAlert('Please accept terms and conditions');
            } else if (!this.isConnected()) {

                this.showToastAlert(Strings.NO_INTERNET);
            } else {
                this.showDialog();
                this.props.loginApi({
                    useremail: email,
                    password: password,
                    device_token: fcmToken,
                });
            }
        });
    };
    /*
       // =============================================================================================
       // Render method for Custom Loader
       // =============================================================================================
       */
    _renderCustomLoader = () => {
        const {isLoading} = this.state;
        return (
            <OrientationLoadingOverlay visible={isLoading} message={Strings.LOADING_TEXT}/>
        );
    };

    openPressTerms = () => {
        Linking.openURL('https://www.propunteralert.com/terms-and-conditions');
    };


    // ----------------------------------------
    // Main render
    // ----------------------------------------

    render() {
        const {navigate} = this.props.navigation;
        const {success, checked} = this.state;
        return (

            <KeyboardAvoidingView
                style={{flex: 1, backgroundColor: COLORS.WHITE_COLOR}}
                behavior={(Platform.OS === 'ios') ? 'padding' : null}>
                <StatusBar barStyle="light-content" hidden={false} backgroundColor={COLORS.THEME_COLOR}
                           translucent={true}/>

                <SafeAreaView style={{flex: 1, backgroundColor: COLORS.THEME_COLOR}}>
                    <ScrollView style={{backgroundColor: COLORS.THEME_COLOR}}>
                        {
                            !success ?
                                <View style={style.mainContainer}>
                                    <View>
                                        <Image source={require('../../../assets/logo/logo.png')}
                                               style={styles.logoImageStyle}/>
                                    </View>
                                    <Spacer space={10}/>
                                    <View>
                                        <TextInput style={style.textInputStyle}
                                                   underlineColorAndroid="transparent"
                                                   placeholder={Strings.EMAIL_PLACEHOLDER}
                                                   placeholderTextColor={COLORS.PLACEHOLDER_COLOR}
                                                   autoCapitalize="none"
                                                   keyboardType="email-address"
                                                   returnKeyType={'next'}
                                                   onSubmitEditing={() => {
                                                       this.refs.Password.focus();
                                                   }}
                                                   onChangeText={text => this.setState({
                                                       email: text,
                                                   })}
                                        />
                                        <Spacer space={3}/>
                                        <TextInput style={style.textInputStyle}
                                                   ref="Password"
                                                   onSubmitEditing={() => {
                                                       this._signin();
                                                   }}
                                                   underlineColorAndroid="transparent"
                                                   placeholder={Strings.PASSWORD_PLACEHOLDER}
                                                   placeholderTextColor={COLORS.PLACEHOLDER_COLOR}
                                                   autoCapitalize="none"
                                                   secureTextEntry={true}
                                                   onChangeText={text => this.setState({
                                                       password: text,
                                                   })}
                                        />
                                        <Spacer space={4}/>

                                        <View style={{
                                            flexDirection: 'row',
                                            alignContent: 'center',
                                            alignItems: 'center',
                                        }}>

                                            <CheckBox
                                                containerStyle={{
                                                    backgroundColor: COLORS.THEME_COLOR,
                                                    marginRight: 0,
                                                    paddingVertical: 2,
                                                }}
                                                checkedColor={COLORS.WHITE_COLOR}
                                                /*  textStyle={{
                                                      color: COLORS.WHITE_SEMI,
                                                      fontFamily: FONTNAME.ProximaNova,
                                                      fontSize: FONT.TextSmall,
                                                  }}*/
                                                //   title='I agree to the PPA TERMS & Conditions'
                                                checked={checked}
                                                /*  onPress={() => _openTerConditions()}*/
                                                onIconPress={() => this.setState(
                                                    {
                                                        checked: !this.state.checked,
                                                    },
                                                )}
                                                //onPress={() => this.openPressTerms()}
                                            />

                                            <Text
                                                style={{

                                                    color: COLORS.WHITE_SEMI,
                                                    fontFamily: FONTNAME.ProximaNova,
                                                    fontSize: FONT.TextSmall_2,

                                                }}
                                            >
                                                I agree to the <Text style={{
                                                color: COLORS.WHITE_SEMI,
                                                fontFamily: FONTNAME.ProximaNova,
                                                fontSize: FONT.TextSmall_2,
                                                textDecorationLine: 'underline',
                                                textDecorationColor: COLORS.WHITE_SEMI,
                                                textDecorationStyle: 'double',

                                                borderWidth: 10,

                                            }} onPress={() => this.openPressTerms()}>PPA Terms & Conditions</Text>

                                            </Text>
                                        </View>
                                        <Spacer space={4}/>
                                        <PrimaryButton
                                            onButtonPress={() => this._signin()}
                                            label={Strings.SIGN_IN}/>
                                    </View>
                                    <Spacer space={10}/>
                                    <Text
                                        onPress={() => navigate('forgot')}
                                        style={style.forgotText}>{Strings.FORGOT_LINK}
                                    </Text>
                                </View> :
                                <View style={{
                                    justifyContent: 'center',
                                    flex: 1,
                                    alignItems: 'center',
                                    paddingVertical: wp(70),
                                }}>
                                    <Text style={{color: COLORS.WHITE_COLOR}}>JUST A SEC</Text>
                                    <Text style={{color: COLORS.WHITE_COLOR}}>WHILE LOGGING YOU IN</Text>
                                </View>
                        }

                        {this._renderCustomLoader()}

                    </ScrollView>
                </SafeAreaView>
            </KeyboardAvoidingView>
        );
    }
}


// ----------------------------------------
// ----------------------------------------
// CONNECT
// ----------------------------------------

const mapStateToProps = state => ({
    loginResponse: state.LoginReducer.loginResponse,

});

// ----------------------------------------

const mapDispatchToProps = (dispatch) => {
    return {
        loginApi: (payload) => dispatch(LoginAction(payload)),
    };
};

// ----------------------------------------

export default connect(mapStateToProps, mapDispatchToProps)(LoginScreen);
