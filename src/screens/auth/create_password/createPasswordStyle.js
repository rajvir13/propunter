import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import {StyleSheet} from 'react-native';
import {COLORS} from '../../../themes/Colors';
import {FONTNAME} from '../../../utils/FontName';

const styles = StyleSheet.create({
    mainContainer: {
        alignItems: 'center',
        backgroundColor: COLORS.THEME_COLOR,
        justifyContent: 'center',
        height: hp(100),
    },
    textInputStyle: {
        width: wp(85),
        height: wp(11),
        textAlign: 'center',
        borderRadius: wp(3.5),
        borderColor: COLORS.WHITE_COLOR,
        borderWidth: 1,
        fontSize: wp(4.4),
        fontFamily: FONTNAME.ProximaNova,
        backgroundColor: COLORS.WHITE_COLOR,
    },
    createTitleText:
        {
            color: COLORS.WHITE_COLOR,
            fontSize: wp(4.4),
            fontFamily: FONTNAME.ProximaNova,
            textAlign: 'center',
        },
    forgotText: {
        color: COLORS.WHITE_COLOR,
        fontSize: wp(4.4),
        fontFamily: FONTNAME.ProximaNova,
    },
    successView: {
        width: wp(85),
        justifyContent: 'center',
    },
    successText: {
        color: COLORS.WHITE_COLOR,
        fontSize: wp(4.4),
        fontFamily: FONTNAME.ProximaNova,
        textAlign: 'center',
    },
});

export default styles;
