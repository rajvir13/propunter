// ----------------------------------------
// PACKAGE IMPORTS
// ----------------------------------------
import React, {Component} from 'react';
import {View, Text, Image, ScrollView, SafeAreaView, TextInput, KeyboardAvoidingView, StatusBar} from 'react-native';
import {connect} from 'react-redux';
import AsyncStorage from '@react-native-community/async-storage';
// ----------------------------------------
// LOCAL IMPORTS
// ----------------------------------------
import PrimaryButton from '../../../components/buttons/PrimaryButton';
import {Spacer} from '../../../components/spacer';
import {COLORS} from '../../../themes/Colors';
import styles from './createPasswordStyle';
import {Strings} from '../../../utils/Strings';
import BaseClass from '../../../utils/BaseClass';
import {ResetPasswordAction} from '../../../redux/actions/ResetPassword';
import OrientationLoadingOverlay from '../../../utils/CustomLoader';
import Validations from '../../../utils/Validations';
import style from '../forgot_password/ForgotStyle';


class CreatePasswordScreen extends BaseClass {
    constructor(props) {
        super(props);
        this.state = {
            password: undefined,
            confirm_password: undefined,
            success: false,
            token: undefined,
            useremail: this.props.route.params.useremail,
        };
    }

    // ----------------------------------------
    // Class life cycle
    // ----------------------------------------

    componentDidMount = () => {
        AsyncStorage.getItem(Strings.TOKEN, (err, token) => {
            if (token !== undefined && token !== null) {
                this.setState(({
                    token: JSON.parse(token),
                }));
            }
        });
    };

    componentWillReceiveProps = (nextProps) => {
        this.hideDialog();
        const {ResetResponse} = nextProps.resetPasswordResponse;
        if (ResetResponse !== undefined && ResetResponse !== null) {
            if (ResetResponse.code === 200) {
                this.setState({
                    success: true,
                });
                this.showToastSucess(ResetResponse.message);
            } else if (ResetResponse.code === 401) {
                this.showToastAlert(ResetResponse.message);
            } else {
                this.showToastAlert(Strings.SOMETHING_WENT_WRONG);
            }

        }
    };

    // ----------------------------------------
    // Validations
    // ----------------------------------------

    _onConfrim() {
        const {password, confirm_password, token, useremail} = this.state;
        if (password === undefined || password.length === 0) {
            this.showToastAlert(Strings.EMPTY_PASSWORD);
        }else if (!Validations.validatePassword(password.trim())) {
            this.showToastAlert(Strings.ENTER_VALID_PASSWORD);

        } else if (confirm_password === undefined || confirm_password.length === 0) {
            this.showToastAlert(Strings.EMPTY_PASSWORD);

        } else if (password !== confirm_password) {
            this.showToastAlert(Strings.PASS_MATCH);

        } else {
            this.showDialog();
            this.props.resetPasswordApi({
                useremail: useremail,
                resetpassword: confirm_password,
                token: token,

            });

        }
    }

    /*
    // =============================================================================================
    // Render method for Custom Loader
    // =============================================================================================
    */
    _renderCustomLoader = () => {
        const {isLoading} = this.state;
        return (
            <OrientationLoadingOverlay visible={isLoading} message={Strings.LOADING_TEXT}/>
        );
    };

    _onContinue() {
        const {navigate} = this.props.navigation;
        navigate('login');
    }

    render() {
        const {goBack} = this.props.navigation;
        const {success} = this.state;
        return (
            <KeyboardAvoidingView
                style={{flex: 1, backgroundColor: COLORS.WHITE_COLOR}}
                behavior={(Platform.OS === 'ios') ? 'padding' : null}>
                <SafeAreaView style={{flex: 1, backgroundColor: COLORS.THEME_COLOR}}>
                    <StatusBar barStyle="light-content" hidden={false} backgroundColor={COLORS.THEME_COLOR}
                               translucent={true}/>

                    <ScrollView style={{backgroundColor: COLORS.THEME_COLOR}}>
                        <View style={styles.mainContainer}>
                            <View>
                                <Image source={require('../../../assets/logo/logo.png')}
                                       style={{}}/>
                            </View>
                            <Spacer space={7}/>
                            {!success ?
                                <View>
                                    <Text style={styles.createTitleText}>{Strings.CREATETEXT}</Text>
                                    <Spacer space={2}/>
                                    <TextInput style={styles.textInputStyle}
                                               onSubmitEditing={() => {
                                                   this.refs.confirmPassword.focus();
                                               }}
                                               secureTextEntry={true}
                                               underlineColorAndroid="transparent"
                                               placeholder={Strings.NEW_PASSWORD_PLACEHOLDER}
                                               placeholderTextColor={COLORS.PLACEHOLDER_COLOR}
                                               autoCapitalize="none"
                                               onChangeText={text => this.setState({
                                                   password: text,
                                               })}
                                    />
                                    <Spacer space={3}/>
                                    <TextInput style={styles.textInputStyle}
                                               ref="confirmPassword"
                                               onSubmitEditing={() => {
                                                   this._onConfrim();
                                               }}
                                               secureTextEntry={true}
                                               underlineColorAndroid="transparent"
                                               placeholder={Strings.CONFIRM_NEW_PASSWORD_PLACEHOLDER}
                                               placeholderTextColor={COLORS.PLACEHOLDER_COLOR}
                                               autoCapitalize="none"
                                               onChangeText={text => this.setState({
                                                   confirm_password: text,
                                               })}
                                    />
                                    <Spacer space={3}/>

                                    <PrimaryButton
                                        onButtonPress={() => this._onConfrim()}
                                        label={Strings.CONFIRM}/>
                                </View> :
                                <View style={styles.successView}>
                                    <Text style={styles.successText}>{Strings.SUCCESS_PASSWORD}</Text>
                                    <Text style={styles.successText}>{Strings.SUCCESS_PASSWORD1}</Text>
                                    {/* <Text style={style.successText}>{Strings.SUCCESS_FORGOT1}</Text> */}
                                    <Spacer space={8}/>
                                    <PrimaryButton
                                        onButtonPress={() => this._onContinue()}
                                        label={Strings.CONTINUE}/>

                                </View>
                            }
                            <Spacer space={7}/>
                                <Text
                                    onPress={() => goBack()}
                                    style={style.forgotText}>{Strings.BACK_TO_LOGIN}</Text>
                            </View>



                        {this._renderCustomLoader()}

                    </ScrollView>
                </SafeAreaView>
            </KeyboardAvoidingView>
        );
    }
}

// ----------------------------------------
// ----------------------------------------
// CONNECT
// ----------------------------------------

const mapStateToProps = state => ({
    resetPasswordResponse: state.ResetPasswordReducer,

});

// ----------------------------------------

const mapDispatchToProps = (dispatch) => {
    return {
        resetPasswordApi: (payload) => dispatch(ResetPasswordAction(payload)),
    };
};

// ----------------------------------------

export default connect(mapStateToProps, mapDispatchToProps)(CreatePasswordScreen);
