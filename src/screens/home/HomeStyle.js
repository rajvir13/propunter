import React from 'react';
import {
    widthPercentageToDP as wp,
    heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import {StyleSheet} from 'react-native';
import {COLORS} from '../../themes/Colors';
import {FONTNAME} from '../../utils/FontName';
import {FONT} from '../../utils/Sizes';

const styles = StyleSheet.create({
    mainContainer: {
        alignContent: 'flex-start',
        paddingBottom: wp(7),
    },
    selectionView: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        paddingHorizontal: wp(4),
        paddingVertical: wp(4),
        alignItems: 'center',
    },
    selectionText: {
        color: COLORS.WHITE_COLOR,
        fontSize: FONT.TextMediumXX,
        fontFamily: FONTNAME.ProximaNova,
    },
    allText: {
        color: COLORS.WHITE_COLOR,
        fontSize: wp(3),
        fontFamily: FONTNAME.ProximaNova,
    },

});

export default styles;
