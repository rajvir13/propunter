// ----------------------------------------
// PACKAGE IMPORTS
// ----------------------------------------
import React, {Component} from 'react';
import AsyncStorage from '@react-native-community/async-storage';
import Icon from 'react-native-vector-icons/AntDesign';
import { connect } from "react-redux";
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from "react-native-responsive-screen";
import {
    View,
    Text,
    ScrollView,
    BackHandler,
    SafeAreaView,
    TouchableOpacity,
    StatusBar,
    KeyboardAvoidingView, Alert,
} from 'react-native';
import * as _ from "lodash";
import { CommonActions } from '@react-navigation/native';

// ----------------------------------------
// LOCAL IMPORTS
// ----------------------------------------
import {backAction} from '../../components/helper';
import {COLORS} from '../../themes/Colors';
import Header from '../../components/header';
import SwiperComponent from './component/swiper';
import TabView from './component/tabview';
import {Spacer} from '../../components/spacer';
import style from './HomeStyle';
import {Strings} from '../../utils/Strings';
import {NewsPostAction} from '../../redux/actions/NewPostAction';
import BaseClass from '../../utils/BaseClass';
import OrientationLoadingOverlay from '../../utils/CustomLoader';
import BottomTab from '../../components/bottomTab';

import {acceptTerm} from '../../redux/actions/ChatMessageAction';

import OuterModal from '../../components/ScreenshotModal/OuterModal';



Icon.loadFont();

class HomeScreen extends BaseClass {
    constructor(props) {
        super(props);
        this.state = {
            token: undefined,
            newPost: [],
            isLoading: false,
            isVisible: false,
            isShowing: false,
            chatCount:0,
        };
    }

    // ----------------------------------------
    // Class Life cycle method
    // ----------------------------------------
    componentDidMount = () => {
        const {navigation} = this.props;
        this._unsubscribe = navigation.addListener('focus', () => {
            this.onFocusFunction();
        });
        BackHandler.addEventListener('hardwareBackPress', backAction);
        AsyncStorage.getItem('chatCount',(err,chatCount) => {
            console.warn('Count-->',chatCount)
            this.setState(
                {
                    chatCount:chatCount,
                }
            )
        });
        AsyncStorage.getItem(Strings.TOKEN, (err, token) => {
            if (token !== undefined && token !== null) {
                this.setState({
                    token: JSON.parse(token),
                });

                if (!this.isConnected()) {
                    this.showToastAlert(Strings.NO_INTERNET);
                } else {
                    this.showDialog();
                    this.props.getNewsPost({
                        token: JSON.parse(token),
                        check: 1,
                    });
                }
            }
        });
    };

    onFocusFunction = () => {
        this.componentDidMount();
    };

    componentWillUnmount() {
        this._unsubscribe();
        BackHandler.removeEventListener('hardwareBackPress', backAction);
    }

    UNSAFE_componentWillReceiveProps = (nextProps) => {
        const {newPostResponse} = nextProps;
        const {newPost} = this.state;
        const {navigation} = this.props;
        this.hideDialog();
        if (newPostResponse !== undefined && newPostResponse !== null) {
            if (newPostResponse.data !== null && newPostResponse.data !== newPost) {
                if (newPostResponse.code === 200) {
                    this.setState({
                        newPost: newPostResponse.data !== null && newPostResponse.data,
                    });
                    //this.showToastSucess(newPostResponse.message)
                } else if (newPostResponse.code === 401) {
                    this.showToastAlert(newPostResponse.message)
                } else if (newPostResponse.code === 'rest_forbidden') {
                    this.hideDialog();
                    navigation.dispatch(
                        CommonActions.reset({
                            index: 0,
                            routes: [
                                {
                                    name: 'login',
                                },
                            ],
                        }),
                    );
                }else if (newPostResponse.code == 403) {
                    const { newPostResponse } = this.props
                    this.showToastAlert(newPostResponse.message)
                    const { navigation } = this.props
                    navigation.dispatch(
                        CommonActions.reset({
                            routes: [
                                { name: 'login' },
                            ],
                        })
                    );
                } else if (newPostResponse == "Network request failed") {
                    this.showToastAlert(Strings.NO_INTERNET)
                }
            }
        } else {
            this.showToastAlert(Strings.SOMETHING_WENT_WRONG);
        }
    };
    _openDrawer = () => {
        const {navigation} = this.props;
        navigation.openDrawer();
    };

    _selection = () => {
        const {navigate} = this.props.navigation;
        navigate('selection');
    };

    _commingSoon = () => {
        alert('coming soon');
    };
    _latestNews = () => {
        const {navigate} = this.props.navigation;
        navigate('news');
    };

    _preview = (item, index, indexItem) => {
        const {navigate} = this.props.navigation;
        navigate('selection', {title: 1, index, indexItem});
    };

    _openNewsDetails = (item) => {
        const { navigate } = this.props.navigation
        navigate('newsdetails', { item: item })
    }


    _tipstersProfile = () => {
        const { navigate } = this.props.navigation
        navigate('tipstrsprofile')
    }


    openHome = () => {
        this.props.navigation.navigate('home')
    }
    openSelection = () => {
        this.props.navigation.navigate('selection')

    }
    openFeed = () => {
        this.props.navigation.navigate('feed', {dummy: "Dummy"})
    }

    openNews = () => {
        this.props.navigation.navigate('news')

    }



    decline = () => {
        const {navigation} = this.props;
        this.setState({
            isVisible: false,
        });
        navigation.navigate('InnerModal');


    };

    // ----------------------------------------
    // Accept chat terms
    // ----------------------------------------

    accept = () => {
        const {navigation} = this.props;
        this.setState({
            isVisible: false,
        });
        navigation.navigate('home');
    };

    userlogout = () => {
        const {navigation} = this.props;
        this.setState({
            isShowing: true,
        });
        navigation.navigate('home');


    };

    // ----------------------------------------
    // Accept chat terms
    // ----------------------------------------

    acceptTerms = () => {
        const {navigation} = this.props;
        this.setState({
            isShowing: false,
        });
        navigation.navigate('home');
    };


    // =============================================================================================
    // Render method for Custom Loader
    // =============================================================================================

    _renderCustomLoader = () => {
        const {isLoading} = this.state;
        return (
            <OrientationLoadingOverlay
                visible={isLoading}
                message={Strings.LOADING_TEXT}
            />
        );
    };
    // ----------------------------------------
    // Main render
    // ----------------------------------------

    render() {
        const {newPost,isVisible,isShowing,chatCount} = this.state;
        return (
            <SafeAreaView style={{flex: 1, backgroundColor: COLORS.THEME_COLOR}}>
                <StatusBar
                    barStyle="light-content"
                    hidden={false}
                    backgroundColor={COLORS.THEME_COLOR}
                    translucent={true}
                />
                <Header
                    openDrawer={() => this._openDrawer()}
                    onPressIcon={() => this._commingSoon()}
                />
                <ScrollView style={{backgroundColor: COLORS.LIGHT_BLACK}}>
                    <SwiperComponent
                        openLatestNews={() => this._latestNews()}
                        openNewsDetails={(item) => this._openNewsDetails.bind(this, item)}
                        newPost={newPost}
                    />

                    <View style={style.mainContainer}>
                        <Spacer space={2}/>
                        <View style={style.selectionView}>
                           {/* <TouchableOpacity
                                onPress={() => this.setState({
                                    isVisible: true,
                                })}
                                style={style.feedIcon}>*/}
                            <Text style={style.selectionText}>SELECTIONS</Text>

                           {/* </TouchableOpacity>*/}
                            <TouchableOpacity
                                onPress={() => this._selection()}
                                style={{ flexDirection: 'row',justifyContent:'center',alignItems:'center'}}>
                                <Text style={style.allText}>ALL</Text>
                                <Spacer row={0.2}/>
                                <Icon
                                   // style={{ marginTop: Platform.OS == 'ios' ? wp(.3) : wp(.6) }}
                                    name={'right'}
                                    size={wp('3%')}
                                    color={COLORS.WHITE_COLOR}
                                />
                            </TouchableOpacity>
                        </View>
                        <TabView
                            _selectItem={(item, index, indexItem) =>
                                this._preview(item, index, indexItem)
                            }
                        />

                        <Spacer space={3} />
                        {/* <TouchableOpacity
                            onPress={() => this._tipstersProfile()} style={{ alignItems: 'center' }}>
                            <Text style={{ color: 'white' }}>VIEW TIPSTER PROFILES</Text>
                        </TouchableOpacity> */}
                    </View>
                    <Spacer space={2}/>
                    {this._renderCustomLoader()}
                </ScrollView>

                <OuterModal
                    decline={() => this.decline(0)}
                    accept={() => this.accept(1)}
                    isVisible={isVisible}
                />

                {/*<InnerModal
                    userlogout={() => this.userlogout(0)}
                    acceptTerms={() => this.acceptTerms(1)}
                    isShowing={isShowing}
                />*/}

                <BottomTab
                    chatCount = {chatCount}
                    openHome={() => this.openHome()}
                    openSelection={() => this.openSelection()}
                    openFeed={() => this.openFeed()}
                    openNews={() => this.openNews()}
                />
            </SafeAreaView>
        );
    }
}

// ----------------------------------------
// ----------------------------------------
// CONNECT
// ----------------------------------------

const mapStateToProps = (state) => ({
    newPostResponse: state.NewPostReducer.newPostResponse,
});

// ----------------------------------------

const mapDispatchToProps = (dispatch) => {
    return {
        getNewsPost: (payload) => dispatch(NewsPostAction(payload)),
    };
};

// ----------------------------------------

export default connect(mapStateToProps, mapDispatchToProps)(HomeScreen);
