import { StyleSheet } from "react-native";
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from "react-native-responsive-screen";
import { COLORS } from '../../../themes/Colors';
import { FONTNAME } from '../../../utils/FontName';

const styles = StyleSheet.create({
    bottomLine: {
        width: wp(100),
        height: wp(.3),
        backgroundColor: COLORS.PLACEHOLDER_TEXT_COLOR
    },
    mainContainer: {
        backgroundColor: COLORS.THEME_COLOR,
        flexDirection: 'row',
        justifyContent: 'space-between',
        paddingHorizontal: wp(4),
        paddingVertical: wp(4),
        alignItems: 'center',
        flex: 1,
    },
    childContainer: {
        flexDirection: 'column',
        paddingLeft: wp(5)
    },
    titleText: {
        color: COLORS.WHITE_COLOR,
        fontFamily: FONTNAME.ProximaNova,
        fontSize: wp(4),
        fontWeight: '100'
    },
    detailText: {
        color: COLORS.PLACEHOLDER_TEXT_COLOR,
        paddingTop: wp(1),
        fontFamily: FONTNAME.ProximaNova,
        fontSize: wp(3.3),
        fontWeight: '100'
    },
    descriptionBackground: {
        backgroundColor: COLORS.WHITE_SEMI,
        paddingHorizontal: wp(5),
        paddingVertical: wp(5)
    },
    descriptionText:
    {
        color: COLORS.PLACEHOLDER_TEXT_COLOR,
        fontSize: wp(3)
    },
    bottomLine: {
        width: wp(100),
        height: wp(.3),
        backgroundColor: COLORS.PLACEHOLDER_TEXT_COLOR
    },
    tipsterRow: {
        width: wp(100),
        backgroundColor: COLORS.LIGHT_BLACK,
        flexDirection: 'row',
        flex: 1,
        justifyContent: 'flex-start',
        paddingVertical: wp(3),
        alignItems: 'center'
    },
    tipsterRowChild: {
        flexDirection: "row",
        justifyContent: 'space-between',
        width: wp(100),
        paddingHorizontal: wp(5),
        paddingVertical: wp(.5)
    },
    tipsterName: {
        fontFamily:FONTNAME.ProximaNovaRegular,
        color: COLORS.WHITE_SEMI,
        width: wp(50)
    },
    tipsterNameView: {
        flexDirection: "row",
        justifyContent: 'center',
        alignItems: 'center'
    },
    image: {
        width: wp(6),
        height: wp(6)
    },
    closeMore: {
        fontFamily:FONTNAME.ProximaNova,
        color: COLORS.WHITE_COLOR,
        fontSize: wp(3)

    },
    headerView: {
        flexDirection: 'row',
        alignContent: 'flex-start',
        alignItems: 'center',
        backgroundColor: COLORS.LIGHT_BLACK
    },
    backArrow: {
        alignSelf: 'center',
        paddingLeft: wp(2),
        paddingRight: wp(0.5)
    },
    backText: {
        color: COLORS.PLACEHOLDER_TEXT_COLOR,
        fontSize: wp(2.8)
    },
    headerTitle: {
        color: COLORS.WHITE_COLOR,
        paddingLeft: wp(15),
        paddingVertical: wp(6),
        fontFamily: FONTNAME.ProximaNova, 
        fontSize: wp(4.8)
    }
})

export default styles;
