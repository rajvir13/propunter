// ----------------------------------------
// PACKAGE IMPORTS
// ----------------------------------------
import React, { Component } from 'react';
import { connect } from "react-redux";
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from "react-native-responsive-screen";
import {View, Text, ScrollView, SafeAreaView, FlatList, Image, StatusBar, Alert} from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';
import { CommonActions } from '@react-navigation/native';

// ----------------------------------------
// LOCAL IMPORTS
// ----------------------------------------
import { COLORS } from '../../../themes/Colors';
import Header from '../../../components/header';
import { Strings } from '../../../utils/Strings';
import OrientationLoadingOverlay from '../../../utils/CustomLoader';
import styles from './TipsterStyle';
import Icon from 'react-native-vector-icons/AntDesign'
import { TipsterRow } from '../component/tipsterRenderItem'
import { TouchableOpacity } from 'react-native-gesture-handler';
import BaseClass from '../../../utils/BaseClass';
import { AllTipsterAction } from '../../../redux/actions/AllTipsterAction'
import BottomTab from '../../../components/bottomTab'

class TipstersProfileScreen extends BaseClass {
    constructor(props) {
        super(props);
        this.state = {
            isCheck: false,
            tipsterdata: [],
            isLoading: false,
            chatCount:0,
        };
    }

    // ----------------------------------------
    // Class Life cycle method
    // ----------------------------------------
    componentDidMount = () => {
        const { navigation } = this.props;
        this._unsubscribe = navigation.addListener("focus", () => {
            this.onFocusFunction();
        });
        // BackHandler.addEventListener('hardwareBackPress', exitAlert);
        AsyncStorage.getItem(Strings.TOKEN, (err, token) => {
            if (token !== undefined && token !== null) {
                if (!this.isConnected()) {
                    this.showToastAlert(Strings.NO_INTERNET)
                }
                else {
                    this.showDialog()
                    this.setState({
                        isCheck: true
                    })
                    this.props.getAllTipsters({
                        token: JSON.parse(token),

                    })
                }

            }
        });
        AsyncStorage.getItem('chatCount',(err,chatCount) => {
            console.warn('Count-->',chatCount)
            this.setState(
                {
                    chatCount:chatCount,
                }
            )
        })

    }

    onFocusFunction = () => {
        this.componentDidMount()
    };

    componentWillUnmount() {
        this._unsubscribe();
        //  BackHandler.removeEventListener('hardwareBackPress', exitAlert);
    }


    componentDidUpdate = (prevProps, prevState) => {
        const { tipsterResponse } = this.props;
        const {navigation} = this.props;
        if (prevProps.tipsterResponse !== this.props.tipsterResponse && prevState.isCheck) {
            if (tipsterResponse.code === 200) {
                this.hideDialog()
                this.setState({
                    tipsterdata: tipsterResponse.message,
                    isCheck: false
                })
            }else if (tipsterResponse.code === 'rest_forbidden') {
                this.hideDialog();
                navigation.dispatch(
                    CommonActions.reset({
                        index: 0,
                        routes: [
                            {
                                name: 'login',
                            },
                        ],
                    }),
                );
            } else if (tipsterResponse.code == 403) {
                this.hideDialog()

                this.showToastAlert(tipsterResponse.message)
                const { navigation } = this.props
                navigation.dispatch(
                    CommonActions.reset({
                        routes: [
                            { name: 'login' },
                        ],
                    })
                );
            }
            else if (tipsterResponse == "Network request failed") {
                this.hideDialog()
                this.showToastAlert(Strings.NO_INTERNET)
            }


        }
        // else {
        //     this.showToastAlert(Strings.SOMETHING_WENT_WRONG
        //     )
        // }
    }


    _openDrawer = () => {
        const { navigation } = this.props
        navigation.openDrawer();

    }
    _notifications = () => {
        const { navigation } = this.props
        navigation.navigate('notification');
    }
    _onBackPressed = () => {
        const { navigation } = this.props
        navigation.goBack();
    }

    openHome = () => {
        this.props.navigation.navigate('home')
    }
    openSelection = () => {
        this.props.navigation.navigate('selection')

    }
    openFeed = () => {
        this.props.navigation.navigate('feed')

    }

    openNews = () => {
        this.props.navigation.navigate('news')

    }


    // =============================================================================================
    // Render method for Custom Loader
    // =============================================================================================

    _renderCustomLoader = () => {
        const { isLoading } = this.state;
        return (
            <OrientationLoadingOverlay visible={isLoading} message={Strings.LOADING_TEXT} />
        )
    };

    // ----------------------------------------
    // Main render
    // ----------------------------------------

    render() {
        const { tipsterdata,chatCount } = this.state
        return (
            <SafeAreaView style={{ flex: 1, backgroundColor: COLORS.THEME_COLOR }}>
                <StatusBar barStyle="light-content" hidden={false} backgroundColor={COLORS.THEME_COLOR} translucent={true} />
                <Header
                    openDrawer={() => this._openDrawer()}
                    onPressIcon={() => this._notifications()}
                />
                <ScrollView style={{ backgroundColor: COLORS.LIGHT_BLACK }}>
                    <View style={styles.headerView}>
                        <TouchableOpacity style={{ flexDirection: 'row' }}
                            onPress={() => this._onBackPressed()}>
                            <Icon
                                style={styles.backArrow}
                                name={'left'}
                                size={wp('2.8%')} color={COLORS.PLACEHOLDER_TEXT_COLOR} />
                            <Text style={styles.backText}>BACK</Text>
                        </TouchableOpacity>
                        <Text style={styles.headerTitle}>
                            {Strings.TIPSTERS_TITLE}
                        </Text>
                    </View>
                    <View style={styles.bottomLine}></View>

                    <FlatList
                        data={tipsterdata}
                        renderItem={({ item, index }) =>
                            <TipsterRow
                                data={item}
                                index={index}
                            />
                        }
                        keyExtractor={(item, index) => index.toString()}
                        style={{ flex: 1 }}
                    />
                    {this._renderCustomLoader()}
                </ScrollView>
                <BottomTab
                    chatCount = {chatCount}
                    openHome={() => this.openHome()}
                    openSelection={() => this.openSelection()}
                    openFeed={() => this.openFeed()}
                    openNews={() => this.openNews()}

                />
            </SafeAreaView >
        )
    }
}
// ----------------------------------------
// ----------------------------------------
// CONNECT
// ----------------------------------------

const mapStateToProps = state => ({
    tipsterResponse: state.AllTipsterReducer.tipsterResponse

});

// ----------------------------------------

const mapDispatchToProps = (dispatch) => {
    return {
        getAllTipsters: (payload) => dispatch(AllTipsterAction(payload)),
    };
};

// ----------------------------------------

export default connect(mapStateToProps, mapDispatchToProps)(TipstersProfileScreen);
