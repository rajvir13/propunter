import React, {useState} from 'react';
import {Collapse, CollapseHeader, CollapseBody} from 'accordion-collapse-react-native';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import {View, Text, ScrollView, SafeAreaView, FlatList, Image, StatusBar} from 'react-native';
import HTML from 'react-native-render-html';

// ----------------------------------------
// LOCAL IMPORTS
// ----------------------------------------
import {COLORS} from '../../../../themes/Colors';
import styles from '../../tipsters/TipsterStyle';
import Icon from 'react-native-vector-icons/AntDesign';
import {Spacer} from '../../../../components/spacer';

export const TipsterRow = (props) => {
    const {data} = props;
    const [isOpened, setOpened] = useState(false);
    return <Collapse isCollapsed={isOpened}
                     onToggle={(isCollapsed) => {
                         setOpened(isCollapsed);
                     }}>
        <CollapseHeader>
            <View style={styles.tipsterRow}>
                <View style={styles.tipsterRowChild}>
                    <View style={styles.tipsterNameView}>
                        <Image source={{uri: data.profile_image}}
                               style={styles.image}
                        />
                        <Spacer row={2}/>
                        <Text style={styles.tipsterName}>{data.user_info.data.user_nicename}</Text>
                    </View>
                    <View style={[styles.tipsterNameView, {paddingRight: wp(0)}]}>
                        <Text style={styles.closeMore}>{isOpened ? 'CLOSE' : 'MORE'}</Text>
                        <Spacer row={1}/>
                        {isOpened ? <Icon
                                style={{alignSelf: 'center'}}
                                name={'close'}
                                size={wp('4%')} color={COLORS.WHITE_SEMI}/>
                            :
                            <View style={{width: wp(5), justifyContent: 'center'}}>
                                <Image
                                    resizeMode='contain'
                                    source={require('../../../../assets/images/Tipsters/Up.png')}/>
                                <Image
                                    resizeMode='contain'
                                    source={require('../../../../assets/images/Tipsters/Down.png')}/>
                            </View>}
                    </View>
                </View>
            </View>
            <View style={styles.bottomLine}></View>
        </CollapseHeader>
        <CollapseBody>
            {data.bio_description !== '' &&
            <View style={styles.descriptionBackground}>

                <HTML html={data.bio_description} imagesMaxWidth={wp(100)}/>

            </View>

            }

        </CollapseBody>
    </Collapse>;
};
