// ----------------------------------------
// PACKAGE IMPORTS
// ----------------------------------------
import Icon from 'react-native-vector-icons/AntDesign';
import React, {Component} from 'react';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import {
    View,
    Text,
    Image,
    TouchableOpacity,
    TextInput,
    FlatList,
    TouchableWithoutFeedback,
    Keyboard,
} from 'react-native';
import moment from 'moment';
import PropTypes from 'prop-types';

// ----------------------------------------
// LOCAL IMPORTS
// ----------------------------------------
import {COLORS} from '../../../../themes/Colors';
import {Spacer} from '../../../../components/spacer';
import style from './Style';

const RenderItem = (props) => {
    const {item, onClickItem, status} = props;
     //console.warn("data",props)
    return <TouchableOpacity
        onPress={onClickItem}>

        {/*{ item.bio !== "" && item.bio !== undefined && <>*/}
            <View style={style.mainContainer}>
                <View style={style.childContainer}>
                    <Image source={{uri: item.avatar}}
                           style={{width: wp(6), height: wp(6)}}
                    />
                    <Spacer row={2}/>
                    <Text style={style.name}>{item.display_name}</Text>
                </View>
                <Text style={style.day}>{moment(item.user_registered).format('hA')} {status.toUpperCase()}</Text>
                <Icon
                    style={{alignSelf: 'center'}}
                    name={'pluscircleo'}
                    size={wp('5%')} color={COLORS.GREEN}/>
            </View>

            <View style={style.lineStyle}></View>
      {/*  </>}*/}

    </TouchableOpacity>;
};

RenderItem.defaultProps = {
    item: {display_name: '', user_registered: ''},

};

RenderItem.propTypes = {
    item: PropTypes.object,
};


export default RenderItem;
