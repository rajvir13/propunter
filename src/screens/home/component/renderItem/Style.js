import React from 'react';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import {StyleSheet} from 'react-native';
import {COLORS} from '../../../../themes/Colors';
import {FONTNAME} from '../../../../utils/FontName';

const styles = StyleSheet.create({
    mainContainer: {
        backgroundColor: COLORS.LIGHT_BLACK,
        flexDirection: 'row',
        justifyContent: 'space-between', paddingHorizontal: wp(4),
        paddingVertical: wp(4), alignItems: 'center',
        flex: 1,
    },
    childContainer: {
        flexDirection: 'row',
    },
    lineStyle: {
        width: wp(100),
        height: wp(.3),
        backgroundColor: COLORS.PLACEHOLDER_TEXT_COLOR,
    },
    name: {
        color: COLORS.WHITE_COLOR,
        width: wp(35),
        fontFamily: FONTNAME.ProximaNovaregular,
        fontSize: wp(3.2),


    },
    day: {
        fontSize: wp(2.3),
        color: '#C6C6C6',
        fontFamily: FONTNAME.ProximaNova,
    },

});

export default styles;
