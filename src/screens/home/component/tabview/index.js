// ----------------------------------------
// PACKAGE IMPORTS
// ----------------------------------------
import React, {Component} from 'react';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import {View, Text, Image, SafeAreaView, TextInput, FlatList, TouchableWithoutFeedback, Keyboard} from 'react-native';

// ----------------------------------------
// LOCAL IMPORTS
// ----------------------------------------
import {COLORS} from '../../../../themes/Colors';
// import RenderItem from '../renderItem';
import {TabView, PagerScroll} from 'react-native-tab-view';
import Today from './Today';
import Tomorrow from './Tomorrow';
import Upcoming from './Upcoming';
import TabBar from '../../../../../local_module/react-native-tab-view/src/TabBar';
import BaseClass from '../../../../utils/BaseClass';
import {FONTNAME} from '../../../../utils/FontName';

export default class Tab extends BaseClass {

    constructor(props) {
        super(props);
        this.state = {
            index: 0,
            routes: [
                {key: 'today', title: 'TODAY'},
                {key: 'tomorrow', title: 'TOMORROW'},
                {key: 'upcoming', title: 'UPCOMING'},
            ],
        };
    }


    _handleIndexChange = (index) => {
        this.setState({index});
    };

    //  _renderHeader = props => <TabBar {...this.props} tabStyle={{}} style={{ fontFamily: 'bold', fontSize: wp('4%'), backgroundColor: COLORS.NEW_THEME_COLOR, }} />;

    _renderLabel = ({route}) => (
        <Text style={{fontSize: 25}}/>
    );


    //   _renderHeader = props => <TabBar {...this.props} renderLabel={this._renderLabel} tabStyle={{}} style={{backgroundColor: COLORS.NEW_THEME_COLOR}} />;


    _renderScene = ({route}) => {
        const {index} = this.state;
        switch (route.key) {
            case 'today':
                if (index === 0) {
                    return <Today routeKey={this.state.index} {...this.props}
                                  _selectItem={(item, index) => this.props._selectItem(item, 0, index)}
                    />;
                }
                break;
            case 'tomorrow':
                if (index === 1) {
                    return <Tomorrow routeKey={this.state.index} {...this.props}
                                     _selectItem={(item, index) => this.props._selectItem(item, 1, index)}
                    />;
                }
                break;
            case 'upcoming':
                if (index === 2) {
                    return <Upcoming routeKey={this.state.index} {...this.props}
                                     _selectItem={(item, index) => this.props._selectItem(item, 2, index)}
                    />;
                }
                break;
            default:
                return null;
        }
    };


    render() {
        const {index} = this.state;
        return (
            <>
                <TabView
                    // style={{ height: wp(100) }}
                    navigationState={this.state}
                    renderScene={this._renderScene}
                    //   lazy={false}
                    // renderHeader={(this._renderHeader)}
                    onIndexChange={(index) => this._handleIndexChange(index)}
                    renderTabBar={props => (
                        <TabBar
                            {...props}
                            scrollEnabled
                            tabStyle={{
                                width: wp('100%') / 3, color: COLORS.GREEN, textAlign: 'center',

                            }}
                            style={{
                                //backgroundColor: COLORS.WHITE_COLOR,
                            }}
                            //labelStyle={{color: 'green', fontsize: 10}}
                            renderLabel={({route, focused, color}) => {
                                if (index === 0) {
                                    if (route.key === 'today') {
                                        return <Text style={{
                                            color: COLORS.WHITE_COLOR,
                                            fontSize: wp(4.3),
                                            fontFamily: FONTNAME.ProximaNova,
                                        }}> {route.title} </Text>;
                                    }
                                }
                                if (index === 1) {
                                    if (route.key === 'tomorrow') {
                                        return <Text style={{
                                            color: COLORS.WHITE_COLOR,
                                            fontSize: wp(4.3),
                                            fontFamily: FONTNAME.ProximaNova,
                                        }}> {route.title} </Text>;
                                    }
                                }
                                if (index === 2) {
                                    if (route.key === 'upcoming') {
                                        return <Text style={{
                                            color: COLORS.WHITE_COLOR,
                                            fontSize: wp(4.3),
                                            fontFamily: FONTNAME.ProximaNova,
                                        }}> {route.title} </Text>;
                                    }
                                }
                                return <Text style={{
                                    color: COLORS.PLACEHOLDER_TEXT_COLOR,
                                    fontSize: wp(3.2),
                                    fontFamily: FONTNAME.ProximaNova,
                                }}> {route.title} </Text>;
                            }}
                        />
                    )}
                    renderPager={(props) => <PagerScroll {...props} />}
                />
            </>
        );
    }

}




