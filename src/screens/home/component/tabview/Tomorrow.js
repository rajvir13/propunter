// ----------------------------------------
// PACKAGE IMPORTS
// ----------------------------------------
import React, {Component} from 'react';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import {View, FlatList} from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';
import {CommonActions} from "@react-navigation/native";
// ----------------------------------------
// LOCAL IMPORTS
// ----------------------------------------
import {COLORS} from '../../../../themes/Colors';
import RenderItem from '../renderItem';
import BaseClass from '../../../../utils/BaseClass';
import {TipserPostAction} from '../../../../redux/actions/NewPostAction';
import {Strings} from '../../../../utils/Strings';
import Tab from '../../../preview';

export default class Tomorrow extends BaseClass {
    constructor(props) {
        super(props);
        this.state = {
            token: undefined,
            tipserPost: [],
        };
    }

    // ----------------------------------------
    // Class Life cycle method
    // ----------------------------------------
    componentDidMount = () => {
        AsyncStorage.getItem(Strings.TOKEN, (err, token) => {
            if (token !== undefined && token !== null) {
                this.setState(({
                    token: JSON.parse(token),
                }));
                TipserPostAction({
                        token: JSON.parse(token),
                        getby: 'tomorrow',
                    }, response => this.tipserDataResponse(response),
                );
            }
        });
    };

    // ----------------------------------------
    // Tipser post response
    // ----------------------------------------

    tipserDataResponse = (response) => {
        const {tipserPost} = this.state;
        const {navigation} = this.props;
        if (response.status === 200) {
            let tipserPost = [];
            let temp = Object.keys(response);
            temp.map((item) => {
                if (item !== 'status') {
                    tipserPost.push(response[item]);
                }
            });
            console.warn('res', tipserPost);
            this.setState({
                tipserPost: tipserPost,
            });
        }else if (response.code === 'rest_forbidden') {
            this.hideDialog();
            navigation.dispatch(
                CommonActions.reset({
                    index: 0,
                    routes: [
                        {
                            name: 'login',
                        },
                    ],
                }),
            );
        } else if (response.code === 401) {
            this.showToastAlert('Record not found');
        }
    };
    // ----------------------------------------
    // Class method
    // ----------------------------------------
    _selectItem = (item, index) => {

        this.props._selectItem(item, index);
    };

    render() {

        const {tipserPost} = this.state;
        return (

            <FlatList
                data={tipserPost}
                renderItem={({item, index}) =>
                    <RenderItem
                        //scrollEnabled={false}
                        item={item.userinfo.data}
                        status={'Tomorrow'}
                        onClickItem={() => this._selectItem(item, index)}
                    />
                }
                keyExtractor={(item, index) => index.toString()}
                style={{backgroundColor: COLORS.LIGHT_BLACK}}
            />


        );


    }
}
