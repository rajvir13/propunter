import React from 'react';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import {StyleSheet} from 'react-native';
import {COLORS} from '../../../../themes/Colors';
import {FONTNAME} from '../../../../utils/FontName';
import {FONT} from '../../../../utils/Sizes';

const styles = StyleSheet.create({
    childContainer: {
        justifyContent: 'space-between',
        paddingHorizontal: wp(5),
        paddingVertical: wp(3),
        backgroundColor: COLORS.LIGHT_BLACK,
        alignContent: 'flex-start',
    },
    latestNews: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
    },
    latestNewsTitle: {
        color: COLORS.WHITE_COLOR,
        fontSize: FONT.TextMediumXX,
        fontFamily: FONTNAME.ProximaNova,
    },
    all: {
        color: COLORS.WHITE_COLOR,
        marginLeft: wp(10),
        fontSize: wp(3),
        fontFamily: FONTNAME.ProximaNova,
    },
    socialContainer: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        marginTop: 1,
    },
    underline: {
        width: wp(100), height: wp(.3), backgroundColor: '#707070',
    },
});

export default styles;
