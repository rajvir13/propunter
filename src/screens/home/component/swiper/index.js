// ----------------------------------------
// PACKAGE IMPORTS
// ----------------------------------------
import React, {Component} from 'react';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import {
    View,
    Text,
    Image,
    Platform,
    TextInput,
    TouchableWithoutFeedback,
    Keyboard,
    TouchableOpacity, Linking, ScrollView,
} from 'react-native';
import Swiper from 'react-native-swiper';
import Icon from 'react-native-vector-icons/AntDesign';
import Insta from 'react-native-vector-icons/Entypo';
import * as _ from 'lodash';
import HTML from 'react-native-render-html';
// ----------------------------------------
// LOCAL IMPORTS
// ----------------------------------------
import {Spacer} from '../../../../components/spacer';
import {COLORS} from '../../../../themes/Colors';
import style from './SwiperStyle';
import {Strings} from '../../../../utils/Strings';
import styles from '../../../../components/drawerItem/Styles';
import IconSocial from 'react-native-vector-icons/FontAwesome';

Icon.loadFont();
Insta.loadFont();


_openFB = () => {
    Linking.openURL('https://www.facebook.com/propunteralert/');
};
_openTwitter = () => {
    Linking.openURL('https://twitter.com/ProPunterAlert');
};

_openInsta = () => {
    Linking.openURL('https://www.instagram.com/pro_punter_alert/');
};

const SwiperComponent = (props) => {
    const {newPost, openLatestNews, openNewsDetails} = props;
    console.warn("te", newPost)
    return (
        <View>
            <View style={style.childContainer}>
                <View style={style.latestNews}>
                    <Text style={style.latestNewsTitle}>{Strings.LATEST}</Text>

                    <View style={{
                        flexDirection: 'row',
                        alignItems: 'center',
                        justifyContent: 'flex-end',
                        //paddingHorizontal:wp(4),

                    }}>
                        <IconSocial
                            onPress={() => _openFB()}
                            name={'facebook'}
                            size={wp('6%')} color={COLORS.WHITE_COLOR}/>
                        <Spacer row={3}/>
                        <IconSocial
                            onPress={() => _openTwitter()}
                            name={'twitter'}
                            size={wp('7%')} color={COLORS.WHITE_COLOR}/>
                        <Spacer row={3}/>
                        <Insta
                            onPress={() => _openInsta()}
                            name={'instagram'}
                            size={wp('6%')} color={COLORS.WHITE_COLOR}/>
                    </View>


                  {/*  <TouchableOpacity
                        onPress={openLatestNews}
                        style={{flexDirection: 'row', justifyContent: 'center', alignItems: 'center'}}>
                        <Text style={style.all}>{Strings.ALL}</Text>
                        <Spacer row={.2}/>
                        <Icon
                            name={'right'}
                            size={wp('3%')} color={COLORS.WHITE_COLOR}/>
                    </TouchableOpacity>*/}
                </View>

            </View>
            <View style={style.underline}/>
            <View>
                <Swiper
                    activeDotStyle={{backgroundColor: 'transparent'}}
                    dotStyle={{backgroundColor: 'transparent'}}
                    autoplay={true}
                    key={newPost.length}
                    containerStyle={{height: wp(65)}}>
                    {
                        _.map(newPost, (item, index) =>
                            <View>
                                <Image
                                    style={{width: wp(100), height: wp(80)}}
                                    source={{
                                        uri: item.featured_url !== false && item.featured_url,
                                    }}
                                />
                                <TouchableOpacity
                                    onPress={openNewsDetails(item)}
                                    style={{
                                        backgroundColor: COLORS.LIGHT_BLACK,
                                        width: wp(85),
                                        paddingVertical: wp(3),
                                        paddingHorizontal: wp(3),
                                        position: 'absolute', top: wp(40),
                                        borderRadius: 10,
                                        left: wp(7.5),
                                    }}>
                                    <Text
                                        style={{color: 'white', fontSize: wp(4)}}>{item.post_title.toUpperCase()}</Text>
                                    <Spacer space={1}/>
                                    <View style={{flexDirection: 'row'}}>
                                        <Text
                                            numberOfLines={1}
                                            style={{color: 'white', width: wp(75), fontSize: wp(3)}}>
                                            {item.post_excerpt.toUpperCase()}
                                        </Text>
                                        <Insta
                                            style={{marginTop: 0}}
                                            name={'dots-three-vertical'}
                                            size={wp('6%')} color={COLORS.WHITE_COLOR}/>
                                    </View>
                                </TouchableOpacity>
                            </View>,
                        )}
                </Swiper>
            </View>

        </View>
    );
};

export default SwiperComponent;
