// ----------------------------------------
// PACKAGE IMPORTS
// ----------------------------------------
import React, {Component} from 'react';
import moment from 'moment';
import PropTypes from 'prop-types';
import {View, Text, TouchableOpacity} from 'react-native';
// ----------------------------------------
// LOCAL IMPORTS
// ----------------------------------------
import Icon from 'react-native-vector-icons/AntDesign';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import styles from '../../NotificationStyle';
import {COLORS} from '../../../../themes/Colors';
import {FONTNAME} from '../../../../utils/FontName';
import {Spacer} from '../../../../components/spacer';
import {timeDifference} from '../../../../components/transform';

const RenderItem = ({item, index, onClickItem}) => {
    return <TouchableOpacity style={{alignContent: 'flex-start'}}
                             onPress={() => onClickItem(item)}>
        <View style={styles.thinLine}></View>
        <View style={styles.rowItem}>
            <View style={styles.textContainer}>
                <Text numberOfLines={1} style={styles.titleText}>
                    {item.notification}
                </Text>
                <Spacer space={.5}/>
                <View style={{
                    flexDirection: 'row',
                    alignItems: 'center',
                }}>
                    <Text numberOfLines={1} style={styles.detailText}>{item.excerpt}</Text>
                    <Spacer row={2}/>
                    <Text style={{
                        width: wp(25),
                        fontFamily: FONTNAME.ProximaNova,
                        fontSize: wp(2.7), color: COLORS.PLACEHOLDER_TEXT_COLOR,
                    }}>
                        {timeDifference(moment(item.created_at).unix(), moment(item.created_at).format('hA'))}
                    </Text>
                    <Icon
                        style={{alignSelf: 'center'}}
                        name={'rightcircleo'}
                        size={wp('4%')} color={item.read_status == 0 ? COLORS.GREEN : COLORS.WHITE_COLOR}/>
                </View>
            </View>

        </View>
    </TouchableOpacity>;
};

export default RenderItem;

RenderItem.defaultProps = {
    item: {notification: '', created_at: ''},

};

RenderItem.propTypes = {
    item: PropTypes.object,
};
