// ----------------------------------------
// PACKAGE IMPORTS
// ----------------------------------------
import {View, Text, Modal, TouchableOpacity, ScrollView} from 'react-native';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import React, {useState} from 'react';
// ----------------------------------------
// LOCAL IMPORTS
// ----------------------------------------
import {Spacer} from '../../../../../src/components/spacer';
import {Strings} from '../../../../utils/Strings';
import {COLORS} from '../../../../themes/Colors';
import {FONTNAME} from '../../../../utils/FontName';
import HTML from 'react-native-render-html';

const NotifyModal = (props) => {
    const [visible, setVisible] = useState(props.isVisible);

    const decline = () => {
        setVisible(false);
        props.decline();
    };

    return (
        <Modal
            transparent={true}
            visible={props.isVisible}
            onRequestClose={() => {
                console.log('hhh');
            }}>
            <View
                style={{
                    flex: 1, justifyContent: 'center', alignItems: 'center',
                    backgroundColor: `rgba(0, 0, 0, .8)`,
                    width: wp(100),
                    paddingHorizontal: wp(5),
                }}>
                <View style={{backgroundColor: COLORS.LIGHT_BLACK, alignItems: "center",borderRadius: wp(3)}}>

                    <View style={{padding: wp(5)}}>
                        <Text style={{
                            alignSelf: 'center',
                            color: COLORS.WHITE_SEMI, fontFamily: FONTNAME.ProximaNova, fontSize: wp(4.8),
                        }}>{props.heading}</Text>
                        <Spacer space={1.7}/>

                        {/*  <Text style={{
                            height:70,
                            color: COLORS.WHITE_SEMI, fontSize: wp(3.2),
                            fontFamily: FONTNAME.ProximaNovaRegular, alignSelf: 'center',
                        }}>{props.content}</Text>*/}
                        <View style={{height: wp(40)}}>
                            <ScrollView>
                                <HTML html={props.content} baseFontStyle={{
                                    color: COLORS.WHITE_SEMI, fontSize: wp(3.2),
                                    fontFamily: FONTNAME.ProximaNovaRegular, alignSelf: 'center',
                                }} imagesMaxWidth={wp(100)}/>

                            </ScrollView>
                        </View>
                    </View>
                    <TouchableOpacity
                        onPress={() => decline()}
                        style={{alignItems: 'center', justifyContent: 'center'}}>
                    <View style={{width: wp(80), height: 1, backgroundColor: COLORS.PLACEHOLDER_TEXT_COLOR}}/>
                    <View style={{flexDirection: 'row', paddingVertical: wp(3),justifyContent: 'space-evenly'}}>

                            <Text style={{
                                color: COLORS.WHITE_COLOR,
                                fontFamily: FONTNAME.ProximaNova,
                                fontSize: wp(3.5),
                            }}>
                                CLOSE</Text>

                    </View>
                    </TouchableOpacity>

                </View>


            </View>
        </Modal>
    );
};


export default NotifyModal;