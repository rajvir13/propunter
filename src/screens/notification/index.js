// ----------------------------------------
// PACKAGE IMPORTS
// ----------------------------------------
import React, {Component} from 'react';
import {CommonActions} from '@react-navigation/native';
import AsyncStorage from '@react-native-community/async-storage';
import {
    View, Image, Text, ScrollView, SafeAreaView, FlatList,
    TouchableOpacity, StatusBar, KeyboardAvoidingView,
} from 'react-native';
// ----------------------------------------
// LOCAL IMPORTS
// ----------------------------------------
import {COLORS} from '../../themes/Colors';
import Header from '../../components/header';
import {Strings} from '../../utils/Strings';
import {connect} from 'react-redux';
import OrientationLoadingOverlay from '../../utils/CustomLoader';
import styles from './NotificationStyle';
import RenderItem from './component/RenderItem';
import BottomTab from '../../components/bottomTab';
import BaseClass from '../../utils/BaseClass';
import {NoificationListAction, GetPost, ReadNotification} from '../../redux/actions/NotificationAction';
import NotifyModal from './component/NotifyModal';

class NotificationScreen extends BaseClass {
    constructor(props) {
        super(props);
        this.state = {
            token: undefined,
            isCheck: false,
            notificationList: [],
            isLoading: false,
            chatCount: 0,
            isVisible: false,
            heading: '',
            content: '',

        };
    }

    // ----------------------------------------
    // Lifecyle method
    // ----------------------------------------
    componentDidMount = () => {

        const {navigation} = this.props;
        this._unsubscribe = navigation.addListener('focus', () => {
            this.onFocusFunction();
        });
        AsyncStorage.getItem(Strings.TOKEN, (err, token) => {
            if (token !== undefined && token !== null) {
                if (!this.isConnected()) {
                    this.showToastAlert(Strings.NO_INTERNET);
                } else {
                    this.setState({
                        isCheck: true,
                        token: JSON.parse(token),
                    });
                    this.showDialog();
                    this.props.getNotificationList({
                        token: JSON.parse(token),
                    });
                }
            }
        });
        AsyncStorage.getItem('chatCount', (err, chatCount) => {
            //  console.warn('Count-->',chatCount)
            this.setState(
                {
                    chatCount: chatCount,
                },
            );
        });
    };
    onFocusFunction = () => {
        this.componentDidMount();
    };

    componentWillUnmount() {
        this._unsubscribe();

    }

    componentDidUpdate = (prevProps, prevState) => {
        const {navigation} = this.props;

        if (prevProps.notificationResponse !== this.props.notificationResponse && prevState.isCheck) {
            this.hideDialog();
            const {notificationResponse} = this.props;
            if (notificationResponse.code == 200) {
                this.hideDialog();
                this.setState({
                    notificationList: notificationResponse.data.data,
                    isCheck: false,
                });
            } else if (notificationResponse.code === 'rest_forbidden') {
                this.hideDialog();
                navigation.dispatch(
                    CommonActions.reset({
                        index: 0,
                        routes: [
                            {
                                name: 'login',
                            },
                        ],
                    }),
                );
            } else if (notificationResponse.code == 403) {
                this.hideDialog();
                this.showToastAlert(notificationResponse.message);
                const {navigation} = this.props;
                navigation.dispatch(
                    CommonActions.reset({
                        routes: [
                            {name: 'login'},
                        ],
                    }),
                );
            } else if (notificationResponse == 'Network request failed') {
                this.hideDialog();
                this.showToastAlert(Strings.NO_INTERNET);
            } else {
                this.hideDialog();
                this.showToastAlert(Strings.SOMETHING_WENT_WRONG);

            }
        }
    };

    // ----------------------------------------
    // Class local method
    // ----------------------------------------

    itemClick = (item) => {
        const {token} = this.state;
        const {navigate} = this.props.navigation;

        ReadNotification({
            token: token,
            post_id: item.post_id,
        }, response => this.handleReadNotificationResponse(response, token, item.post_id));
    };


    handleReadNotificationResponse = (response, token, id) => {
        const {navigation} = this.props;
        if (response.code == 200) {
            GetPost({
                    token: token,
                    post_id: id,
                }, res => this.GetPostResponse(res),
            );
        } else if (response.code === 'rest_forbidden') {
            this.hideDialog();
            navigation.dispatch(
                CommonActions.reset({
                    index: 0,
                    routes: [
                        {
                            name: 'login',
                        },
                    ],
                }),
            );
        }
    };

    GetPostResponse = (res) => {
        if (res.code == 200) {
            console.warn('res', res);
            this.setState({
                heading: res.message.data.post_title,
                content: res.message.data.post_content,
                isVisible: true,
            });
        } else if (res.code === 'rest_forbidden') {
            this.hideDialog();
            navigation.dispatch(
                CommonActions.reset({
                    index: 0,
                    routes: [
                        {
                            name: 'login',
                        },
                    ],
                }),
            );
        }

    };


    // ----------------------------------------
    _openDrawer = () => {
        const {navigation} = this.props;
        navigation.openDrawer();

    };
    // ----------------------------------------
    _commingSoon = () => {
    };
    // ----------------------------------------
    openHome = () => {
        this.props.navigation.navigate('home');
    };
    // ----------------------------------------
    openSelection = () => {
        this.props.navigation.navigate('selection');

    };
    // ----------------------------------------
    openFeed = () => {
        this.props.navigation.navigate('feed', {dummy: "Dummy"});

    };
    // ----------------------------------------

    openNews = () => {
        this.props.navigation.navigate('news');

    };

    // =============================================================================================
    // Render method for Custom Loader
    // =============================================================================================

    _renderCustomLoader = () => {
        const {isLoading} = this.state;
        return (
            <OrientationLoadingOverlay visible={isLoading} message={Strings.LOADING_TEXT}/>
        );
    };

    async decline() {
        const {token} = this.state;
        await this.setState({
            isVisible: false,
            isCheck: true,
        });
        this.props.getNotificationList({
            token: token,
        });
        // navigation.navigate('notification');
    };


    // ----------------------------------------
    // Main render
    // ----------------------------------------

    render() {
        const {notificationList, chatCount, isVisible, heading, content} = this.state;
        return (
            <SafeAreaView style={{flex: 1, backgroundColor: COLORS.THEME_COLOR}}>
                <StatusBar barStyle="light-content" hidden={false} backgroundColor={COLORS.THEME_COLOR}
                           translucent={true}/>
                <Header
                    openDrawer={() => this._openDrawer()}
                    onPressIcon={() => this._commingSoon()}
                />
                <ScrollView style={styles.mainBG}>
                    <View style={styles.titleBackground}>
                        <Text style={styles.notificationTitleText}>
                            {Strings.LATEST_NOTIFICATION}
                        </Text>
                    </View>
                    <FlatList
                        data={notificationList}
                        renderItem={({item, index}) =>
                            <RenderItem item={item} index={index}
                                        onClickItem={(item) =>
                                            this.itemClick(item)}
                            />
                        }
                        keyExtractor={(item, index) => index.toString()}
                        style={{flex: 1}}
                    />
                    {this._renderCustomLoader()}

                </ScrollView>

                <NotifyModal
                    heading={heading}
                    content={content}
                    decline={() => this.decline()}
                    isVisible={isVisible}
                />

                <BottomTab
                    chatCount={chatCount}
                    openHome={() => this.openHome()}
                    openSelection={() => this.openSelection()}
                    openFeed={() => this.openFeed()}
                    openNews={() => this.openNews()}
                />
            </SafeAreaView>
        );
    }
}


// ----------------------------------------
// ----------------------------------------
// CONNECT
// ----------------------------------------

const mapStateToProps = state => ({
    notificationResponse: state.NotificationReducer.notificationResponse,

});

// ----------------------------------------

const mapDispatchToProps = (dispatch) => {
    return {
        getNotificationList: (payload) => dispatch(NoificationListAction(payload)),
    };
};

// ----------------------------------------

export default connect(mapStateToProps, mapDispatchToProps)(NotificationScreen);
