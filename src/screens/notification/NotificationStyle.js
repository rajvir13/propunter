import {StyleSheet} from 'react-native';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import {COLORS} from '../../themes/Colors';
import {FONTNAME} from '../../utils/FontName';

const styles = StyleSheet.create({
    mainBG: {
        backgroundColor: COLORS.LIGHT_BLACK,
    },
    thinLine: {
        width: wp(100),
        height: wp(.3),
        backgroundColor: COLORS.PLACEHOLDER_TEXT_COLOR,
    },
    titleBackground: {
        flex: 1,
        alignContent: 'flex-start',
        backgroundColor: COLORS.LIGHT_BLACK,
    },
    notificationTitleText: {
        color: COLORS.WHITE_SEMI,
        paddingLeft: wp(5),
        paddingVertical: wp(3),
        fontFamily: FONTNAME.ProximaNova,
        fontSize: wp(4.8),
    },

    rowItem: {
        backgroundColor: COLORS.LIGHT_BLACK,
        flexDirection: 'row',
        justifyContent: 'space-between',
        paddingHorizontal: wp(4),
        paddingVertical: wp(3),
        alignItems: 'center',
        flex: 1,
    },
    textContainer: {
        flexDirection: 'column',
        paddingLeft: wp(5),
    },
    titleText: {
        color: COLORS.WHITE_SEMI,
        fontFamily: FONTNAME.ProximaNova,
        fontSize: wp(3.7),
    },
    detailText: {
        color: COLORS.PLACEHOLDER_TEXT_COLOR,
        fontFamily: FONTNAME.ProximaNovaRegular,
        fontSize: wp(3.2),
        fontWeight: 'bold',
        width: wp(52),
    },
});

export default styles;
