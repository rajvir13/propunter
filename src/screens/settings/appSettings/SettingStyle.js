import React from 'react';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import {StyleSheet} from 'react-native';
import {COLORS} from '../../../themes/Colors';
import {FONTNAME} from '../../../utils/FontName';
import {FONT} from '../../../utils/Sizes';

const styles = StyleSheet.create({
    container: {
        flex: 1, backgroundColor: COLORS.THEME_COLOR,
    },
    headerTittleView: {
        justifyContent: 'center',
        paddingHorizontal: wp(4),
        paddingVertical: wp(6),
        alignItems: 'center',
        fontFamily: FONTNAME.ProximaNova,
        fontWeight: '700',
        fontSize: FONT.TextSmall,
    },
    tittleText: {
        color: COLORS.WHITE_COLOR,
        fontSize: FONT.TextMediumXX,
        fontFamily: FONTNAME.ProximaNova,
    },
    underline: {
        width: wp(100), height: wp(.3), backgroundColor: '#707070',
    },
});

export default styles;
