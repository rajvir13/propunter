// ----------------------------------------
// PACKAGE IMPORTS
// ----------------------------------------
import React, {Component} from 'react';
import {
    View,
    Text,
    ScrollView,
    FlatList,
    SafeAreaView,
    TextInput,
    TouchableWithoutFeedback,
    StatusBar,
    Linking, BackHandler,
} from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';
import {connect} from 'react-redux';

// ----------------------------------------
// LOCAL IMPORTS
// ----------------------------------------
import {COLORS} from '../../../themes/Colors';
import {Strings} from '../../../utils/Strings';
import Header from '../../../components/header';
/*import RenderItem from './component/RenderItem'*/

import BaseClass from '../../../utils/BaseClass';
import OrientationLoadingOverlay from '../../../utils/CustomLoader';
import style from './SettingStyle';
import SettingsData from '../component/SettingsView';
import RenderItem from '../../news';
import {Spacer} from '../../../components/spacer';
import BottomTab from '../../../components/bottomTab';
import {backAction} from '../../../components/helper';


class SettingsScreen extends BaseClass {
    constructor(props) {
        super(props);
        this.state = {
            chatCount: 0,
            isLoading: false,
            newDataNotification: [
                {
                    title: 'NOTIFICATIONS',
                    subTitle: 'Manage your push notification preference for each Tipster',
                },
                {
                    title: 'ACCOUNT DETAILS',
                    subTitle: 'Reset password and manage your account details',
                },
                {
                    title: 'CONTACT US',
                    subTitle: 'Click on this link to send us an email.',
                },
            ],


        };
        this.onEndReachedCalledDuringMomentum = true;
    }

    componentDidMount() {
        AsyncStorage.getItem('chatCount', (err, chatCount) => {
            // console.warn('Count-->', chatCount);
            if (!this.isConnected()) {
                this.showToastAlert(Strings.NO_INTERNET);
            }
            this.setState(
                {
                    chatCount: chatCount,
                },
            );
        });
    }

    openHome = () => {
        this.props.navigation.navigate('home');
    };
    openSelection = () => {
        this.props.navigation.navigate('selection');

    };
    openFeed = () => {
        this.props.navigation.navigate('feed', {dummy: 'Dummy'});
    };

    openNews = () => {
        this.props.navigation.navigate('news');

    };


    // ----------------------------------------
    // Class Life cycle method
    // ----------------------------------------


    _onPressItem = (item, index) => {
        const {navigate} = this.props.navigation;
        //console.warn('ItemIndex-->>', index);
        switch (index) {
            case 0:
                navigate('notifications');
                break;
            case 1:
                navigate('accounts', {dummy: 'Dummy'});
                break;
            case 2:
                if (!this.isConnected()) {
                    this.showToastAlert(Strings.NO_INTERNET);
                }
                Linking.openURL('mailto:pro@propunteralert.com');
                // navigate('notifications');
                //  alert('coming soon');
                break;
        }


    };


    _openDrawer = () => {
        const {navigation} = this.props;
        navigation.openDrawer();

    };

    // =============================================================================================
    // Render method for Custom Loader
    // =============================================================================================

    _renderCustomLoader = () => {
        const {isLoading} = this.state;
        return (
            <OrientationLoadingOverlay visible={isLoading} message={Strings.LOADING_TEXT}/>
        );
    };


    _commingSoon = () => {
        alert('coming soon');
    };

    // ----------------------------------------
    // Main render
    // ----------------------------------------

    render() {
        const {newPost, chatCount} = this.state;
        return (


            <SafeAreaView style={style.container}>
                <StatusBar barStyle="light-content" hidden={false} backgroundColor={COLORS.THEME_COLOR}
                           translucent={true}/>
                <Header
                    openDrawer={() => this._openDrawer()}
                    onPressIcon={() => this._commingSoon()}
                />
                <ScrollView style={{backgroundColor: COLORS.LIGHT_BLACK}}>
                    <View style={{alignContent: 'flex-start'}}>
                        <View style={style.headerTittleView}>
                            <Text style={style.tittleText}>{Strings.APP_SETTINGS}</Text>
                        </View>
                        <View style={style.underline}></View>

                        <FlatList
                            data={this.state.newDataNotification}
                            extraData={this.state}
                            keyExtractor={this.keyExtractor}
                            scrollEnabled={false}
                            renderItem={({item, index}) =>
                                <SettingsData
                                    item={item}
                                    openItem={(item, index) => this._onPressItem(item, index)}
                                    index={index}
                                />
                            }
                        />
                        <Spacer space={2}/>
                    </View>
                    {this._renderCustomLoader()}
                </ScrollView>
                <BottomTab
                    chatCount={chatCount}
                    openHome={() => this.openHome()}
                    openSelection={() => this.openSelection()}
                    openFeed={() => this.openFeed()}
                    openNews={() => this.openNews()}
                />
            </SafeAreaView>

        );
    }
}

// ----------------------------------------
// ----------------------------------------
// CONNECT
// ----------------------------------------
/*
const mapStateToProps = state => ({
    newPostResponse: state.NewPostReducer.newPostResponse,

});

// ----------------------------------------

const mapDispatchToProps = (dispatch) => {
    return {
        getNewsPost: (payload) => dispatch(NewsPostAction(payload)),
    };
};*/

// ----------------------------------------

export default (SettingsScreen);
