import React, {Component} from 'react';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import {View, Text, Image, FlatList, TextInput, TouchableWithoutFeedback, Keyboard} from 'react-native';
import {COLORS} from '../../../../themes/Colors';
import {FONTNAME} from '../../../../utils/FontName';
import {Spacer} from '../../../../components/spacer';
import style from './styles';
import {TouchableOpacity} from 'react-native-gesture-handler';
import Icon from 'react-native-vector-icons/AntDesign';


const SettingsData = (props) => {
    const {item, openItem, index} = props;
    return <View>
        <TouchableOpacity
            onPress={() => openItem(item, index)}
            style={style.childContainer}>
            <View style={{flexDirection: 'column',height:wp(12)}}>
                <Text style={style.tittleText}>{item.title}</Text>
                <Spacer space={1}/>
                <Text style={style.detailsText}
                >{item.subTitle}</Text>

            </View>

            <View style={{}}>
                <Icon
                    style={{marginTop: 2}}
                    name={'rightcircleo'}
                    size={wp('6%')}
                    color={COLORS.WHITE_COLOR}/>

            </View>
        </TouchableOpacity>
        <Spacer space={1}/>
        <View style={style.underline}></View>
    </View>;
};
export default SettingsData;
