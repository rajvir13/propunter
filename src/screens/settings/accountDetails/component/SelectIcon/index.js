import React, {Component} from 'react';
import {widthPercentageToDP as wp} from 'react-native-responsive-screen';
import {View, Text, Image, TouchableOpacity} from 'react-native';


import style from './Style';
import {FONT} from '../../../../../utils/Sizes';
import {COLORS} from '../../../../../themes/Colors';
import {Spacer} from '../../../../../components/spacer';


const SelectIcon = ({data, index,onPress}) => {
    //  console.warn('yyyyy',data)
    return (
        //{/*<View style={style.childContainer}>*/}

            <View style={{
                // marginLeft: 4,
                borderWidth: 1,
                height: wp(14),
                width: wp(14),
                marginHorizontal: wp(.75),
                marginBottom: wp(1.5),
                borderColor: COLORS.PLACEHOLDER_TEXT_COLOR,
                alignItems: 'center',
                justifyContent: 'center',

            }}>
                <TouchableOpacity
                    onPress={onPress}
                >
                <Image
                    resizeMode='contain'
                    source={{uri: data.path}}
                    style={{width: wp(9), height: wp(9), borderWidth: 1}}
                />
                </TouchableOpacity>
            </View>
        // </View>

    );
};
export default SelectIcon;
