// ----------------------------------------
// PACKAGE IMPORTS
// ----------------------------------------
import React, {Component} from 'react';
import {
    View,
    Text,
    ScrollView,
    Alert,
    SafeAreaView,
    TextInput,
    StatusBar,
    TouchableOpacity,
    KeyboardAvoidingView,
    BackHandler,
    Image,
    FlatList,
} from 'react-native';

// ----------------------------------------
// LOCAL IMPORTS
// ----------------------------------------
import {COLORS} from '../../../../themes/Colors';
import {Strings} from '../../../../utils/Strings';
import Header from '../../../../components/header';
import BaseClass from '../../../../utils/BaseClass';
import OrientationLoadingOverlay from '../../../../utils/CustomLoader';
import style from './SelectIconStyle';
import Icon from 'react-native-vector-icons/AntDesign';
import {widthPercentageToDP as wp} from 'react-native-responsive-screen';
import {Spacer} from '../../../../components/spacer';
import {CommonActions} from '@react-navigation/native';
import NewPrimaryButton from '../../../../components/buttons/NewPrimaryButton';
import Validations from '../../../../utils/Validations';
import {FONT} from '../../../../utils/Sizes';
import {FONTNAME} from '../../../../utils/FontName';
import AsyncStorage from '@react-native-community/async-storage';
import {LoginAction} from '../../../../redux/actions/LoginAction';
import {connect} from 'react-redux';
import BottomTab from '../../../../components/bottomTab';
import {backAction} from '../../../../components/helper';
import SelectIcon from '../../../settings/accountDetails/component/SelectIcon';
import {GetIconAction} from '../../../../redux/actions/SelectIconAction';
import GetIconReducer from '../../../../redux/reducers/GetProfilereIconducer';
import GetProfileIconReducer from '../../../../redux/reducers/GetProfilereIconducer';
import {UpdateIconAction} from '../../../../redux/actions/UpdateIconAction';

class SelectIconScreen extends BaseClass {
    constructor(props) {
        super(props);
        this.state = {

            isLoading: false,
            token: undefined,
            isCheck: false,
            iconimage: undefined,
            emoId: undefined,
            username: undefined,
            data: [],

        };
    }


    // ----------------------------------------
    // Class life cycle
    // ----------------------------------------


    componentDidMount = () => {
        const {navigation} = this.props;
        this._unsubscribe = navigation.addListener('focus', () => {
            this.onFocusFunction();
        });
        AsyncStorage.getItem(Strings.TOKEN, (err, token) => {
            console.warn(token);
            this.setState({
                token: JSON.parse(token),
                isCheck: true,
            });
            if (token !== undefined && token !== null) {
                this.props.getSelectIcon({
                    token: JSON.parse(token),

                });

            }
        });
    };


    onFocusFunction = () => {
        this.componentDidMount();
    };

    componentDidUpdate = (prevProps, prevState) => {
        const {updateProfileResponse} = this.props;
        const {navigation} = this.props;
        // console.warn("result-->",updateProfileResponse.data.path)
        if (prevProps.updateProfileResponse !== updateProfileResponse && prevState.isCheck) {
            if (updateProfileResponse !== undefined && updateProfileResponse !== null) {
                if (updateProfileResponse.data !== null && updateProfileResponse.data !== undefined) {
                    if (updateProfileResponse.code === 200) {
                        this.hideDialog();
                        this.setState({
                            data: updateProfileResponse.data.data,
                            isCheck: false,
                        });

                    } else if (updateProfileResponse.code === 'rest_forbidden') {
                        this.hideDialog();
                        navigation.dispatch(
                            CommonActions.reset({
                                index: 0,
                                routes: [
                                    {
                                        name: 'login',
                                    },
                                ],
                            }),
                        );
                    } else if (updateProfileResponse.code == 403) {
                        this.hideDialog();
                        this.showToastAlert(updateProfileResponse.message);
                    } else if (updateProfileResponse == 'Network request failed') {
                        this.hideDialog();
                        this.showToastAlert(Strings.NO_INTERNET);
                    }

                }
            }
        }
    };


    componentWillUnmount() {
        BackHandler.addEventListener('hardwareBackPress', backAction);
    }

    /* UNSAFE_componentWillReceiveProps(nextProps: Readonly<P>, nextContext: any) {
         const {getIconResponse} = nextProps;
         const {navigation} = this.props;
         this.hideDialog();
         if (getIconResponse !== undefined) {
             if (getIconResponse.code == 200) {
                 console.warn("response", getIconResponse)
                 this.setState({
                     profileImage:getIconResponse.data[0].path,
                 });
                 this.showToastSucess(getIconResponse.data);
             } else if (getIconResponse.code === 400) {
                 this.hideDialog();
                 this.showToastAlert(getIconResponse.data);
             } else if (getIconResponse.code === 'rest_forbidden') {
                 this.hideDialog();
                 navigation.dispatch(
                     CommonActions.reset({
                         index: 0,
                         routes: [
                             {
                                 name: 'login',
                             },
                         ],
                     }),
                 );
             } else {
                 this.hideDialog();
             }
         }
     }
 */

    openHome = () => {
        this.props.navigation.navigate('home');
    };
    openSelection = () => {
        this.props.navigation.navigate('selection');

    };
    openFeed = () => {
        this.props.navigation.navigate('feed', {dummy: "Dummy"});
    };

    openNews = () => {
        this.props.navigation.navigate('news');

    };


    // ----------------------------------------
    // Class Life cycle method
    // ----------------------------------------


    _openDrawer = () => {
        const {navigation} = this.props;
        navigation.openDrawer();

    };

    // =============================================================================================
    // Render method for Custom Loader
    // =============================================================================================

    _renderCustomLoader = () => {
        const {isLoading} = this.state;
        return (
            <OrientationLoadingOverlay visible={isLoading} message={Strings.LOADING_TEXT}/>
        );
    };


    _commingSoon = () => {
        alert('coming soon');
    };


    _goBack = () => {
        /*this.props.navigation.dispatch(
            CommonActions.reset({
                index: 1,
                routes: [
                    {name: 'settings'},
                ],
            }),
        );*/
        const {navigation} = this.props;
        navigation.goBack();
    };

    iconSelectClick = (item) => {
        const {navigation} = this.props;
        const {navigate} = navigation;
        navigate('accounts', {selectedIcon: item});
    };

    handleUpdateIconResponse = (response) => {
        if (response !== undefined) {

        } else {
            this.hideDialog();
            this.showToastAlert(Strings.SOMETHING_WENT_WRONG);
        }
    };

    // ----------------------------------------
    // Main render
    // ----------------------------------------

    render() {

        //  console.warn('chkk-->>', this.state.name);
        const {navigate} = this.props.navigation;
        const {data} = this.state;
        return (
            <KeyboardAvoidingView
                style={{flex: 1, backgroundColor: COLORS.WHITE_COLOR}}
                behavior={(Platform.OS === 'ios') ? 'padding' : null}>
                <SafeAreaView style={style.container}>
                    <StatusBar barStyle="light-content" hidden={false} backgroundColor={COLORS.THEME_COLOR}
                               translucent={true}/>
                    <Header
                        openDrawer={() => this._openDrawer()}
                        onPressIcon={() => this._commingSoon()}
                    />
                    <ScrollView style={{backgroundColor: COLORS.LIGHT_BLACK}}>
                        <View style={{}}>
                            <View style={style.headerTittleView}>
                                <TouchableOpacity style={{flexDirection: 'row', width: wp(30)}}
                                                  onPress={() => this._goBack()}>
                                    <Icon
                                        style={{marginTop: 1.8}}
                                        name={'left'}
                                        size={wp('3%')} color={COLORS.PLACEHOLDER_TEXT_COLOR}/>
                                    <Spacer row={.5}/>
                                    <Text style={{
                                        alignSelf: 'center',
                                        color: COLORS.PLACEHOLDER_TEXT_COLOR,
                                        fontSize: FONT.TextExtraSmall,
                                        fontFamily: FONTNAME.ProximaNova,
                                    }}>BACK</Text>
                                </TouchableOpacity>
                                <Text style={style.headerTittleText}>{Strings.SELECT_ICON}</Text>


                            </View>
                        </View>
                        <FlatList
                            numColumns={6}
                            data={data}
                            style={{width: wp(100), paddingTop: wp(3),paddingHorizontal: wp(3.5)}}
                            renderItem={({item, index}) =>
                                <SelectIcon
                                    data={item}
                                    index={index}
                                    onPress={() => this.iconSelectClick(item)}
                                />
                            }
                            keyExtractor={(item, index) => index.toString()}
                        />
                        {this._renderCustomLoader()}
                    </ScrollView>
                    <BottomTab
                        openHome={() => this.openHome()}
                        openSelection={() => this.openSelection()}
                        openFeed={() => this.openFeed()}
                        openNews={() => this.openNews()}
                    />
                </SafeAreaView>
            </KeyboardAvoidingView>

        );
    }
}

// ----------------------------------------
// ----------------------------------------
// CONNECT
// ----------------------------------------


const mapStateToProps = state => ({
    updateProfileResponse: state.GetProfileIconReducer.updateProfileResponse,
    updateIconResponse: state.GetProfileIconReducer.updateIconResponse,


});

// ----------------------------------------

const mapDispatchToProps = (dispatch) => {
    return {
        getSelectIcon: (payload) => dispatch(GetIconAction(payload)),
    };
};

// ----------------------------------------

export default connect(mapStateToProps, mapDispatchToProps)(SelectIconScreen);
