// ----------------------------------------
// PACKAGE IMPORTS
// ----------------------------------------
import React, {Component} from 'react';
import {
    View,
    Text,
    ScrollView,
    Alert,
    SafeAreaView,
    TextInput,
    StatusBar,
    TouchableOpacity,
    KeyboardAvoidingView,
    BackHandler,
    Image,
} from 'react-native';

// ----------------------------------------
// LOCAL IMPORTS
// ----------------------------------------
import {COLORS} from '../../../themes/Colors';
import {Strings} from '../../../utils/Strings';
import Header from '../../../components/header';
import BaseClass from '../../../utils/BaseClass';
import OrientationLoadingOverlay from '../../../utils/CustomLoader';
import style from './AccountStyle';
import Icon from 'react-native-vector-icons/AntDesign';
import {widthPercentageToDP as wp} from 'react-native-responsive-screen';
import {Spacer} from '../../../components/spacer';
import {CommonActions} from '@react-navigation/native';
import NewPrimaryButton from '../../../components/buttons/NewPrimaryButton';
import Validations from '../../../utils/Validations';
import {FONT} from '../../../utils/Sizes';
import {FONTNAME} from '../../../utils/FontName';
import AsyncStorage from '@react-native-community/async-storage';
import {LoginAction} from '../../../redux/actions/LoginAction';
import {connect} from 'react-redux';
import {UpdateProfileAction, GetProfileAction} from '../../../redux/actions/UpdateProfileAction';
import {TipserPostAction} from '../../../redux/actions/NewPostAction';
import BottomTab from '../../../components/bottomTab';
import {backAction} from '../../../components/helper';
import {UpdateIconAction} from '../../../redux/actions/UpdateIconAction';

class AccountScreen extends BaseClass {
    constructor(props) {
        super(props);
        this.state = {
            name: undefined,
            lastname: undefined,
            email: undefined,
            password: '',
            confirmpassword: '',
            userid: undefined,
            isLoading: false,
            token: undefined,
            chatCount: 0,
            displayname: undefined,
            profileImage: '',
            previousData: {},
            selectedIconData: {},
        };
    }


    // ----------------------------------------
    // Class life cycle
    // ----------------------------------------


    componentDidMount = () => {
        BackHandler.removeEventListener('hardwareBackPress', backAction);
        AsyncStorage.getItem('chatCount', (err, chatCount) => {
            //console.warn('Count-->', chatCount);
            this.setState(
                {
                    chatCount: chatCount,
                },
            );
        });
        AsyncStorage.getItem(Strings.TOKEN, (err, token) => {
            if (token !== undefined && token !== null) {
                this.showDialog();
                this.setState({
                    token: JSON.parse(token),
                });

                this.props.getUserDetails({
                    token: JSON.parse(token),
                });
            }
        });
    };


    componentWillUnmount() {
        BackHandler.addEventListener('hardwareBackPress', backAction);

    }

    UNSAFE_componentWillReceiveProps(nextProps: Readonly<P>, nextContext: any) {
        const {updateProfileResponse, route} = nextProps;
        const {navigation} = this.props;
        const {previousData} = this.state;

        if(route.params !== undefined){
            if(route.params.dummy !== undefined && route.params.dummy !== ""){
                navigation.setParams({dummy: ""});
                this.componentDidMount()
            }
        }
        if (route.params !== undefined && route.params.selectedIcon !== undefined) {
            this.setState({
                selectedIconData: route.params.selectedIcon,
                profileImage: route.params.selectedIcon.path,
            });
        }
        this.hideDialog();
        if (updateProfileResponse !== undefined) {
            if (updateProfileResponse.code == 200) {
                if (previousData !== updateProfileResponse.message) {
                   /* console.warn("w4");
                    console.warn('response', updateProfileResponse.message);*/
                    this.setState({
                        name: updateProfileResponse.message.first_name,
                        lastname: updateProfileResponse.message.last_name,
                        displayname: updateProfileResponse.message.display_name,
                        email: updateProfileResponse.message.email,
                        profileImage: updateProfileResponse.message.profile_image,
                        previousData: updateProfileResponse.message,
                    });
                    this.showToastSucess(updateProfileResponse.data);
                }


            } else if (updateProfileResponse.code === 400) {
                this.hideDialog();
                this.showToastAlert(updateProfileResponse.data);
            } else if (updateProfileResponse.code === 'rest_forbidden') {
                this.hideDialog();
                navigation.dispatch(
                    CommonActions.reset({
                        index: 0,
                        routes: [
                            {
                                name: 'login',
                            },
                        ],
                    }),
                );
            } else {
                this.hideDialog();
            }
        }
    }


    openHome = () => {
        this.props.navigation.navigate('home');
    };
    openSelection = () => {
        this.props.navigation.navigate('selection');

    };
    openFeed = () => {
        this.props.navigation.navigate('feed', {dummy: "Dummy"});
    };

    openNews = () => {
        this.props.navigation.navigate('news');

    };


    // ----------------------------------------
    // Class Life cycle method
    // ----------------------------------------


    _openDrawer = () => {
        const {navigation} = this.props;
        navigation.openDrawer();

    };

    // =============================================================================================
    // Render method for Custom Loader
    // =============================================================================================

    _renderCustomLoader = () => {
        const {isLoading} = this.state;
        return (
            <OrientationLoadingOverlay visible={isLoading} message={Strings.LOADING_TEXT}/>
        );
    };


    _commingSoon = () => {
        alert('coming soon');
    };


    _goBack = () => {
        /*this.props.navigation.dispatch(
            CommonActions.reset({
                index: 1,
                routes: [
                    {name: 'settings'},
                ],
            }),
        );*/
        const {navigation} = this.props;
        navigation.goBack();
    };


    _updateDetails = () => {
        const {navigate} = this.props.navigation;
        const {name, lastname, token, email, password, confirmpassword, displayname, profileImage, selectedIconData} = this.state;
        console.warn('display', displayname);
        if (name === undefined || name.trim().length === 0) {
            this.showToastAlert(Strings.FIRSTNAME);
        } else if (lastname === undefined || lastname.trim().length === 0) {
            this.showToastAlert(Strings.LASTNAME);
        } else if (displayname === undefined || displayname.trim().length === 0) {
            this.showToastAlert(Strings.DISPALYNAME);
        } else if (password.length > 0) {
            if (!Validations.validatePassword(password)) {
                this.showToastAlert(Strings.ENTER_VALID_PASSWORD);
            } else if (confirmpassword.length < 1) {
                this.showToastAlert(Strings.CONF_PASSWORD);
            } else if (password !== confirmpassword) {
                this.showToastAlert(Strings.PASS_MATCH);
            } else {
                this.showDialog();
                if (selectedIconData.id !== undefined) {
                    console.warn("w1");
                    UpdateIconAction({
                        id: selectedIconData.id,
                        token: token,
                    }, response => this.handleUpdateIconResponse(response));
                } else {
                    this.props.updateApi({
                        first_name: name,
                        last_name: lastname,
                        display_name: displayname,
                        password: password,
                        confirm_password: confirmpassword,
                        // profile_image: profileImage,
                        token: token,
                        email: email,

                    });
                }
            }
        } else {
            this.showDialog();
            if (selectedIconData.id !== undefined) {
                UpdateIconAction({
                    id: selectedIconData.id,
                    token: token,
                }, response => this.handleUpdateIconResponse(response));
            } else {
                this.props.updateApi({
                    first_name: name,
                    last_name: lastname,
                    display_name: displayname,
                    password: password,
                    confirm_password: confirmpassword,
                    // profile_image: profileImage,
                    token: token,
                    email: email,
                });
            }


        }
        //  this.hideDialog();
    };


    handleUpdateIconResponse = (response) => {
        const {navigation} = this.props;
        const {name, lastname, token, email, password, confirmpassword, displayname, profileImage} = this.state;
        if (response !== undefined) {
            if (response.code === 200) {
                console.warn("w3");
                this.props.updateApi({
                    first_name: name,
                    last_name: lastname,
                    display_name: displayname,
                    password: password,
                    confirm_password: confirmpassword,
                    // profile_image: profileImage,
                    token: token,
                    email: email,
                });
            } else if (response.code === 'rest_forbidden') {
                this.hideDialog();
                navigation.dispatch(
                    CommonActions.reset({
                        index: 0,
                        routes: [
                            {
                                name: 'login',
                            },
                        ],
                    }),
                );
            } else {
                this.hideDialog();
                this.showToastAlert(Strings.SOMETHING_WENT_WRONG);
            }
        } else {
            this.hideDialog();
        }


    };


    // ----------------------------------------
    // Main render
    // ----------------------------------------

    render() {

        //  console.warn('chkk-->>', this.state.name);
        const {navigate} = this.props.navigation;
        const {newPost, chatCount, profileImage} = this.state;
        return (
            <KeyboardAvoidingView
                style={{flex: 1, backgroundColor: COLORS.WHITE_COLOR}}
                behavior={(Platform.OS === 'ios') ? 'padding' : null}>
                <SafeAreaView style={style.container}>
                    <StatusBar barStyle="light-content" hidden={false} backgroundColor={COLORS.THEME_COLOR}
                               translucent={true}/>
                    <Header
                        openDrawer={() => this._openDrawer()}
                        onPressIcon={() => this._commingSoon()}
                    />
                    <ScrollView style={{backgroundColor: COLORS.LIGHT_BLACK}}>
                        <View style={{}}>
                            <View style={style.headerTittleView}>
                                <TouchableOpacity style={{flexDirection: 'row', width: wp(30)}}
                                                  onPress={() => this._goBack()}>
                                    <Icon
                                        style={{marginTop: 1.8}}
                                        name={'left'}
                                        size={wp('3%')} color={COLORS.PLACEHOLDER_TEXT_COLOR}/>
                                    <Spacer row={.5}/>
                                    <Text style={{
                                        alignSelf: 'center',
                                        color: COLORS.PLACEHOLDER_TEXT_COLOR,
                                        fontSize: FONT.TextExtraSmall,
                                        fontFamily: FONTNAME.ProximaNova,
                                    }}>BACK</Text>
                                </TouchableOpacity>
                                <Text style={style.headerTittleText}>{Strings.ACCOUNT_SETTINGS}</Text>
                            </View>
                            <View style={style.underline}/>
                            <View style={style.mainView}>
                                <Text style={style.inputText}>{Strings.DISPLAY_NAME}</Text>
                                <TextInput style={style.textInputStyle}
                                    //  ref="DispalyName"
                                           underlineColorAndroid="transparent"
                                           placeholder={Strings.LAST_NAME_HERE}
                                           placeholderTextColor={COLORS.PLACEHOLDER_TEXT_COLOR}
                                           autoCapitalize="none"
                                           keyboardType="email-address"
                                           returnKeyType={'next'}
                                           value={this.state.displayname}
                                           onSubmitEditing={() => {
                                               this.refs.FirstName.focus();
                                           }}
                                           onChangeText={text => this.setState({
                                               displayname: text,
                                           })}
                                />
                            </View>
                            <View style={style.underline}/>
                            <View style={style.mainDispalyView}>
                                <Text style={style.inputInnerText}>{Strings.DISPLAY_ICON}</Text>
                                <View style={{
                                    flexDirection: 'row',
                                    justifyContent: 'space-between',
                                }}>
                                    <Text onPress={() => navigate('selecticon')} style={style.changeIconText}>{Strings.CHANGE_ICON}</Text>
                                    <View style={{
                                        marginLeft: wp(4),
                                        borderWidth: 0.5,
                                        height: wp(13),
                                        width: wp(13),
                                        borderColor: COLORS.WHITE_COLOR,
                                        alignItems: 'center',
                                        justifyContent: 'center',
                                    }}>


                                        <TouchableOpacity
                                            onPress={() => navigate('selecticon')}
                                        >
                                            <Image
                                                resizeMode='contain'
                                                source={{uri: profileImage}}
                                                /*  onPress={() => navigate('forgot')}*/
                                                style={{width: wp(9), height: wp(9)}}
                                            />
                                        </TouchableOpacity>
                                    </View>
                                </View>

                            </View>

                            <View style={style.underline}/>
                            <View style={style.mainView}>
                                <Text style={style.inputText}>{Strings.NAME_TEXT}</Text>
                                <TextInput style={style.textInputStyle}
                                           ref="FirstName"
                                           underlineColorAndroid="transparent"
                                           placeholder={Strings.NAME_HERE}
                                           placeholderTextColor={COLORS.PLACEHOLDER_TEXT_COLOR}

                                           autoCapitalize="none"
                                           keyboardType="email-address"
                                           returnKeyType={'next'}
                                           value={this.state.name}
                                           onSubmitEditing={() => {
                                               this.refs.LastName.focus();
                                           }}
                                           onChangeText={text => this.setState({
                                               name: text,
                                           })}
                                />
                            </View>

                            <View style={style.underline}/>
                            <View style={style.mainView}>
                                <Text style={style.inputText}>{Strings.LAST_NAME}</Text>
                                <TextInput style={style.textInputStyle}
                                           ref="LastName"
                                           underlineColorAndroid="transparent"
                                           placeholder={Strings.LAST_NAME_HERE}
                                           placeholderTextColor={COLORS.PLACEHOLDER_TEXT_COLOR}
                                           autoCapitalize="none"
                                           keyboardType="email-address"
                                           returnKeyType={'next'}
                                           value={this.state.lastname}
                                           onSubmitEditing={() => {
                                               this.refs.Email.focus();
                                           }}
                                           onChangeText={text => this.setState({
                                               lastname: text,
                                           })}
                                />
                            </View>


                            <View style={style.underline}/>


                            <View style={style.mainView}>
                                <Text style={style.inputText}>{Strings.EMAIL_ADDRESS}</Text>
                                <TextInput style={style.textInputStyle}
                                           ref="Email"
                                           underlineColorAndroid="transparent"
                                           placeholder={Strings.EMAIL_ADDRESS_HERE}
                                           placeholderTextColor={COLORS.PLACEHOLDER_TEXT_COLOR}
                                           autoCapitalize="none"
                                           keyboardType="email-address"
                                           returnKeyType={'next'}
                                           editable={false}
                                           value={this.state.email}
                                           onSubmitEditing={() => {
                                               this.refs.Password.focus();
                                           }}
                                           onChangeText={text => this.setState({
                                               email: text,
                                           })}
                                />
                            </View>

                            <View style={style.underline}/>

                            <View style={style.mainView}>
                                <Text style={style.inputText}>{Strings.PASSWORD_TEXT}</Text>
                                <TextInput style={style.textInputStyle}
                                           ref="Password"
                                           underlineColorAndroid="transparent"
                                           secure
                                           placeholder={Strings.PASSWORD_TEXT_HERE}
                                           placeholderTextColor={COLORS.PLACEHOLDER_TEXT_COLOR}
                                           autoCapitalize="none"
                                           keyboardType="email-address"
                                           secureTextEntry={true}
                                           returnKeyType={'next'}
                                           onSubmitEditing={() => {
                                               this.refs.confirmPassword.focus();
                                           }}
                                           onChangeText={text => this.setState({
                                               password: text.trim(),
                                           })}
                                />
                            </View>

                            <View style={style.underline}/>
                            <View style={style.mainView}>
                                <Text style={style.inputText}>{Strings.CONFIRM_PASS_TEXT}</Text>
                                <TextInput style={style.textInputStyle}
                                           ref="confirmPassword"
                                           underlineColorAndroid="transparent"
                                           secureTextEntry={true}
                                           placeholder={Strings.CONFIRM_PASS_TEXT_HERE}
                                           placeholderTextColor={COLORS.PLACEHOLDER_TEXT_COLOR}
                                           autoCapitalize="none"
                                           keyboardType="email-address"
                                           returnKeyType={'next'}
                                    /* onSubmitEditing={() => {
                                         this.refs.Password.focus();
                                     }}*/
                                           onChangeText={text => this.setState({
                                               confirmpassword: text,
                                           })}
                                />
                            </View>

                            <View style={style.underline}/>
                            <Spacer space={6}/>
                            <NewPrimaryButton
                                onButtonPress={() => this._updateDetails()}
                                label={Strings.UPDATE_DETAILS}/>
                            <Spacer space={2}/>
                        </View>
                        {this._renderCustomLoader()}
                    </ScrollView>
                    <BottomTab
                        chatCount={chatCount}
                        openHome={() => this.openHome()}
                        openSelection={() => this.openSelection()}
                        openFeed={() => this.openFeed()}
                        openNews={() => this.openNews()}
                    />
                </SafeAreaView>
            </KeyboardAvoidingView>

        );
    }
}

// ----------------------------------------
// ----------------------------------------
// CONNECT
// ----------------------------------------

const mapStateToProps = state => ({
    updateProfileResponse: state.UpdateProfileReducer.updateProfileResponse,
});

// ----------------------------------------

const mapDispatchToProps = (dispatch) => {
    return {
        getUserDetails: (payload) => dispatch(GetProfileAction(payload)),
        updateApi: (payload) => dispatch(UpdateProfileAction(payload)),
    };
};

// ----------------------------------------

export default connect(mapStateToProps, mapDispatchToProps)(AccountScreen);
