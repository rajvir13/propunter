import React from 'react';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import {StyleSheet} from 'react-native';
import {COLORS} from '../../../themes/Colors';
import {FONTNAME} from '../../../utils/FontName';
import {FONT} from '../../../utils/Sizes';

const styles = StyleSheet.create({
    container: {
        flex: 1, backgroundColor: COLORS.THEME_COLOR,
    },
    headerTittleView: {
        flexDirection: 'row',
        paddingHorizontal: wp(4),
        paddingVertical: wp(5),
        alignItems: 'center',

    },
    tittleText: {
        color: COLORS.WHITE_COLOR,
        fontWeight: '700',
        fontSize: FONT.TextSmall,
        fontFamily: FONTNAME.ProximaNova,
    },
    underline: {
        width: wp(100), height: wp(.3), backgroundColor: '#707070',
    },
    headerTittleText: {
        color: COLORS.WHITE_COLOR,
        fontSize: FONT.TextMediumXX,
        fontFamily: FONTNAME.ProximaNova,


    },
    textInputStyle: {
        height: wp(13),
        width: wp(70),
        borderEndColor: 'green',
        fontSize: wp(4.5),
        fontFamily: FONTNAME.ProximaNova,
        color: COLORS.WHITE_COLOR,


    },
    inputText: {
        color: COLORS.WHITE_COLOR,
        fontWeight: '700',
        fontSize: FONT.TextSmall,
        fontFamily: FONTNAME.ProximaNova,

        paddingVertical: wp(3),
    },
    mainView: {
        alignItems: 'flex-start',
        flexDirection: 'column',
        paddingHorizontal: wp(5),
    },
    mainDispalyView: {
        alignItems: 'flex-start',
        flexDirection: 'row',
        padding: wp(5),
        height: wp(22),
        justifyContent:'space-between'

    },
    inputInnerText: {
        color: COLORS.WHITE_COLOR,
        fontWeight: '700',
        fontSize: FONT.TextSmall,
        fontFamily: FONTNAME.ProximaNova,

      
    },
    changeIconText: {
        color: COLORS.WHITE_COLOR,
        fontSize: FONT.TextSmall,
        fontFamily: FONTNAME.ProximaNovaRegular,


    },
});

export default styles;
