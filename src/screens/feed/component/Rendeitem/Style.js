import { StyleSheet } from "react-native";
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from "react-native-responsive-screen";
import { COLORS } from '../../../../themes/Colors';
import { FONTNAME } from '../../../../utils/FontName';
const styles = StyleSheet.create({
    mainContainer: {
        paddingHorizontal: wp(6),
        marginBottom: wp(6)
    },
    chatContainer: {
        flexDirection: 'row',
        alignItems: 'center',
        paddingHorizontal: wp(1)
    },
    userImage: {
        height: 15,
        width: 15
    },
    userName: {
        fontFamily: FONTNAME.ProximaNovaRegular,
        fontSize:wp(2.7),
        color: COLORS.PLACEHOLDER_TEXT_COLOR
    },
    messageReciever: {
        backgroundColor: '#EEEEEE',
        borderRadius: wp(2),
        paddingVertical: wp(3),
        width: wp(65),
        paddingHorizontal: wp(4)
    },
    messageSender: {
        borderRadius: wp(2),
        paddingVertical: wp(3),
        backgroundColor: COLORS.LIGHT_BLACK, width: wp(65),
        paddingHorizontal: wp(4)
    },
    reciever: {
        fontFamily: FONTNAME.ProximaNovaRegular,
        fontSize: wp(3.7),
        color: COLORS.PLACEHOLDER_TEXT_COLOR
    },
    sender: {
        fontFamily: FONTNAME.ProximaNovaRegular,
        fontSize: wp(3.7),
        color: COLORS.WHITE_SEMI
    },




});

export default styles;
