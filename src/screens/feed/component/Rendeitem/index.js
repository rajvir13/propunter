
// ----------------------------------------
// PACKAGE IMPORTS
// ----------------------------------------
import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from "react-native-responsive-screen";
import { View, Text, ScrollView, Image, FlatList, SafeAreaView, TextInput, Platform, KeyboardAvoidingView, StatusBar } from "react-native";
// ----------------------------------------
// LOCAL IMPORTS
// ----------------------------------------
import { COLORS } from '../../../../themes/Colors';
import { Spacer } from '../../../../components/spacer';
import style from './Style'
const RenderItem = (props) => {
    const { item, userId } = props
    return (
        <View style={style.mainContainer}>
            {
                parseInt(item.user_id) !== userId ?
                    <View>
                        <View style={style.chatContainer}>
                            <Image
                                resizeMode='contain'
                                style={style.userImage}
                                source={{
                                    uri: item.profile_image
                                }} />
                            <Spacer row={1} />
                            <Text style={style.userName}>{(item.display_name_in_app !== undefined && item.display_name_in_app !== null) ? item.display_name_in_app.toUpperCase() : item.user_name.toUpperCase()}</Text>
                        </View>
                        <Spacer space={1} />
                        <View style={style.messageReciever}>
                            <Text style={style.reciever}>{item.message}</Text>
                        </View>
                    </View>
                    :

                    // <Spacer space={3} />
                    <View style={{ alignItems: 'flex-end', }} >
                        <View style={style.chatContainer}>
                            <Text style={style.userName}>{(item.display_name_in_app !== undefined && item.display_name_in_app !== null) ? item.display_name_in_app.toUpperCase() : item.user_name.toUpperCase()}</Text>
                            <Spacer row={1} />
                            <Image
                                style={style.userImage}
                                source={{
                                    uri: item.profile_image
                                }} />
                        </View>
                        <Spacer space={1} />
                        <View style={style.messageSender}>
                            <Text style={style.sender}>{item.message}
                            </Text>
                        </View>
                    </View>
            }
        </View >
    )


}
export default RenderItem
RenderItem.defaultProps = {
    item: {
        user_name: "",
        message: "",
        profile_image: ""
    },

};
RenderItem.propTypes = {
    item: PropTypes.object,
}
