// ----------------------------------------
// PACKAGE IMPORTS
// ----------------------------------------
import React, {Component} from 'react';

// const io = require('socket.io-client');
import io from 'socket.io-client';
import AsyncStorage from '@react-native-community/async-storage';
import {connect} from 'react-redux';
import {CommonActions} from '@react-navigation/native';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import {
    View,
    Text,
    ScrollView,
    TouchableOpacity,
    Image,
    FlatList,
    SafeAreaView,
    TextInput,
    Platform,
    KeyboardAvoidingView,
    StatusBar, Alert,
} from 'react-native';
// ----------------------------------------
// LOCAL IMPORTS
// ----------------------------------------
import {COLORS} from '../../themes/Colors';
import Header from '../../components/header';
import {Spacer} from '../../components/spacer';
import {Strings} from '../../utils/Strings';
import BaseClass from '../../utils/BaseClass';
import style from './FeedStyle';
import ChatModal from '../../components/modal';
import RenderItem from './component/Rendeitem';
import BottomTab from '../../components/bottomTab';
import {ChatMessageAction, acceptTerm, lastSeen} from '../../redux/actions/ChatMessageAction';
import {NotificationCount} from '../../redux/actions/NotificationAction';
import {ReadNotification} from '../../redux/actions/NotificationAction';
import OrientationLoadingOverlay from '../../utils/CustomLoader';


class FeedScreen extends BaseClass {
    constructor(props) {
        super(props);
        this.state = {
            user_name: undefined,
            chatMessage: '',
            chatMessages: [],
            isCheck: false,
            is: true,
            length: 0,
            token: undefined,
            userProfile: undefined,
            userId: undefined,
            isVisible: false,
            chatCountt: 0,
            chatCount: 0,
            allowToMessage: false,
            isLoading: false,
            isRefreshing: false,
            refreshItem: undefined,
        };
        this.onEndReachedCalledDuringMomentum = true;

    }

    // ----------------------------------------
    // Life Cycle method
    // ----------------------------------------
    componentDidMount = () => {
        const {navigation} = this.props;
        this.setState({
            is: false,
        });
        this.socket = io('http://stgn.appsndevs.com:9099', {
            transports: ['websocket'],
            jsonp: false,
        });
        this.socket.connect();
        /*  this.socket.on('connect', () => {
              console.warn('connected to socket server');
          });*/
        this.socket.on('received', msg => {
            this.state.chatMessages.push(msg.message);
        });


        AsyncStorage.getItem('chatCount', (err, chatCount) => {
            //   console.warn('Count-->', chatCount);
            if (!this.isConnected()) {
                this.showToastAlert(Strings.NO_INTERNET);
            } else {
                console.warn('count', chatCount);
                this.setState(
                    {
                        chatCount: chatCount,
                    },
                );
            }

        });

        //  BackHandler.addEventListener('hardwareBackPress', exitAlert);
        AsyncStorage.getItem(Strings.TOKEN, (err, token) => {

            if (token !== undefined && token !== null) {
                if (!this.isConnected()) {
                    this.showToastAlert(Strings.NO_INTERNET);
                } else {
                    this.setState({
                        isCheck: true,
                        token: JSON.parse(token),
                    });
                    /*  this.showDialog();*/

                    this.props.getChatMessage({
                        token: JSON.parse(token),
                        per_page: 20,
                        page: 1,
                    });


                }


                ReadNotification({
                        token: JSON.parse(token),
                        //  post_id: postID,
                    }, res => this.ReadNotificationResponse(res),
                );


            }
            NotificationCount({
                    token: JSON.parse(token),
                }, res => this.NotificationCountResponse(res),
            );


        });
    };

    ReadNotificationResponse = (res) => {
        //  console.warn('chkk', res)
        if (res.code == 200) {
            AsyncStorage.setItem('chatCount', '');
            this.setState({
                chatCount: '',
            });
        }

    };


    NotificationCountResponse = (res) => {
        console.warn('CheckData', res);
        const {navigation} = this.props;
        this.hideDialog();
        if (res.code == 200 && res.data !== undefined) {
            console.warn('hhjjkk', res.data.chat_count);
            AsyncStorage.setItem('chatCount', res.data.chat_count);
            this.setState({
                    chatCount: res.data.chat_count,
                },
            );
        } else if (res.code === 'rest_forbidden') {
            this.hideDialog();
            navigation.dispatch(
                CommonActions.reset({
                    index: 0,
                    routes: [
                        {
                            name: 'login',
                        },
                    ],
                }),
            );
        }
    };


    onFocusFunction = () => {
        this.componentDidMount();
    };

    componentDidUpdate = async (prevProps, prevState) => {
        const {navigation, route} = this.props;

        if (route.params !== undefined) {
            if (route.params.dummy !== undefined && route.params.dummy !== '') {
                navigation.setParams({dummy: ''});
                this.onFocusFunction();
            }
        }
        if (prevProps.chatMessageResponse !== this.props.chatMessageResponse && prevState.isCheck) {
            const {chatMessageResponse} = this.props;

            if (chatMessageResponse.code == 200) {
                console.warn('chch', chatMessageResponse.message[0]);

                console.warn('dfdf', chatMessageResponse.data);


                await this.setState({
                    length: chatMessageResponse.message.length,
                    isVisible: chatMessageResponse.data.login_user_term_status == 0 || chatMessageResponse.data.login_user_term_status === null ? true : false,
                    user_name: chatMessageResponse.data.login_user_display_in_app !== undefined && chatMessageResponse.data.login_user_display_in_app !== null ? chatMessageResponse.data.login_user_display_in_app : '',
                    userId: chatMessageResponse.data.login_user_id,
                    userProfile: chatMessageResponse.data.login_user_profile_image,
                    chatMessages: chatMessageResponse.message,
                    isCheck: false,
                    isRefreshing: false,
                    allowToMessage: chatMessageResponse.data.login_user_term_status == 0 || chatMessageResponse.data.login_user_term_status === null ? false : true,
                });
                // if (this.state.refreshItem !== undefined) {
                //     this.flatList.scrollToItem({
                //         animated: false,
                //         item: this.state.refreshItem,
                //     });
                //     // this.flatList.scrollToIndex({
                //     //     animated: true,
                //     //     viewPosition: 1,
                //     //     index: 20,
                //     // });
                // }


            } else if (chatMessageResponse.code === 'rest_forbidden') {
                this.hideDialog();
                navigation.dispatch(
                    CommonActions.reset({
                        index: 0,
                        routes: [
                            {
                                name: 'login',
                            },
                        ],
                    }),
                );
            } else if (chatMessageResponse.code == 403) {
                this.showToastAlert(chatMessageResponse.message);
                const {navigation} = this.props;
                navigation.dispatch(
                    CommonActions.reset({
                        routes: [
                            {name: 'login'},
                        ],
                    }),
                );
            } else if (chatMessageResponse == 'Network request failed') {
                this.showToastAlert(Strings.NO_INTERNET);
            } else {
                this.showToastAlert(Strings.SOMETHING_WENT_WRONG);

            }

        }


    };


    /*  componentWillUnmount() {
          this._unsubscribe();

          // BackHandler.removeEventListener('hardwareBackPress', exitAlert);
      }*/


    // ----------------------------------------
    // Class local method
    // ----------------------------------------

    async sendMessage() {
        const {chatMessage, userId, user_name, userProfile} = this.state;
        if (!this.isConnected()) {
            this.showToastAlert(Strings.NO_INTERNET);
        } else if (chatMessage.trim().length !== 0) {
            this.onEndReachedCalledDuringMomentum = true;
            await this.socket.emit('chat message', {
                message: chatMessage,
                user_id: userId,
                user_name: user_name,
                profile_image: userProfile,
                created_at: '',
            });
            this.setState({chatMessage: ''}, () => this.onFocusFunction());
        } else {
            this.showToastAlert('Message cannot be empty.');
        }
    };

    // ----------------------------------------
    // Open drawer
    // ----------------------------------------

    _openDrawer = () => {
        const {navigation} = this.props;
        navigation.openDrawer();

    };

    _commingSoon = () => {
        alert('coming soon');
    };


    decline = () => {
        const {navigation} = this.props;
        this.setState({
            isVisible: false,
            allowToMessage: false,
        });
        navigation.navigate('home');
    };

    // ----------------------------------------
    // Accept chat terms
    // ----------------------------------------

    accept = () => {
        const {token} = this.state;
        acceptTerm({
                token: token,
                status: 1,
            }, res => this.acceptTermResponse(res),
        );
    };

    acceptTermResponse = (res) => {
        const {navigation} = this.props;
        if (res.code === 200) {
            this.setState({
                isVisible: false,
                allowToMessage: true,
            });

        } else if (res.code === 'rest_forbidden') {
            this.hideDialog();
            navigation.dispatch(
                CommonActions.reset({
                    index: 0,
                    routes: [
                        {
                            name: 'login',
                        },
                    ],
                }),
            );
        } else if (res.code === 403) {
            this.showToastAlert(res.message);
            const {navigation} = this.props;
            navigation.dispatch(
                CommonActions.reset({
                    routes: [
                        {name: 'login'},

                    ],
                }),
            );
        }
    };


    componentWillUnmount = () => {
        const {token} = this.state;
        //  console.warn('ttttt', token);
        lastSeen({
                token: token,
            }, res => this.lastSeenResponse(res),
        );
    };


    lastSeenResponse = (res) => {
        const {navigation} = this.props;
        //  console.warn('hiii', res);
        if (res.code === 200) {
            /* this.setState({
                 isVisible: false,
             });*/

        } else if (res.code === 'rest_forbidden') {
            this.hideDialog();
            navigation.dispatch(
                CommonActions.reset({
                    index: 0,
                    routes: [
                        {
                            name: 'login',
                        },
                    ],
                }),
            );
        } else if (res.code === 403) {
            this.showToastAlert(res.message);
            /* const {navigation} = this.props;
             navigation.dispatch(
                 CommonActions.reset({
                     routes: [
                         {name: 'login'},

                     ],
                 }),
             );*/
        }

    };


    handlePagination = () => {
        const {token, length} = this.state;
        // if (!this.onEndReachedCalledDuringMomentum) {
        this.setState({
            isCheck: true,
            isRefreshing: true,
            refreshItem: this.state.chatMessages[length - 1],
        });
        this.props.getChatMessage({
            token: token,
            per_page: length + 20,
            page: 1,
        });

        //     this.onEndReachedCalledDuringMomentum = true;
        // }
    };

    openHome = () => {
        this.props.navigation.navigate('home');
    };
    openSelection = () => {
        this.props.navigation.navigate('selection');

    };
    openFeed = () => {
        this.props.navigation.navigate('feed', {dummy: 'Dummy'});

    };

    openNews = () => {
        this.props.navigation.navigate('news');

    };


    Capitalize = (str) => {
        return str.charAt(0).toUpperCase() + str.slice(1);
    };

    // =============================================================================================
    // Render method for Custom Loader
    // =============================================================================================

    _renderCustomLoader = () => {
        const {isLoading} = this.state;
        return (
            <OrientationLoadingOverlay visible={isLoading} message={Strings.LOADING_TEXT}/>
        );
    };


    // ----------------------------------------
    // Main render
    // ----------------------------------------
    render() {
        const {chatMessage, isVisible, chatMessages, userId, chatCountt, chatCount, allowToMessage} = this.state;
        return (
            <View style={{flex: 1}}>
                <KeyboardAvoidingView
                    style={{flex: 1, backgroundColor: COLORS.LIGHT_BLACK}}
                    behavior={(Platform.OS === 'ios') ? 'padding' : null}>
                    <SafeAreaView style={style.mainContainer}>
                        <StatusBar barStyle="light-content" hidden={false}
                                   backgroundColor={COLORS.THEME_COLOR} translucent={true}/>
                        <Header
                            openDrawer={() => this._openDrawer()}
                            onPressIcon={() => this._commingSoon()}
                        />
                        <View style={style.header}>
                            <Text style={style.unreadText}>{Strings.FEED}</Text>
                        </View>
                        <View style={{backgroundColor: 'white', flex: 1}}>
                            <Spacer space={1}/>
                            <FlatList
                                ref={ref => this.flatList = ref}
                                // onContentSizeChange={() => this.flatList.scrollToEnd({animated: true})}
                                // onLayout={() => this.flatList.scrollToEnd({animated: true})}
                                data={chatMessages}
                                inverted={true}
                                getItemLayout={(data, index) => {
                                    return {length: data.length, index, offset: data.length * index};
                                }}
                                onEndReached={() => {
                                    this.handlePagination();
                                }}
                                onEndReachedThreshold={0.01}
                                // onMomentumScrollBegin={() => {
                                //     this.onEndReachedCalledDuringMomentum = false;
                                // }}
                                // onRefresh={() => {
                                //     this.handlePagination();
                                // }}
                                // refreshing={this.state.isRefreshing}
                                keyExtractor={(item) => item.id}
                                extraData={this.state}
                                renderItem={({item}) => (
                                    <RenderItem
                                        item={item}
                                        userId={userId}
                                    />
                                )}
                            />

                            <View style={style.textinputContainer}>
                                <TextInput
                                    style={style.textInput}
                                    onSubmitEditing={() => {
                                        this.sendMessage();
                                    }}
                                    keyboardType="default"
                                    value={chatMessage}
                                    underlineColorAndroid="transparent"
                                    placeholder={Strings.TYPE_MESSAGE}
                                    placeholderTextColor={COLORS.PLACEHOLDER_COLOR}
                                    //  autoCapitalize="none"
                                    onChangeText={text => this.setState({
                                        chatMessage: text,
                                    })}
                                    multiline={true}

                                />
                                <TouchableOpacity
                                    onPress={() => {
                                        if (allowToMessage) {
                                            this.sendMessage();
                                        } else {
                                            this.showToastAlert('Please accept terms and conditions.');
                                        }
                                    }}
                                    style={{position: 'absolute', right: wp(8)}}>
                                    <Image
                                        style={{height: wp(6), width: wp(6)}}
                                        source={require('../../assets/images/Feed/chat.png')}/>
                                </TouchableOpacity>
                            </View>


                        </View>
                        {this._renderCustomLoader()}
                        <BottomTab
                            chatCount={chatCount}
                            openHome={() => this.openHome()}
                            openSelection={() => this.openSelection()}
                            openFeed={() => this.openFeed()}
                            openNews={() => this.openNews()}

                        />
                    </SafeAreaView>

                    <ChatModal
                        decline={() => this.decline(0)}
                        accept={() => this.accept(1)}
                        isVisible={isVisible}
                    />
                </KeyboardAvoidingView>
            </View>
        );
    }
}

// ----------------------------------------
// ----------------------------------------
// CONNECT
// ----------------------------------------

const mapStateToProps = state => ({
    chatMessageResponse: state.ChatMessageReducer.chatMessageResponse,

});

// ----------------------------------------

const mapDispatchToProps = (dispatch) => {
    return {
        getChatMessage: (payload) => dispatch(ChatMessageAction(payload)),
    };
};

// ----------------------------------------

export default connect(mapStateToProps, mapDispatchToProps)(FeedScreen);
