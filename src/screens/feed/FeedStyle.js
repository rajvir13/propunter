import { StyleSheet } from "react-native";
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from "react-native-responsive-screen";
import { COLORS } from '../../themes/Colors';
import { FONTNAME } from '../../utils/FontName';
const styles = StyleSheet.create({
    mainContainer: {
        flex: 1,
        backgroundColor: COLORS.THEME_COLOR
    },
    header: {
        backgroundColor: COLORS.LIGHT_BLACK,
        paddingVertical: wp(4),
        justifyContent: 'center',
        alignItems: 'center',

    },
    headerChild: {


        justifyContent: 'center',
        alignItems: 'center',
    },
    feedText: {
        color: COLORS.WHITE_SEMI,
        fontSize: wp(4.8),
        fontFamily: FONTNAME.ProximaNova,

    },
    unreadText: {
        fontFamily: FONTNAME.ProximaNova,
        color: COLORS.WHITE_SEMI,
        fontSize: wp(4.5),

    },
    textinputContainer: {
        backgroundColor: COLORS.LIGHT_BLACK,
        paddingVertical: wp(3),
        justifyContent: 'center',
        alignItems: 'center',
        flexDirection: 'row'
    },
    textInput: {
        paddingTop:wp(3),
        width: wp(90),
        height: wp(10),
        paddingLeft: wp(4),
        borderRadius: wp(6),
        borderColor: COLORS.WHITE_COLOR,
        borderWidth: 1,
        fontSize: wp(3),
        // fontWeight: 'bold',
        backgroundColor: COLORS.WHITE_COLOR,



    },
    feedIcon: {
        height: wp(4),
        width: wp(4), 
        borderRadius: wp(2),
        backgroundColor: 'white'
    },
    unreadMessage: {
        height: wp(4.5),
        alignItems: 'center',
        justifyContent: 'center',
        width: wp(6),
        borderRadius: wp(2),
        backgroundColor: COLORS.THEME_COLOR
    }


});

export default styles;
