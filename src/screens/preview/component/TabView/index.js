// ----------------------------------------
// PACKAGE IMPORTS
// ----------------------------------------
import React, {Component} from 'react';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import {View, Text, Image, SafeAreaView, TextInput, FlatList, TouchableWithoutFeedback, Keyboard} from 'react-native';

// ----------------------------------------
// LOCAL IMPORTS
// ----------------------------------------
import {COLORS} from '../../../../themes/Colors';
// import RenderItem from '../renderItem';
import {TabView, PagerScroll} from '../../../../../local_module/react-native-tab-view';
import Today from './Today';
import Tomorrow from './Tomorrow';
import Upcoming from './Upcoming';
import TabBar from '../../../../../local_module/react-native-tab-view/src/TabBar';
import BaseClass from '../../../../utils/BaseClass';
import {FONTNAME} from '../../../../utils/FontName';

export default class Tab extends BaseClass {
    constructor(props) {
        super(props);
        this.state = {
            indexItem: undefined,
            index: 0,
            routes: [
                {key: 'today', title: 'TODAY'},
                {key: 'tomorrow', title: 'TOMORROW'},
                {key: 'upcoming', title: 'UPCOMING'},
            ],
        };
    }


    componentWillReceiveProps = (nextProps) => {
        console.warn('next', nextProps);
        this.setState({
            index: this.props.index,
            indexItem: this.props.indexItem,
        });
    };


    _handleIndexChange = (index) => {
        //  console.warn(index);
        this.setState({index});
    };
    _renderScene = ({route}) => {
        const {indexItem, index} = this.state;
        console.warn('chk', route.key);
        switch (route.key) {
            case 'today':
                if (index === 0) {
                    return <Today
                        indexItem={indexItem}
                        routeKey={this.state.index} {...this.props}
                    />;
                }
            case 'tomorrow':
                if (index === 1) {
                    return <Tomorrow
                        indexItem={indexItem}
                        routeKey={this.state.index} {...this.props} />;
                }
            case 'upcoming':
                if (index === 2) {
                    return <Upcoming
                        indexItem={indexItem}
                        routeKey={this.state.index} {...this.props} />;
                }
            default:
                return null;
        }
    };

    render() {
        const {index} = this.state;
        return (
            <>
                <TabView
                    navigationState={this.state}
                    renderScene={this._renderScene}
                    onIndexChange={this._handleIndexChange}
                    renderTabBar={props => (
                        <TabBar
                            {...props}
                            scrollEnabled
                            tabStyle={{width: wp('100%') / 3, color: COLORS.GREEN, textAlign: 'center'}}
                            style={{
                                // backgroundColor: COLORS.WHITE_COLOR,
                            }}
                            //labelStyle={{color: 'yellow', fontsize: 10}}
                            renderLabel={({route, focused, color}) => {
                                // console.warn(route, focused, color);
                                if (index === 0) {
                                    if (route.key === 'today') {
                                        return <Text style={{
                                            color: COLORS.WHITE_COLOR,
                                            fontSize: wp(4.4),
                                            fontFamily: FONTNAME.ProximaNova,
                                        }}> {route.title} </Text>;
                                    }
                                }
                                if (index === 1) {
                                    if (route.key === 'tomorrow') {
                                        return <Text style={{
                                            color: COLORS.WHITE_COLOR,
                                            fontSize: wp(4.4),
                                            fontFamily: FONTNAME.ProximaNova,
                                        }}> {route.title} </Text>;
                                    }
                                }
                                if (index === 2) {
                                    if (route.key === 'upcoming') {
                                        return <Text style={{
                                            color: COLORS.WHITE_COLOR,
                                            fontSize: wp(4.4),
                                            fontFamily: FONTNAME.ProximaNova,
                                        }}> {route.title} </Text>;
                                    }
                                }
                                return <Text style={{
                                    color: COLORS.PLACEHOLDER_TEXT_COLOR,
                                    fontSize: wp(3.2),
                                    fontFamily: FONTNAME.ProximaNova,
                                }}> {route.title} </Text>;
                            }}
                        />
                    )}

                    renderPager={(props) => <PagerScroll {...props}/>}

                />
            </>
        );
    }

}

