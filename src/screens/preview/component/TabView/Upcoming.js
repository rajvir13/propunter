// ----------------------------------------
// PACKAGE IMPORTS
// ----------------------------------------
import React, {Component} from 'react';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import {View, Text, Image, SafeAreaView, TextInput, FlatList, TouchableWithoutFeedback, Keyboard} from 'react-native';
import {createMaterialTopTabNavigator} from '@react-navigation/material-top-tabs';
import AsyncStorage from '@react-native-community/async-storage';
import {CommonActions} from "@react-navigation/native";
// ----------------------------------------
// LOCAL IMPORTS
// ----------------------------------------
import {Collapsable} from '../CollapsableItem';
import BaseClass from '../../../../utils/BaseClass';
import {TipserPostAction} from '../../../../redux/actions/NewPostAction';
import {Strings} from '../../../../utils/Strings';
import {COLORS} from '../../../../themes/Colors';
import RenderItem from '../../../home/component/tabview/Today';

export default class Upcoming extends BaseClass {
    constructor(props) {
        super(props);
        this.state = {
            token: undefined,
            tipserPost: [],
        };
    }

    // ----------------------------------------
    // Class Life cycle method
    // ----------------------------------------
    componentDidMount = () => {
        AsyncStorage.getItem(Strings.TOKEN, (err, token) => {
            if (token !== undefined && token !== null) {
                this.setState(({
                    token: JSON.parse(token),
                    tipserPost: [],
                }));
                TipserPostAction({
                        token: JSON.parse(token),
                        getby: 'upcoming',
                    }, response => this.tipserDataResponse(response),
                );
            }
        });

    };

    // ----------------------------------------
    // Tipser post response
    // ----------------------------------------

    tipserDataResponse = (response) => {
        const {navigation} = this.props;
        if (response.status === 200) {
            let tipserPost = [];
            let temp = Object.keys(response);
            temp.map((item) => {
                if (item !== 'status') {
                    tipserPost.push(response[item]);
                }
            });
            this.setState({
                tipserPost: tipserPost,
            });
        } else if (response.code === 'rest_forbidden') {
            this.hideDialog();
            navigation.dispatch(
                CommonActions.reset({
                    index: 0,
                    routes: [
                        {
                            name: 'login',
                        },
                    ],
                }),
            );
        }else if (response.code === 401) {
            this.showToastAlert('Record not found');
        }
    };
    // ----------------------------------------
    // Class method
    // ----------------------------------------
    _selectItem = (item) => {

    };

    render() {
        const {tipserPost} = this.state;
        const {indexItem} = this.props;
        console.warn('tecctC', tipserPost.length);

        return (
            <FlatList
                scrollEnabled={false}
                data={tipserPost}
                renderItem={({item, index}) =>
                    <Collapsable
                        mydata={item}
                        status={'upcoming'}
                        indexItem={indexItem}
                        flatListItemIndex={index}
                        tabIndex={2}
                        index={this.props.index}
                    />
                }
                keyExtractor={(item, index) => index.toString()}
                style={{backgroundColor: COLORS.LIGHT_BLACK}}
            />


        );
    }
}


