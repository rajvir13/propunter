import {StyleSheet} from 'react-native';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import {COLORS} from '../../../../themes/Colors';
import {FONTNAME} from '../../../../utils/FontName';

const styles = StyleSheet.create({
    mainContainer: {
        backgroundColor: COLORS.LIGHT_BLACK,
        flexDirection: 'row',
        flex: 1,
        justifyContent: 'space-between', paddingHorizontal: wp(4),
        paddingVertical: wp(4), alignItems: 'center',
    },
    childContainer: {
        flexDirection: 'row',
    },
    lineStyle: {
        width: wp(100),
        height: wp(.3),
        backgroundColor: COLORS.PLACEHOLDER_TEXT_COLOR,
    },
    name: {
        color: COLORS.WHITE_COLOR,
        width: wp(35),
    },
    day: {
        color: '#C6C6C6',
        fontSize: wp(2.8),
    },
    bottomLine: {
        width: wp(100),
        height: wp(.3),
        backgroundColor: COLORS.PLACEHOLDER_TEXT_COLOR,
    },
    descriptionBackground: {
        backgroundColor: COLORS.WHITE_COLOR,
        paddingHorizontal: wp(4),
        paddingTop: wp(3),
        paddingBottom: wp(8),
    },
    descriptionText:
        {
            color: COLORS.PLACEHOLDER_TEXT_COLOR,
            fontSize: wp(3),
        },

    puntersBackground: {
        backgroundColor: COLORS.WHITE_GREY,
        flexDirection: 'row',
        justifyContent: 'space-between',
        paddingLeft: wp(5),
        paddingRight: wp(4),
        paddingVertical: wp(2),
        alignItems: 'center',
    },
    postTitleText: {
        color: COLORS.LIGHT_BLACK,
        width: wp(75),
        fontFamily: FONTNAME.ProximaNova,
    },
    lineSeparator: {
        width: wp(90),
        height: wp(.3),
        backgroundColor: COLORS.PLACEHOLDER_TEXT_COLOR,
        marginLeft: wp(5),
    },
    postBackground: {
        backgroundColor: COLORS.WHITE_COLOR,
        flexDirection: 'column',
        paddingHorizontal: wp(8),
    },
    postHeaderText: {
        color: COLORS.PLACEHOLDER_TEXT_COLOR,
        fontFamily: FONTNAME.ProximaNova,
        fontSize: wp(3),
    },
    postBodyText: {
        color: COLORS.PLACEHOLDER_TEXT_COLOR,
        fontFamily: FONTNAME.ProximaNova,
        fontSize: wp(3),
    },

});

export default styles;
