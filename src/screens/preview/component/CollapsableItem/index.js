// ----------------------------------------
// PACKAGE IMPORTS
// ----------------------------------------

import React from 'react';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import {View, Text, Image, StyleSheet} from 'react-native';
import Icon from 'react-native-vector-icons/AntDesign';
import Icons from 'react-native-vector-icons/FontAwesome5';
import moment from 'moment';
import HTML from 'react-native-render-html';
import {Collapse, CollapseHeader, CollapseBody} from 'accordion-collapse-react-native';

// ----------------------------------------
// LOcal IMPORTS
// ----------------------------------------
import {COLORS} from '../../../../themes/Colors';
import {Spacer} from '../../../../components/spacer';
import styles from './Style';
import {ScrollView} from 'react-native-gesture-handler';

const Collapsable = (props) => {
    const {user_registered, display_name, bio, avatar} = props.mydata.userinfo.data;
    const {status, index, indexItem, flatListItemIndex, tabIndex} = props;
    debugger
    return <Collapse
        isCollapsed={(tabIndex == index && flatListItemIndex == indexItem) ? true : false}
    >
       {/* { props.mydata.data !== "" && props.mydata.data !== undefined && <>*/}
        <CollapseHeader>

           {/* { bio !== "" && bio !== undefined && <>*/}
            <View style={styles.mainContainer}>
                <View style={styles.childContainer}>
                    <Image source={{uri: avatar}}
                           style={{
                               width: wp(6),
                               height: wp(6)}}
                    />
                    <Spacer row={2}/>
                    <Text style={styles.name}>{display_name}</Text>
                </View>
                <Text style={styles.day}>{moment(user_registered).format('hA')} {status.toUpperCase()}</Text>
                <Icon
                    style={{alignSelf: 'center'}}
                    name={'pluscircleo'}
                    size={wp('5%')} color={COLORS.GREEN}/>
            </View>

            <View style={styles.bottomLine}></View>
           {/* </>}*/}
        </CollapseHeader>


        <CollapseBody>
            <Spacer row={200}/>
            {bio !== '' &&
            <View style={styles.descriptionBackground}>
                <HTML html={bio} imagesMaxWidth={wp(100)}/>

            </View>
            }
            <View style={{backgroundColor: COLORS.WHITE_COLOR}}>
                {
                    props.mydata.data.map((item) => {
                        return (<RenderPunterPosts item={item}/>);
                    })
                }
            </View>

        </CollapseBody>
    </Collapse>;
};

const RenderPunterPosts = ({item}) => {
    return (
        <Collapse>
            <CollapseHeader>

                <View style={styles.puntersBackground}>
                    <Icons style={{alignSelf: 'center'}} name={'horse-head'} size={wp('5%')} color={COLORS.THEME_COLOR}/>
                    <Text style={styles.postTitleText}> {item.tracklist.track_name.toUpperCase()}</Text>
                    <Icon
                        name={'pluscircleo'}
                        size={wp('5%')} color={COLORS.GREEN}/>
                </View>
                <Spacer space={2}/>
                <View style={styles.lineSeparator}></View>

            </CollapseHeader>

            <CollapseBody>

                <View style={styles.postBackground}>
                    <Spacer space={3}/>
                    <Text style={styles.postHeaderText}>
                        {item.post_title}
                    </Text>
                    <Spacer space={2}/>
                    <HTML html={item.post_content} imagesMaxWidth={wp(100)}/>
                    <Spacer space={3}/>
                </View>
            </CollapseBody>
        </Collapse>
    );
};

export {Collapsable};
