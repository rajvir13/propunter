import {StyleSheet} from 'react-native';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import {COLORS} from '../../themes/Colors';
import {FONTNAME} from '../../utils/FontName';

const styles = StyleSheet.create({
    mainContainer: {
        flex: 1,
        backgroundColor: COLORS.THEME_COLOR,
    },
    selection: {
        alignItems: 'center',
        justifyContent: 'center',
        paddingVertical: wp(16),
    },
    selectionText: {
        color: COLORS.WHITE_COLOR,
        fontSize: wp(4.8),
        fontFamily: FONTNAME.ProximaNova,
        fontWeight: 'bold',
    },
    notificationAlertView: {
        backgroundColor: COLORS.LIGHT_BLACK,
        flexDirection: 'row',
        padding: wp(5),
        width: wp(90),
    },
    notificationAlertText: {
        textDecorationLine: 'underline',
        color: COLORS.WHITE_COLOR,
        fontWeight: 'bold',
        fontSize: wp(3.5),
    },

});

export default styles;
