// ----------------------------------------
// PACKAGE IMPORTS
// ----------------------------------------
import React, {Component} from 'react';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import {View, Text, ScrollView, SafeAreaView, TextInput, TouchableOpacity, StatusBar} from 'react-native';
import IconNotification from 'react-native-vector-icons/MaterialIcons';

IconNotification.loadFont();

// ----------------------------------------
// LOCAL IMPORTS
// ----------------------------------------
import {COLORS} from '../../themes/Colors';
import Header from '../../components/header';
import {FONTNAME} from '../../utils/FontName';
import Tab from './component/TabView';
import {Spacer} from '../../components/spacer';
import {Strings} from '../../utils/Strings';
import style from './PreviewStyle';
import BottomTab from '../../components/bottomTab';
import AsyncStorage from '@react-native-community/async-storage';
import BaseClass from '../../utils/BaseClass';
import {connect} from 'react-redux';

class PreviewScreen extends BaseClass {

    constructor(props) {
        super(props);
        this.state = {
            chatCount: 0,
            index: this.props.route.params != undefined &&
                this.props.route.params.index !== undefined &&
                this.props.route.params.index,
            header: this.props.route.params != undefined && this.props.route.params.title !== undefined
                && this.props.route.params.title,
            indexItem: this.props.route.params != undefined &&
                this.props.route.params.indexItem !== undefined &&
                this.props.route.params.indexItem,

        };

    }

    // ----------------------------------------
    // Life Cycle Method
    // ----------------------------------------

    componentDidMount = () => {
        const {navigation} = this.props;
        this._unsubscribe = navigation.addListener('focus', () => {
            this.onFocusFunction();
        });
        console.warn('Count-->', this.props.route.params !== undefined ? this.props.route.params.index :0);
        this.setState({
            index: this.props.route.params !== undefined ? this.props.route.params.index :0 ,

            indexItem: this.props.route.params !== undefined ? this.props.route.params.indexItem : '',

            header: this.props.route.params !== undefined ? this.props.route.params.title : undefined,

        });
        AsyncStorage.getItem('chatCount', (err, chatCount) => {

            if (!this.isConnected()) {
                this.showToastAlert(Strings.NO_INTERNET);
            }
            this.setState(
                {
                    chatCount: chatCount,
                },
            );


        });


    };
    onFocusFunction = () => {
        this.componentDidMount();
    };

    componentWillUnmount() {
        this._unsubscribe();
    }


    _tipstersProfile = () => {
        const {navigate} = this.props.navigation;
        navigate('tipstrsprofile');
    };
    // ----------------------------------------
    // Open Drawer
    // ----------------------------------------

    _openDrawer = () => {
        const {navigation} = this.props;
        navigation.openDrawer();

    };

    _commingSoon = () => {
        alert('coming soon');
    };

    _updateSetting = () => {

        this.props.navigation.navigate('notifications');
    };
    openHome = () => {
        this.props.navigation.navigate('home');
    };
    openSelection = () => {
        this.props.navigation.navigate('selection');

    };
    openFeed = () => {
        this.props.navigation.navigate('feed', {dummy: "Dummy"});

    };

    openNews = () => {
        this.props.navigation.navigate('news');

    };
    // ----------------------------------------
    // Main render
    // ----------------------------------------

    render() {
        const {index, indexItem, chatCount} = this.state;
        console.warn("indexs", index, indexItem)
        return (
            <SafeAreaView style={style.mainContainer}>
                <StatusBar barStyle="light-content" hidden={false} backgroundColor={COLORS.THEME_COLOR}
                           translucent={true}/>
                <Header openDrawer={() => this._openDrawer()}
                        onPressIcon={() => this._updateSetting()}/>
                <ScrollView
                    style={{backgroundColor: COLORS.LIGHT_BLACK}}>
                    <View>
                        <View style={style.selection}>
                            <Text style={style.selectionText}>{Strings.SELECTION_HEADING}</Text>
                        </View>
                        <Tab index={index}
                             indexItem={indexItem}
                        />
                    </View>
                    <Spacer space={5}/>
                    <TouchableOpacity
                        onPress={() => this._tipstersProfile()} style={{alignItems: 'center'}}>
                        <Text style={{color: COLORS.PLACEHOLDER_TEXT_COLOR}}>{Strings.TIPSTER}</Text>
                    </TouchableOpacity>
                    <Spacer row={5}/>
                </ScrollView>

              {/*  <View style={{backgroundColor: COLORS.LIGHT_BLACK}}>
                    <View style={style.notificationAlertView}>
                        <IconNotification
                            name={'notifications-active'}
                            size={wp('7%')} color={COLORS.THEME_COLOR}/>
                        <Spacer row={2.3}/>
                        <Text style={{color: COLORS.WHITE_COLOR, fontSize: wp(3.5)}}>
                            {Strings.NOTIFICATION_ALERT}
                            <Text
                                onPress={() => this._updateSetting()}
                                style={style.notificationAlertText}>{Strings.NOTIFICATION_ALERT1}</Text></Text>
                    </View>
                </View>*/}

                <BottomTab
                    chatCount={chatCount}
                    openHome={() => this.openHome()}
                    openSelection={() => this.openSelection()}
                    openFeed={() => this.openFeed()}
                    openNews={() => this.openNews()}
                />

            </SafeAreaView>

        );
    }
}
export default PreviewScreen;