import React from 'react';
import {widthPercentageToDP as wp} from 'react-native-responsive-screen';
import {StyleSheet} from 'react-native';
import {COLORS} from '../../themes/Colors';
import {FONTNAME} from '../../utils/FontName';
import {FONT} from '../../utils/Sizes';

const styles = StyleSheet.create({
    container: {
        flex: 1, backgroundColor: COLORS.THEME_COLOR,
    },
    headerTittleView: {
        flexDirection: 'row',
        paddingHorizontal: wp(4),
        paddingVertical: wp(4),
        alignItems: 'center',


    },
    innerHeaderTittleView: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        paddingHorizontal: wp(4),
        paddingVertical: wp(4),
        alignItems: 'center',


    },
    tittleText: {
        color: COLORS.WHITE_COLOR,
        fontWeight: '700',
        fontSize: wp(4),
        fontFamily: FONTNAME.ProximaNova,
    },
    underline: {
        width: wp(100), height: wp(.3), backgroundColor: '#707070',
    },
    headerTittleText: {
        color: COLORS.WHITE_COLOR,
        fontSize: FONT.TextMediumXX,
        fontFamily: FONTNAME.ProximaNova,


    },
});

export default styles;
