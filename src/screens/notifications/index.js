// ----------------------------------------
// PACKAGE IMPORTS
// ----------------------------------------
import React, {Component} from 'react';
import {
    View,
    Text,
    ScrollView,
    FlatList,
    SafeAreaView,
    TouchableOpacity,
} from 'react-native';

// ----------------------------------------
// LOCAL IMPORTS
// ----------------------------------------
import {COLORS} from '../../themes/Colors';
import {Strings} from '../../utils/Strings';
import Header from '../../components/header';
import RenderItemNotify from './component/RenderItemNotification';
import OrientationLoadingOverlay from '../../utils/CustomLoader';
import style from './NotificationStyle';
import Icon from 'react-native-vector-icons/AntDesign';
import {widthPercentageToDP as wp} from 'react-native-responsive-screen';
import {Spacer} from '../../components/spacer';
import {CommonActions} from '@react-navigation/native';
import {FONT} from '../../utils/Sizes';
import {FONTNAME} from '../../utils/FontName';
import BaseClass from '../../utils/BaseClass';
import {UpdateProfileAction} from '../../redux/actions/UpdateProfileAction';
import {connect} from 'react-redux';
import AsyncStorage from '@react-native-community/async-storage';
import {TipserNotificationStatusAction} from '../../redux/actions/TipserNotificationStatusAction';
import BottomTab from '../../components/bottomTab';
import {AllTipsterAction} from '../../redux/actions/AllTipsterAction';
import {TipsterRow} from '../home/component/tipsterRenderItem';
import * as _ from "lodash"

class NotificationsScreen extends BaseClass {


    constructor(props) {
        super(props);
        this.state = {
            chatCount:0,
            isLoading: false,
            switch1Value: false,
            tipserId: 'undefined',
            status: '',
            token: undefined,
            newDataNotification: [
                {
                    image: 'https://pbs.twimg.com/profile_images/486929358120964097/gNLINY67_400x400.png',
                    title: 'Pro',
                    isSwitchOn: false,
                    tipsterId:6,
                },


            ],

        };
    }


    // ----------------------------------------
    // Class life cycle
    // ----------------------------------------

    componentDidMount = () => {

        const {navigation} = this.props;
        this._unsubscribe = navigation.addListener('focus', () => {
            this.onFocusFunction();
        });
        AsyncStorage.getItem(Strings.TOKEN, (err, token) => {
            console.warn(token);
            this.setState({
                token: JSON.parse(token),
            });
            if (token !== undefined && token !== null) {
                {
                    this.showDialog();
                    this.setState({
                        isCheck: true,

                    });

                    this.props.getAllTipster({
                        token: JSON.parse(token),

                    });
                }

            }
        });
        AsyncStorage.getItem('chatCount',(err,chatCount) => {
            this.setState(
                {
                    chatCount:chatCount,
                }
            )
        })
    };


    onFocusFunction = () => {
        this.componentDidMount();
    };
    componentWillUnmount() {
        this._unsubscribe();
    }

    componentDidUpdate = (prevProps, prevState) => {
        const {tipsterResponse} = this.props;
        const {navigation} = this.props;
        if (prevProps.tipsterResponse !== this.props.tipsterResponse && prevState.isCheck) {
            if (tipsterResponse.code === 200) {
                this.hideDialog();
                let data = [];
                for (let i = 0; i < tipsterResponse.message.length; i++) {
                    data.push({
                        image: tipsterResponse.message[i].profile_image,
                        title: tipsterResponse.message[i].user_info.data.user_nicename,
                        tipsterId: tipsterResponse.message[i].tipster_id,
                        isSwitchOn: tipsterResponse.message[i].status == 0 ? false : true,
                    });
                }
                this.setState({
                    newDataNotification: data,
                    isCheck: false,
                });

            } else if (tipsterResponse.code === 'rest_forbidden') {
                this.hideDialog();
                navigation.dispatch(
                    CommonActions.reset({
                        index: 0,
                        routes: [
                            {
                                name: 'login',
                            },
                        ],
                    }),
                );
            }else if (tipsterResponse.code == 403) {
                this.hideDialog();
                this.showToastAlert(tipsterResponse.message);
            } else if (tipsterResponse == 'Network request failed') {
                this.hideDialog();
                this.showToastAlert(Strings.NO_INTERNET);
            }


        }

    };

    UNSAFE_componentWillReceiveProps(nextProps: Readonly<P>, nextContext: any) {
        const {tipserStatusResponse} = nextProps;
        const {navigation} = this.props;
        this.hideDialog();
        if (tipserStatusResponse !== undefined) {
            if (tipserStatusResponse.code == 200) {
                this.showToastSucess(tipserStatusResponse.message);
            } else if (tipserStatusResponse.code === 'rest_forbidden') {
                this.hideDialog();
                navigation.dispatch(
                    CommonActions.reset({
                        index: 0,
                        routes: [
                            {
                                name: 'login',
                            },
                        ],
                    }),
                );
            }else if (tipserStatusResponse.code === 400) {
                this.hideDialog();
                this.showToastAlert(tipserStatusResponse.message);
            } else {
                this.hideDialog();
                this.showToastAlert(Strings.SOMETHING_WENT_WRONG);
            }
        }
    }


    openHome = () => {
        this.props.navigation.navigate('home');
    };
    openSelection = () => {
        this.props.navigation.navigate('selection');

    };
    openFeed = () => {
        this.props.navigation.navigate('feed', {dummy: "Dummy"});
    };

    openNews = () => {
        this.props.navigation.navigate('news');

    };


    _openDrawer = () => {
        const {navigation} = this.props;
        navigation.openDrawer();

    };

    // =============================================================================================
    // Render method for Custom Loader
    // =============================================================================================

    _renderCustomLoader = () => {
        const {isLoading} = this.state;
        return (
            <OrientationLoadingOverlay visible={isLoading} message={Strings.LOADING_TEXT}/>
        );
    };

    _goBack = () => {
        const {navigation} = this.props;
        navigation.goBack();
    };

    _onSwitchChange = (index, id) => {
        const {newDataNotification, token} = this.state;
        const clone = _.cloneDeep(newDataNotification);
        for (let i = 0; i < clone.length; i++) {
            if (i === index) {
                AsyncStorage.getItem(Strings.TOKEN, (err, token) => {
                    console.warn(token);
                    this.setState({
                        token: JSON.parse(token),
                    });
                    if (token !== undefined && token !== null) {
                        {
                            this.props.tipserStatusApi({
                                tipster_id: id,
                                status: clone[i].isSwitchOn === true ? 1 : 0 ,
                                token: JSON.parse(token),
                            });
                        }
                    }
                });

              //  console.warn("ttttt",clone[i].isSwitchOn)
                clone[i].isSwitchOn = !clone[i].isSwitchOn;
            }

        }
      //  console.warn(newDataNotification[0].isSwitchOn)
        this.setState({newDataNotification:clone});

    };


    // ----------------------------------------
    // Main render
    // ----------------------------------------

    render() {
        const {newDataNotification,chatCount} = this.state;
        return (
            <SafeAreaView style={style.container}>
                <Header
                    openDrawer={() => this._openDrawer()}
                />
                <ScrollView style={{backgroundColor: COLORS.LIGHT_BLACK}}>

                    <View style={style.headerTittleView}>
                        <TouchableOpacity style={{flexDirection: 'row', width: wp(30)}}
                                          onPress={() => this._goBack()}>
                            <Icon
                                style={{}}
                                name={'left'}
                                size={wp('3%')} color={COLORS.PLACEHOLDER_TEXT_COLOR}/>
                            <Spacer row={.5}/>
                            <Text style={{
                                alignSelf: 'center',
                                color: COLORS.PLACEHOLDER_TEXT_COLOR,
                                fontSize: FONT.TextExtraSmall,
                                fontFamily: FONTNAME.ProximaNova,
                            }}>BACK</Text>
                        </TouchableOpacity>

                        <Text style={style.headerTittleText}>{Strings.NOTIFICATIONS}</Text>
                    </View>
                    <View style={style.underline}/>
                    <View style={style.innerHeaderTittleView}>
                        <Text style={{
                            color: COLORS.PLACEHOLDER_TEXT_COLOR,
                            fontSize: FONT.TextExtraSmall,
                            fontFamily: FONTNAME.ProximaNovaregular,
                        }}>{Strings.NOTIFYTEXT}</Text>
                    </View>
                    <View style={style.underline}/>

                    <FlatList
                        data={newDataNotification}
                        renderItem={({item, index}) =>
                            <RenderItemNotify
                                data={item}
                                index={index}
                                onSwitchChange={(index, id) => this._onSwitchChange(index, id)}
                            />
                        }
                        keyExtractor={(item, index) => index.toString()}

                    />

                    {this._renderCustomLoader()}
                </ScrollView>
                <BottomTab
                    chatCount = {chatCount}
                    openHome={() => this.openHome()}
                    openSelection={() => this.openSelection()}
                    openFeed={() => this.openFeed()}
                    openNews={() => this.openNews()}
                />
            </SafeAreaView>

        );
    }
}

// ----------------------------------------
// ----------------------------------------
// CONNECT
// ----------------------------------------

const mapStateToProps = state => ({
    tipserStatusResponse: state.TipserStatusReducer.tipserStatusResponse,
    tipsterResponse: state.AllTipsterReducer.tipsterResponse,
});

// ----------------------------------------

const mapDispatchToProps = (dispatch) => {
    return {
        tipserStatusApi: (payload) => dispatch(TipserNotificationStatusAction(payload)),
        getAllTipster: (payload) => dispatch(AllTipsterAction(payload)),
    };
};

// ----------------------------------------

export default connect(mapStateToProps, mapDispatchToProps)(NotificationsScreen);
