import React, {Component} from 'react';
import {widthPercentageToDP as wp} from 'react-native-responsive-screen';
import {View, Text, Image} from 'react-native';
import {Switch} from '../../../../components/toggleSwitch';
import {Spacer} from '../../../../components/spacer';
import style from './Style';
import {FONT} from '../../../../utils/Sizes';


const RenderItemNotify = ({data, index, onSwitchChange}) => {
    return (
        <View>
            <View style={{flexDirection: 'row'}}>
                <View style={style.childContainer}>
                    <Image source={{uri: data.image}}
                           style={{width: wp(6), height: wp(6), borderWidth: 1}}
                    />
                    <Spacer row={2}/>
                    <Text style={style.tittleText}>{data.title}</Text>
                </View>

                <View style={{justifyContent: 'center'}}>
                    <Switch
                        value={data.isSwitchOn}
                        onValueChange={() => {
                            onSwitchChange(index, data.tipsterId);
                        }}
                        activeTextStyle={{fontSize: 6}}
                        inactiveTextStyle={{fontSize: 6}}
                        activeText="ON"
                        inActiveText="OFF"
                    />
                </View>
            </View>

            <View style={style.underline}></View>
        </View>
    );
};
export default RenderItemNotify;
