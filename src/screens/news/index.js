// ----------------------------------------
// PACKAGE IMPORTS
// ----------------------------------------
import React, {Component} from 'react';
import {
    View,
    Text,
    ScrollView,
    FlatList,
    SafeAreaView,
    TextInput,
    TouchableWithoutFeedback,
    StatusBar, BackHandler,
} from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';
import {connect} from 'react-redux';
import {CommonActions} from '@react-navigation/native';

// ----------------------------------------
// LOCAL IMPORTS
// ----------------------------------------
import {COLORS} from '../../themes/Colors';
import {Strings} from '../../utils/Strings';
import Header from '../../components/header';
import BottomTab from '../../components/bottomTab';

import RenderItem from './component/RenderItem';
import {NewsPostAction} from '../../redux/actions/NewPostAction';
import BaseClass from '../../utils/BaseClass';
import OrientationLoadingOverlay from '../../utils/CustomLoader';
import style from './NewsStyle';
import {backAction} from '../../components/helper';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';

class NewsScreen extends BaseClass {
    constructor(props) {
        super(props);
        this.state = {
            newPost: [],
            isLoading: false,
            chatCount: 0,

        };
        this.onEndReachedCalledDuringMomentum = true;
    }

    // ----------------------------------------
    // Class Life cycle method
    // ----------------------------------------
    componentDidMount = () => {
        const {navigation} = this.props;
        BackHandler.removeEventListener('hardwareBackPress', backAction);

        this._unsubscribe = navigation.addListener('focus', () => {
            this.onFocusFunction();
        });
        AsyncStorage.getItem('chatCount', (err, chatCount) => {
       //     console.warn('Count-->', chatCount);
            if (!this.isConnected()) {
                this.showToastAlert(Strings.NO_INTERNET);
            }
            this.setState(
                {
                    chatCount: chatCount,
                },
            );
        });
        AsyncStorage.getItem(Strings.TOKEN, (err, token) => {
            if (token !== undefined && token !== null) {
                this.setState(({
                    token: JSON.parse(token),
                }));

                this.showDialog();
                this.props.getNewsPost({token: JSON.parse(token), check: 0, perPage: 20});


            }
        });
    };

    componentWillUnmount() {
        BackHandler.addEventListener('hardwareBackPress', backAction);
    }



    onFocusFunction = () => {
        this.componentDidMount();
    };


    UNSAFE_componentWillReceiveProps = (nextProps) => {
        const {newPostResponse} = nextProps;
        const {newPost} = this.state;
        const {navigation} = this.props;
        this.hideDialog();
        if (newPostResponse !== undefined && newPostResponse !== null) {
            if (newPostResponse.data !== null && newPostResponse.data !== newPost) {
                if (newPostResponse.code === 200) {
                    this.setState({
                        newPost: newPostResponse.data !== null && newPostResponse.data,
                    });
                    //this.showToastSucess(newPostResponse.message)
                } else if (newPostResponse.code === 'rest_forbidden') {
                    this.hideDialog();
                    navigation.dispatch(
                        CommonActions.reset({
                            index: 0,
                            routes: [
                                {
                                    name: 'login',
                                },
                            ],
                        }),
                    );
                }else if (newPostResponse.code === 401) {
                    this.showToastAlert(newPostResponse.message);
                } else if (newPostResponse.code == 403) {
                    const {newPostResponse} = this.props;
                    this.showToastAlert(newPostResponse.message);
                    const {navigation} = this.props;
                    navigation.dispatch(
                        CommonActions.reset({
                            routes: [
                                {name: 'login'},
                            ],
                        }),
                    );
                } else if (newPostResponse == 'Network request failed') {
                    this.showToastAlert(Strings.NO_INTERNET);
                } else {
                    this.showToastAlert(Strings.SOMETHING_WENT_WRONG);
                }
            }
        } else {
            this.showToastAlert(Strings.SOMETHING_WENT_WRONG);

        }
    };


    _openDrawer = () => {
        const {navigation} = this.props;
        navigation.openDrawer();

    };

    // =============================================================================================
    // Render method for Custom Loader
    // =============================================================================================

    _renderCustomLoader = () => {
        const {isLoading} = this.state;
        return (
            <OrientationLoadingOverlay visible={isLoading} message={Strings.LOADING_TEXT}/>
        );
    };
    handlePagination = () => {
        const {token} = this.state;
        let perPage = 15;
        perPage = perPage + 1;
        if (!this.onEndReachedCalledDuringMomentum) {
            this.props.getNewsPost({
                token: token,
                check: 0,
                perPage: 15,
            });

            this.onEndReachedCalledDuringMomentum = true;
        }

    };


    _onPressOpenDetails = (item,item1) => {
        console.warn("item +++++++",item,item1)
        const {navigate} = this.props.navigation;
        navigate('newsdetails', {item: item});

    };
    _commingSoon = () => {
        alert('coming soon');
    };

    openHome = () => {
        this.props.navigation.navigate('home');
    };
    openSelection = () => {
        this.props.navigation.navigate('selection');

    };
    openFeed = () => {
        this.props.navigation.navigate('feed', {dummy: "Dummy"});

    };

    openNews = () => {
        this.props.navigation.navigate('news');

    };

    /* componentWillUnmount() {
         this._unsubscribe();
         BackHandler.removeEventListener('hardwareBackPress',  this.goBack());
         return true;
     }*/


    // ----------------------------------------
    // Main render
    // ----------------------------------------

    render() {
        const {newPost, chatCount} = this.state;
        return (
            <SafeAreaView style={style.container}>
                <StatusBar barStyle="light-content" hidden={false} backgroundColor={COLORS.THEME_COLOR}
                           translucent={true}/>

                <Header
                    openDrawer={() => this._openDrawer()}
                    onPressIcon={() => this._commingSoon()}

                />
                <ScrollView style={{backgroundColor: COLORS.LIGHT_BLACK}}>
                    <View style={{alignContent: 'flex-start'}}>
                        <View style={style.headerTittleView}>
                            <Text style={style.tittleText}>{Strings.LATEST_NEWS}</Text>
                        </View>
                        <View style={style.underline}></View>
                        <View style={{flex:1,}}>
                        <FlatList
                            data={newPost}
                            extraData={this.state}
                            keyExtractor={this.keyExtractor}
                            onEndReached={this.handlePagination.bind(this)}
                            onEndReachedThreshold={0.5}
                            onMomentumScrollBegin={() => {
                                this.onEndReachedCalledDuringMomentum = false;
                            }}
                            scrollEnabled={false}
                            renderItem={({item}) => <RenderItem
                                item={item}
                                openDetails={(item) => this._onPressOpenDetails.bind(this,item)}
                            />}
                            keyExtractor={(item, index) => index.toString()}
                            style={{flex: 1}}
                        />
                        </View>
                    </View>
                    {this._renderCustomLoader()}
                </ScrollView>
                <BottomTab
                    chatCount={chatCount}
                    openHome={() => this.openHome()}
                    openSelection={() => this.openSelection()}
                    openFeed={() => this.openFeed()}
                    openNews={() => this.openNews()}

                />

            </SafeAreaView>

        );
    }
}

// ----------------------------------------
// ----------------------------------------
// CONNECT
// ----------------------------------------

const mapStateToProps = state => ({
    newPostResponse: state.NewPostReducer.newPostResponse,

});

// ----------------------------------------

const mapDispatchToProps = (dispatch) => {
    return {
        getNewsPost: (payload) => dispatch(NewsPostAction(payload)),
    };
};

// ----------------------------------------

export default connect(mapStateToProps, mapDispatchToProps)(NewsScreen);
