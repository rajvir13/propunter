// ----------------------------------------
// PACKAGE IMPORTS
// ----------------------------------------
import Icon from 'react-native-vector-icons/AntDesign';
import React, {Component} from 'react';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import {
    View,
    Text,
    ScrollView,
    SafeAreaView,
    TextInput,
    TouchableOpacity,
    StatusBar,
    Platform,
    BackHandler,
} from 'react-native';
import moment from 'moment';
import HTML from 'react-native-render-html';
import IconNotification from 'react-native-vector-icons/MaterialIcons';
import {CommonActions} from '@react-navigation/native';

// ----------------------------------------
// LOCAL IMPORTS
// ----------------------------------------
import {COLORS} from '../../../themes/Colors';
import Header from '../../../components/header';
import {Spacer} from '../../../components/spacer';
import {FONTNAME} from '../../../utils/FontName';
import BottomTab from '../../../components/bottomTab';
import {FONT} from '../../../utils/Sizes';
import {backAction} from '../../../components/helper';
import AsyncStorage from '@react-native-community/async-storage';


Icon.loadFont();
IconNotification.loadFont();
export default class NewsDetailsScreen extends Component {
    constructor(props) {
        super(props);
        this.state = {
            item: this.props.route.params.item,
            chatCount:0,
        };
    }


    componentDidMount() {
        this.setState({
            item: this.props.route.params.item,
        })
        const {navigation} = this.props;
        this._unsubscribe = navigation.addListener('focus', () => {
            this.onFocusFunction();
        });

        console.warn("item value====>", this.props.route.params.item)
        AsyncStorage.getItem('chatCount', (err, chatCount) => {
            BackHandler.removeEventListener('hardwareBackPress', backAction);
         //   console.warn('Count-->', chatCount);
            this.setState(
                {
                    chatCount: chatCount,
                },
            );
        });
    }

    onFocusFunction = () => {
        this.componentDidMount();
    };

    componentWillUnmount() {
        this._unsubscribe();
        BackHandler.addEventListener('hardwareBackPress', backAction);
    }



    _openDrawer = () => {

        const {navigation} = this.props;
        navigation.openDrawer();

    };
    _goBack = () => {
        const {navigation} = this.props;
        navigation.goBack();


    };


    _updateSetting = () => {
        this.props.navigation.navigate('notifications');
    };
    openHome = () => {
        this.props.navigation.navigate('home');
    };
    openSelection = () => {
        this.props.navigation.navigate('selection');

    };
    openFeed = () => {
        this.props.navigation.navigate('feed', {dummy: "Dummy"});

    };

    openNews = () => {
        this.props.navigation.navigate('news');

    };
    // ----------------------------------------
    // Main render
    // ----------------------------------------

    render() {
        const {item, chatCount} = this.state;
        console.warn("ttt-->",item)
        return (
            <SafeAreaView style={{flex: 1, backgroundColor: COLORS.THEME_COLOR}}>
                <StatusBar barStyle="light-content" hidden={false} backgroundColor={COLORS.THEME_COLOR}
                           translucent={true}/>

                <Header
                    openDrawer={() => this._openDrawer()}
                    onPressIcon={() => this._updateSetting()}

                />
                <ScrollView style={{backgroundColor: COLORS.WHITE_COLOR}}>
                    <View>
                        <View style={{
                            backgroundColor: COLORS.LIGHT_BLACK,
                            paddingVertical: wp(2), paddingHorizontal: wp(4),
                        }}>
                            <TouchableOpacity style={{flexDirection: 'row'}}
                                              onPress={() => this._goBack()}>
                                <Icon
                                    style={{marginTop: 2}}
                                    name={'left'}
                                    size={wp('3%')} color={COLORS.PLACEHOLDER_TEXT_COLOR}/>
                                <Spacer row={.5}/>
                                <Text style={{
                                    color: COLORS.PLACEHOLDER_TEXT_COLOR,
                                    fontFamily: FONTNAME.ProximaNova,
                                    fontSize: wp(2.3),
                                    marginTop: Platform.OS == 'ios' ? wp(1) : wp(0.8),
                                }}>BACK</Text>
                            </TouchableOpacity>
                            <Spacer space={2}/>

                            <View style={{paddingHorizontal: wp(2)}}>
                                <Text style={{
                                    color: COLORS.WHITE_COLOR,
                                    fontFamily: FONTNAME.ProximaNova,
                                    fontSize: FONT.TextMediumXX,
                                }}>{item.post_title.toUpperCase()}</Text>
                            </View>
                            <Spacer space={1}/>

                            <View style={{paddingHorizontal: wp(2), flexDirection: 'row'}}>
                                <Text style={{
                                    color: COLORS.PLACEHOLDER_TEXT_COLOR,
                                    fontFamily: FONTNAME.ProximaNova,
                                    fontSize: wp(2.3),
                                }}>Author:</Text>
                                <Spacer row={3}/>

                                <Text
                                    style={{
                                        color: COLORS.PLACEHOLDER_TEXT_COLOR,
                                        fontFamily: FONTNAME.ProximaNova,
                                        fontSize: wp(2.3),
                                    }}>{item.user.data.display_name}</Text>
                                <Spacer row={3}/>

                                <Text
                                    style={{
                                        color: COLORS.PLACEHOLDER_TEXT_COLOR,
                                        fontFamily: FONTNAME.ProximaNova,
                                        fontSize: wp(2.3),
                                    }}>{moment(item.user.data.user_registered).format('D/MM/YYYY')}</Text>
                            </View>
                            <Spacer space={1}/>

                        </View>

                        <View style={{padding: wp(5)}}>
                            <HTML html={item.post_content} imagesMaxWidth={wp(100)}/>
                        </View>


                    </View>
                </ScrollView>

                {/*<View style={{backgroundColor: COLORS.WHITE_COLOR}}>*/}
                {/*<View style={{flexDirection: 'row', padding: wp(5), width: wp(90)}}>*/}
                {/*    <IconNotification*/}
                {/*        name={'notifications-active'}*/}
                {/*        size={wp('7%')} color={COLORS.THEME_COLOR}/>*/}
                {/*    <Spacer row={3}/>*/}
                {/*    <Text style={{color: COLORS.BLACK_COLOR}}>Don`t miss a tip . Switch on Push Notifications*/}
                {/*        to get all the latest updates live. <Text*/}
                {/*            onPress={() => this._updateSetting()}*/}
                {/*            style={{*/}
                {/*                textDecorationLine: 'underline',*/}
                {/*                color: COLORS.BLACK_COLOR, fontFamily: FONTNAME.ProximaNova, fontSize: wp(3.6),*/}
                {/*            }}>Update settings here.</Text></Text>*/}

                {/*</View>*/}
                {/*</View>*/}
                <BottomTab
                    chatCount={chatCount}
                    openHome={() => this.openHome()}
                    openSelection={() => this.openSelection()}
                    openFeed={() => this.openFeed()}
                    openNews={() => this.openNews()}

                />

            </SafeAreaView>

        );
    }
}
