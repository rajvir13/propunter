import React, {Component} from 'react';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import {View, Text, Image, FlatList, TextInput, TouchableWithoutFeedback, Keyboard} from 'react-native';
import {COLORS} from '../../../../themes/Colors';
import {FONTNAME} from '../../../../utils/FontName';
import {Spacer} from '../../../../components/spacer';
import style from './Style';
import {TouchableOpacity} from 'react-native-gesture-handler';

const RenderItem = (props) => {
    const {item, openDetails} = props;
  //  console.warn("Newsss-->",props)
    return <View style={{}}>
        <TouchableOpacity
            onPress={openDetails(item)}
            style={style.childContainer}>
            <Image source={{uri: item.featured_url !== false && item.featured_url}}
                   style={{width: wp(23), height: wp(16)}}
            />
            <Spacer row={2} />
            <View style={{ flexDirection: 'column'}}>
                <Text style={style.tittleText}>{item.post_title.toUpperCase()}</Text>
                <Spacer space={1}/>
                <Text style={style.detailsText}
                >{item.post_excerpt}</Text>
            </View>
        </TouchableOpacity>
        <View style={style.underline}></View>
    </View>;
};
export default RenderItem;