import React from 'react';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import {StyleSheet} from 'react-native';
import {COLORS} from '../../../../themes/Colors';
import {FONTNAME} from '../../../../utils/FontName';
import {FONT} from '../../../../utils/Sizes';

const styles = StyleSheet.create({
    childContainer: {
        backgroundColor: COLORS.LIGHT_BLACK,
        flexDirection: 'row',
        justifyContent: 'space-between',
        paddingHorizontal: wp(4),
        paddingVertical: wp(4), alignItems: 'center',
    },
    tittleText: {
        width: wp(70),
        color: 'white',
        fontFamily: FONTNAME.ProximaNova,
        fontSize: FONT.TextMediumXX,
    },

    detailsText: {
        width: wp(65),
        color: 'white',
        fontFamily: FONTNAME.ProximaNovaregular,
        fontSize: wp(2.3),
    },
    underline: {
        width: wp(100), height: wp(.3), backgroundColor: '#707070',
    },
});

export default styles;
