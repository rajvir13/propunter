// ----------------------------------------
// PACKAGE IMPORTS
// ----------------------------------------
import * as React from 'react';
import {NavigationContainer} from '@react-navigation/native';
import {createDrawerNavigator} from '@react-navigation/drawer';
import {createStackNavigator} from '@react-navigation/stack';
import {heightPercentageToDP as hp, widthPercentageToDP as wp} from 'react-native-responsive-screen';
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';

const Stack = createStackNavigator();
const Drawer = createDrawerNavigator();
const Tab = createBottomTabNavigator();
// ----------------------------------------
// LOCAL IMPORTS
// ----------------------------------------
import SplashScreen from '../screens/auth/splash';
import LoginScreen from '../screens/auth/login';
import ForgotScreen from '../screens/auth/forgot_password';

import CreatePasswordScreen from '../screens/auth/create_password';
import HomeScreen from '../screens/home';
import PreviewScreen from '../screens/preview';
import FeedScreen from '../screens/feed';
import NewsScreen from '../screens/news';
import TutorialScreen from '../screens/auth/tutorial';
import Tutorial1Screen from '../screens/auth/tutorial/Tutorial1';
import Tutorial2Screen from '../screens/auth/tutorial/Tutorial2';
import Tutorial3Screen from '../screens/auth/tutorial/Tutorial3';
import Tutorial4Screen from '../screens/auth/tutorial/Tutorial4';

import DrawerItemComponent from '../components/drawerItem';
import {COLORS} from '../themes/Colors';
import {Image, View} from 'react-native';
import {DRAWER_ICONS} from '../utils/ImagePaths';
import NewsDetailsScreen from '../screens/news/newsDetails';
import TipsterProfileScreen from '../screens/home/tipsters';
import NotificationScreen from '../screens/notification';
import {navigationRef} from '../../RootNavigation';
import NotificationsScreen from '../screens/notifications';


import SettingsScreen from '../screens/settings/appSettings';
import AccountScreen from '../screens/settings/accountDetails';
import InnerModal from '../components/ScreenshotModal/InnerModal';
import SelectIconScreen from '../screens/settings/accountDetails/SelectIcon';

const DrawerNavigator = () => {
    return (
        <Drawer.Navigator
            drawerPosition='right'
            drawerStyle={{
                backgroundColor: 'rgba(34, 34, 34, 1)',
                width: wp('75%'),

            }}
            drawerContent={props => <DrawerItemComponent {...props} />} initialRouteName="home">
            <Stack.Screen name="notification" component={NotificationScreen} options={{gestureEnabled: false}}/>
            <Stack.Screen name="home" component={HomeScreen} options={{gestureEnabled: false}}/>
            <Stack.Screen name="feed" component={FeedScreen} options={{gestureEnabled: false}}/>
            <Stack.Screen name="selection" component={PreviewScreen} options={{gestureEnabled: false}}/>
            <Stack.Screen name="news" component={NewsScreen} options={{gestureEnabled: false}}/>
            <Stack.Screen name="tipstrsprofile" component={TipsterProfileScreen} options={{gestureEnabled: false}}/>
            <Stack.Screen name="newsdetails" component={NewsDetailsScreen} options={{gestureEnabled: false}}/>
            <Stack.Screen name="settings" component={SettingsScreen} options={{gestureEnabled: false}}/>
            <Stack.Screen name="notifications" component={NotificationsScreen} options={{gestureEnabled: false}}/>
            <Stack.Screen name="accounts" component={AccountScreen} options={{gestureEnabled: false}}/>
            <Stack.Screen name="selecticon" component={SelectIconScreen} options={{gestureEnabled: false}}/>
        </Drawer.Navigator>
    );
};


const InternalStack = () => {
    return (
        <Stack.Navigator initialRouteName="home" headerMode="none">
            <Stack.Screen name="notification" component={NotificationScreen} options={{gestureEnabled: false}}/>
            <Stack.Screen name="home" component={HomeScreen} options={{gestureEnabled: false}}/>
            <Stack.Screen name="feed" component={FeedScreen} options={{gestureEnabled: false}}/>
            <Stack.Screen name="selection" component={PreviewScreen} options={{gestureEnabled: false}}/>
            <Stack.Screen name="news" component={NewsScreen} options={{gestureEnabled: false}}/>
            <Stack.Screen name="tipstrsprofile" component={TipsterProfileScreen} options={{gestureEnabled: false}}/>
            <Stack.Screen name="newsdetails" component={NewsDetailsScreen} options={{gestureEnabled: false}}/>
            <Stack.Screen name="settings" component={SettingsScreen} options={{gestureEnabled: false}}/>


            {/* <Stack.Screen name="newsStack" component={NewsScreen} /> */}


        </Stack.Navigator>
    );
};


const TabNavigator = () => {
    return (
        <NavigationContainer
            independent={true}>
            <Tab.Navigator
                tabBarOptions={{
                    labelStyle: {
                        justifyContent: 'center',
                        alignItems: 'center',
                        paddingTop: hp('1%'),
                        fontSize: wp(2.5),

                    },
                    style: {
                        paddingTop: hp('1%'),
                        backgroundColor: COLORS.THEME_COLOR,
                        justifyContent: 'center',
                        alignItems: 'center',

                    },
                    activeTintColor: COLORS.WHITE_COLOR,
                    inactiveTintColor: COLORS.WHITE_COLOR,
                }}>

                <Tab.Screen name="home"
                            component={InternalStack}
                            options={{
                                tabBarLabel: 'HOME',
                                tabBarIcon: ({color, size}) => (
                                    <Image
                                        style={{
                                            width: wp('6%'),
                                            height: hp('3%'),
                                            marginTop: wp(1),
                                        }}
                                        source={DRAWER_ICONS.HOME_ICON}
                                        resizeMode={'contain'}/>

                                ),

                            }}

                />

                <Tab.Screen name="selection"
                            component={PreviewScreen}
                            options={{
                                tabBarLabel: 'SELECTIONS',
                                tabBarIcon: ({color, size}) => (

                                    <Image style={{
                                        width: wp('6%'),
                                        height: hp('3%'),
                                        marginTop: wp(1),
                                    }}
                                           source={DRAWER_ICONS.PREVIEW_ICON}
                                           resizeMode={'contain'}/>

                                ),
                            }}
                />
                <Tab.Screen name="feed"
                            component={FeedScreen}
                            options={{
                                tabBarLabel: 'THE FEED',
                                tabBarIcon: ({color, size}) => (
                                    <Image style={{
                                        width: wp('6%'),
                                        height: hp('3%'),
                                        marginTop: wp(1),
                                    }}
                                           source={DRAWER_ICONS.FEED_ICON}
                                           resizeMode={'contain'}/>

                                ),
                            }}
                />

                <Tab.Screen name="news"
                            component={NewsScreen}
                            options={{
                                tabBarLabel: 'NEWS',
                                tabBarIcon: ({color, size}) => (

                                    <Image style={{
                                        width: wp('6%'),
                                        height: hp('3%'),
                                        marginTop: wp(1),
                                    }}
                                           source={DRAWER_ICONS.NEWS_ICON}
                                           resizeMode={'contain'}/>

                                ),
                            }}

                />
            </Tab.Navigator>
        </NavigationContainer>

    );
};


const Navigator = () => {

    return (

        <NavigationContainer ref={navigationRef}>
            <Stack.Navigator initialRouteName="splash" headerMode="none" mode="modal">
                <Stack.Screen name="splash" component={SplashScreen}/>
                <Stack.Screen name="login" component={LoginScreen} options={{gestureEnabled: false}}/>
                <Stack.Screen name="tutorial" component={TutorialScreen} options={{gestureEnabled: false}}/>
                <Stack.Screen name="tutorial1" component={Tutorial1Screen}/>
                <Stack.Screen name="tutorial2" component={Tutorial2Screen}/>
                <Stack.Screen name="tutorial3" component={Tutorial3Screen}/>
                <Stack.Screen name="tutorial4" component={Tutorial4Screen}/>
                <Stack.Screen name="feed" component={FeedScreen} options={{gestureEnabled: false}}/>
                <Stack.Screen name="newsdetails" component={NewsDetailsScreen} options={{gestureEnabled: false}}/>
                <Stack.Screen name="notification" component={NotificationScreen} options={{gestureEnabled: false}}/>
                <Stack.Screen name="news" component={NewsScreen} options={{gestureEnabled: false}}/>
                <Stack.Screen name="selection" component={PreviewScreen} options={{gestureEnabled: false}}/>
                <Stack.Screen name="forgot" component={ForgotScreen} options={{gestureEnabled: false}}/>

                <Stack.Screen name="createPassword" component={CreatePasswordScreen} options={{gestureEnabled: false}}/>
                <Stack.Screen name="InnerModal" component={InnerModal} options={{gestureEnabled: false}}/>
                <Stack.Screen name="home" component={DrawerNavigator} options={{gestureEnabled: false}}/>

            </Stack.Navigator>
        </NavigationContainer>
    );
};
export default Navigator;
