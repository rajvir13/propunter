// ----------------------------------------
// PACKAGE IMPORTS
// ----------------------------------------
import React, {Component} from 'react';
import {Provider} from 'react-redux';
import Navigator from './src/routers/AppNavigator';
// import { NetworkProvider } from "./src/utils/NetworkProvider";
import 'react-native-gesture-handler';
import AsyncStorage from '@react-native-community/async-storage';
import PushNotificationIOS from '@react-native-community/push-notification-ios';
import PushNotification from 'react-native-push-notification';
import * as RootNavigation from './RootNavigation';
import {PermissionsAndroid, SafeAreaView} from 'react-native';
import {CommonActions} from '@react-navigation/native';


import {
    addScreenshotListener,
    removeScreenshotListener,
} from 'react-native-detector';
// ----------------------------------------
// LOCAL IMPORTS
// ----------------------------------------

import Store from './src/redux/store';
import OuterModal from './src/components/ScreenshotModal/OuterModal';
import InnerModal from './src/components/ScreenshotModal/InnerModal';

export default class App extends Component {
    constructor() {
        super();
        this.state = {
            isVisible: false,
            isInnerVisible: false,
        };
    }


    decline = () => {

        const {navigation} = this.props;
        this.setState({
            isVisible: false,
            isInnerVisible: true,

        });

    };

    // ----------------------------------------
    // Accept chat terms
    // ----------------------------------------

    accept = () => {
        const {navigation} = this.props;
        this.setState({
            isVisible: false,

        });
    };

    userlogout = () => {
        AsyncStorage.getItem('Auth_token', (err, token) => {
            if (token !== undefined && token !== null) {
                AsyncStorage.clear();
                RootNavigation.navigate('login');
                this.setState({isInnerVisible: false});
            } else {
                this.setState({isInnerVisible: false});
            }
        });

    };
    acceptTerms = () => {
        this.setState({isInnerVisible: false});

    };
    componentDidMount = () => {
        const {isVisible} = this.state;
        const requestPermission = async () => {
            await PermissionsAndroid.request(
                PermissionsAndroid.PERMISSIONS.READ_EXTERNAL_STORAGE,
                {
                    title: 'Get Read External Storage Access',
                    message: 'get read external storage access for detecting screenshots',
                    buttonNeutral: 'Ask Me Later',
                    buttonNegative: 'Cancel',
                    buttonPositive: 'OK',
                },
            );
        };
        if (Platform.OS === 'android') {
            requestPermission();
        }

        const userDidScreenshot = () => {
            this.setState({
                isVisible: true,
            });

            // alert("take screenshotttt....")
        };
        // this.pushConfig();
        if (Platform.OS === 'ios') {
            const eventEmitter = addScreenshotListener(userDidScreenshot);
            return () => {
                removeScreenshotListener(eventEmitter);
            };
        }


    };


    pushConfig = () => {
        PushNotification.configure({
            // (optional) Called when Token is generated (iOS and Android)
            onRegister: function (token) {
                AsyncStorage.setItem('notification', token.token);
            },

            // (required) Called when a remote is received or opened, or local notification is opened
            // onNotification: function (notification) {
            //     // RootNavigation.navigate('notification', {userName: 'Lucy'});
            //     console.warn('onNotification chaliya', appState);
            //     // process the notification
            //
            //     // (required) Called when a remote is received or opened, or local notification is opened
            //     notification.finish(PushNotificationIOS.FetchResult.NoData);
            // },

            onNotification: notification => {
                console.log('notification received', notification);

                notification.foreground
                    ? // toast the message as to not remove the user from what they are doing
                      // they can click it to go to the target or click the x to dismiss
                    console.warn('onNotification chaliya', 'foreground')
                    : // wait until we have a navigation object available to actually navigate
                      // which happens if the app is just starting
                    RootNavigation.navigate('notification', {userName: 'Lucy'});
                notification.finish(PushNotificationIOS.FetchResult.NoData);
            },

            // (optional) Called when Registered Action is pressed and invokeApp is false, if true onNotification will be called (Android)
            onAction: function (notification) {
                console.warn('onAction chaliya');
                RootNavigation.navigate('notification');
            },

            // (optional) Called when the user fails to register for remote notifications. Typically occurs when APNS is having issues, or the device is a simulator. (iOS)
            onRegistrationError: function (err) {
                //  console.error(err.message, err);
            },

            // IOS ONLY (optional): default: all - Permissions to register.
            permissions: {
                alert: true,
                badge: true,
                sound: true,
            },

            // Should the initial notification be popped automatically
            // default: true
            popInitialNotification: true,

            /**
             * (optional) default: true
             * - Specified if permissions (ios) and token (android and ios) will requested or not,
             * - if not, you must call PushNotificationsHandler.requestPermissions() later
             * - if you are not using remote notification or do not have Firebase installed, use this:
             *     requestPermissions: Platform.OS === 'ios'
             */
            requestPermissions: true,
        });
    };


    render() {
        const {isVisible, isInnerVisible} = this.state;
        //===================Main Stack====================//
        return (
            <Provider store={Store}>
                <Navigator/>
                <OuterModal
                    decline={() => this.decline()}
                    accept={() => this.accept()}
                    isVisible={isVisible}
                />
                <InnerModal
                    acceptTerms={() => this.acceptTerms()}
                    userlogout={() => this.userlogout()}
                    isInnerVisible={isInnerVisible}
                />
            </Provider>

        );
    }
}
